<?php
/**
 * @var \frontend\modules\pages\models\Page $page
 */

$this->title = $page->name;
?>
<? if (!empty($page)): ?>
    <section class="block">
        <div class="container">
            <h2 class="block-title"><?=$page->name;?></h2>
            <div class="row">
                <div class="col-sm-12">
                    <h1><?=$page->subname;?></h1>
                    <div class="text">
                        <?=$page->description;?><br><br>
                        <p><?=$page->first_name.' '.$page->last_name.' '.$page->mid_name;?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>