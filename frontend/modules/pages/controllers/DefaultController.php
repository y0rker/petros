<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 20.05.2017
 * Time: 14:53
 */

namespace frontend\modules\pages\controllers;


use frontend\modules\pages\models\Page;
use yii\web\Controller;
use yii\web\HttpException;

class DefaultController extends Controller
{
    public function actionView($page) {
        $page = Page::findByAlias($page);
        if (!empty($page)) {
            return $this->render('view', [
                'page' => $page
            ]);
        } else {
            throw new HttpException(404);
        }
    }
}