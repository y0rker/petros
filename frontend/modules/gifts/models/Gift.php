<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 17:44
 */

namespace frontend\modules\gifts\models;


class Gift extends \common\modules\gifts\models\Gift
{

    /**
     * @return self[]
     */
    public static function findAllGifts() {
        return self::find()->all();
    }
}