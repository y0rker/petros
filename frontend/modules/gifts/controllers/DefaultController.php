<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 17:44
 */

namespace frontend\modules\gifts\controllers;

use common\modules\users\models\User;
use common\modules\users\models\UserGifts;
use frontend\modules\gifts\models\Gift;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class DefaultController
 *
 * @package frontend\modules\gifts\controllers
 */
class DefaultController extends Controller
{
    /**
     * Posts list page.
     */
    public function actionIndex()
    {

        $gifts = Gift::findAllGifts();
        return $this->render('index', [
            'models' => $gifts
        ]);
    }

    /**
     * @param $id
     *
     * @throws \yii\web\NotFoundHttpException
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $gift = Gift::findOne($id);
        if (null === $gift) {
            throw new NotFoundHttpException();
        }

        return $this->render('view', [
            'model' => $gift
        ]);
    }

    public function actionBuy($id)
    {
        $gift = Gift::findOne($id);
        if (null === $gift) {
            throw new NotFoundHttpException();
        }

        if ($gift->canBuy()) {
            try {
                if ($userGift = UserGifts::addNewGiftForUser($gift->id)) {
                    return $this->redirect(['/gifts/default/buy-success', 'id' => $userGift->id]);
                }
            } catch (BadRequestHttpException $e) {
                throw new BadRequestHttpException();
            }
        }

        throw new BadRequestHttpException('У вас не хватает баллов для заказа');
    }

    public function actionBuySuccess($id)
    {
        $userGift = UserGifts::findOne($id);
        if (null === $userGift) {
            throw new NotFoundHttpException();
        }

        return $this->render('buy', [
            'model'     => $userGift->gift,
            'userGift'  => $userGift
        ]);
    }
}