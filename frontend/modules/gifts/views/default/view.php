<?php
/**
 * User: y0rker
 * Date: 12.12.2017
 * Time: 0:32
 */

use common\modules\users\models\User;
use yii\helpers\Url;

/** @var \common\modules\gifts\models\Gift $model */

$this->title = 'Магазин поощрений';

$this->params['layout-title'] = 'Выбор поощрения';
$this->params['layout-subtitle'] = 'Магазин поощрений включает в себя сувенирную и иную продукцию, которую можно обменять на баллы, полученные за активное участие в голосованиях на данном портале.';

?>

<section class="block">
    <div class="container">
        <div class="row">
            <div class="offer__subtitle" style="text-align: center; margin-bottom: 85px;">
                Выдача поощрений производится по адресу:<br>
                ул.Большая Пушкарская,д. 32А, Молодёжный центр «Среда», 3 этаж,<br>
                кабинет руководителя и специалистов центра.<br>
                Время работы с 10:00 до 22:00.<br>
                Правила оформления заказа поощрения указаны в <a href="/pages/term/">пользовательском соглашении</a><br>
                (п.5 «Порядок предоставления поощрений»).
            </div>
            <div class="col-sm-6 col-md-4">
                <div class="catalog-item">
                    <div class="catalog-cart">
                        <? if (!empty($model->photo)): ?>
                            <img class="catalog-item__img img-responsive" src="<?=$model->photoLink;?>">
                        <? endif; ?>
                        <? if (!empty($model->points)): ?>
                            <div class="catalog-item__price-wrap">
                                <div class="catalog-item__price-bg"></div>
                                <div class="catalog-item__price"><?=$model->points;?><?= (!empty($model->points_title) ? ' ' . $model->points_title : ''); ?></div>
                            </div>
                        <? endif; ?>
                    </div>
                    <div class="catalog-item__title"><?=$model->name;?></div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4">
                <?php if ($model->canBuy()): ?>
                    <a data-toggle="modal" data-target="#acceptGift" class="btn btn-success">Заказать</a>
                    <div class="modal modal-primary fade" role="dialog" id="acceptGift">
                        <div class="modal-dialog" role="document">
                            <div class="loader"></div>
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">Заказать
                                    </h4>
                                    <h5 class="modal-subtitle">Вы уверены, что хотите заказать?
                                    </h5>
                                </div>
                                <div class="modal-content-wrapper">
                                    <div class="modal-footer">
                                        <div class="btn-group">
                                            <a href="<?= Url::toRoute(['/gifts/default/buy', 'id' => $model->id]); ?>" class="btn btn-modal">Заказать</a>
                                        </div>
                                        <p></p>
                                        <p>
                                            <a href="#" data-dismiss="modal">отмена</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php elseif (!Yii::$app->getUser()->getIsGuest()): ?>
                    Недостаточно баллов для заказа данного поощрения.
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>