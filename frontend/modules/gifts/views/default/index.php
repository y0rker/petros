<?php
/**
 * @var \yii\web\View $this
 * @var \frontend\modules\gifts\models\Gift[] $models
 */

use yii\helpers\Url;

$this->title = 'Магазин поощрений';

$this->params['layout-title'] = 'Выбор поощрения';
$this->params['layout-subtitle'] = 'Магазин поощрений включает в себя сувенирную и иную продукцию, которую можно обменять на баллы, полученные за активное участие в голосованиях на данном портале.';

?>

<section class="block">
    <div class="container">
        <div class="row">
            <div class="offer__subtitle" style="text-align: center; margin-bottom: 85px;">
                Выдача поощрений производится по адресу:<br>
                ул.Большая Пушкарская,д. 32А, Молодёжный центр «Среда», 3 этаж,<br>
                кабинет руководителя и специалистов центра.<br>
                Время работы с 10:00 до 22:00.<br>
                Правила оформления заказа поощрения указаны в <a href="/pages/term/">пользовательском соглашении</a><br>
                (п.5 «Порядок предоставления поощрений»).
            </div>
            <? foreach ($models AS $gift):
                $url = 'href="' . Url::toRoute(['/gifts/default/view', 'id' => $gift->id]) . '"';
                if (Yii::$app->getUser()->getIsGuest()) {
                    $url = 'href="#" data-toggle="modal" data-target="#needReg"';
                }
                ?>
                <div class="col-sm-6 col-md-4">
                    <div class="catalog-item">
                        <a class="catalog-cart" <?=$url; ?>>
                            <? if (!empty($gift->photo)): ?>
                                <img class="catalog-item__img img-responsive" src="<?=$gift->photoLink;?>">
                            <? endif; ?>
                            <? if (!empty($gift->points)): ?>
                                <div class="catalog-item__price-wrap">
                                    <div class="catalog-item__price-bg"></div>
                                    <div class="catalog-item__price"><?=$gift->points;?><?= (!empty($gift->points_title) ? ' ' . $gift->points_title : ''); ?></div>
                                </div>
                            <? endif; ?>
                        </a>
                        <div class="catalog-item__title"><a <?=$url; ?>><?=$gift->name;?></a></div>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</section>