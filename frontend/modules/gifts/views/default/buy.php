<?php
/**
 * User: y0rker
 * Date: 12.12.2017
 * Time: 1:13
 */

$this->title = 'Магазин поощрений';

$this->params['layout-title'] = 'Выбор поощрения';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="vote-end" style="margin-top: 30px; margin-bottom: 30px;">Спасибо за ваш заказ #<?= $userGift->id; ?></div>
        </div>
        <div class="col-md-12">
            <div class="col-md-4 offset-md-3" style="margin: 0 auto; float: none;">
                <div class="catalog-item">
                    <div class="catalog-cart">
                        <? if (!empty($model->photo)): ?>
                            <img class="catalog-item__img img-responsive" src="<?=$model->photoLink;?>">
                        <? endif; ?>
                        <? if (!empty($model->points)): ?>
                            <div class="catalog-item__price-wrap">
                                <div class="catalog-item__price-bg"></div>
                                <div class="catalog-item__price"><?=$model->points;?></div>
                            </div>
                        <? endif; ?>
                    </div>
                    <div class="catalog-item__title"><?=$model->name;?></div>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-center"><a class="btn btn-primary" href="/">На главную</a></div>
    </div>
</div>
