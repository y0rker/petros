<?php
namespace frontend\modules\polls\controllers;

use frontend\modules\polls\models\Polls;
use common\modules\polls\models\vote\Vote;
use frontend\modules\polls\Module;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default controller.
 *
 * @property Module module
 */
class DefaultController extends Controller
{

    public $enableCsrfValidation = false;

    public function actionIndex() {
        $dataProvider = new ActiveDataProvider([
            'query' => Polls::find()->published()->notPrivate(),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionView($id)
    {
        $poll = Polls::findOne($id);
        if (empty($poll) || (!empty($poll) && $poll->private && !Yii::$app->user->can('iogv'))) {
            throw new HttpException(404);
        }
        if (Yii::$app->user->isGuest && !$poll->complete) {
            throw new HttpException(401, 'Для доступа к этой странице необходима авторизация');
        }
        if ($poll->complete) {
            $this->layout = 'main-poll';
            return $this->render('vote_already', [
                'model' => $poll,
            ]);
        } else {
            $poll->setScenario('select-votes');
            $vote = Vote::findVotePollByUser($id);
            $this->layout = 'main-poll';
            if ($vote && $vote > 0) {
                return $this->render('vote_already', [
                    'model' => $poll,
                ]);
            } else {
                return $this->render('view', [
                    'model' => $poll,
                    'selected' => Yii::$app->request->get('votes', [])
                ]);
            }
        }
    }

    public function actionVote($id) {
        $poll = Polls::findOne($id);
        if (empty($poll) || (!empty($poll) && $poll->private && !Yii::$app->user->can('iogv'))) {
            throw new HttpException(404);
        }
        if (Yii::$app->user->isGuest) {
            throw new HttpException(401, 'Для доступа к этой странице необходима авторизация');
        }
        $poll->setScenario('select-votes');
        $vote = new Vote();
        $vote->user_id = Yii::$app->user->id;
        if ($poll->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($poll);
            } else {
                $vote->votes_array = $poll->votes_array;
                if ($poll->validate()) {
                    if ($vote->validate()) {
                        $this->layout = 'main-poll';
                        $vote->comment = '';
                        return $this->render('vote', [
                            'model' => $poll,
                            'vote'  => $vote
                        ]);
                    }
                }
            }
        }
        return $this->redirect(['/polls/default/view', 'id' => $id]);
    }

    public function actionSuccess($id) {
        $poll = Polls::findOne($id);
        if (empty($poll) || (!empty($poll) && $poll->private && !Yii::$app->user->can('iogv'))) {
            throw new HttpException(404);
        }
        if (Yii::$app->user->isGuest) {
            throw new HttpException(401, 'Для доступа к этой странице необходима авторизация');
        }
        $vote = new Vote(['scenario' => 'save']);
        $vote->poll_id = $id;
        $vote->user_id = Yii::$app->user->id;
        if ($vote->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($vote);
            } elseif ($vote->validate()) {
                $vote->created_at = time();
                $vote->accepted = false;
                if ($vote->save()) {
                    return $this->render('success');
                }
            }
        }

        throw new HttpException(403);
    }

}