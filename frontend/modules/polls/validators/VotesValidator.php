<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 22.05.2017
 * Time: 20:09
 */

namespace frontend\modules\polls\validators;


use common\modules\polls\helpers\ParamsHelper;
use common\modules\polls\models\PollsParams;
use frontend\modules\polls\models\Polls;
use yii\validators\Validator;

class VotesValidator extends Validator
{

    /**
     * @param Polls $model
     * @param string $attribute
     * @return void
     */
    public function validateAttribute($model, $attribute)
    {
        $params = PollsParams::getParamsArrayByPollId($model->id);
        $values = ParamsHelper::getValue('STEP_1', 'COUNT_MAX_SELECT', $params);
        if (!is_array($values)) {
            $need_count = $values;
            $count_votes = 0;
            if (is_array($model->votes_array)) {
                foreach ($model->votes_array AS $vote) {
                    if ($vote > 0) {
                        $count_votes++;
                    }
                }
            }
            if ($count_votes < 1) {
                $this->addError($model, 'votes_validator', 'Необходимо выбрать хотя бы 1 вариант');
                return;
            }
            if ($count_votes > $need_count) {
                $this->addError($model, 'votes_validator', 'Необходимо максимум выбранных вариантов: '.$need_count);
                return;
            } else {
                return;
            }
        }
        $this->addError($model, 'votes_validator', 'Не верное количество необходимых вариантов');
        return;
    }
}