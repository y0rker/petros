<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.05.2017
 * Time: 18:48
 */

namespace frontend\modules\polls\models;


use frontend\modules\polls\validators\VotesValidator;

class Polls extends \common\modules\polls\models\Polls
{
    public $votes_array = [];
    public $votes_validator;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        return array_merge($rules,[
            [['votes_array'], 'required'],
            [['votes_array'], VotesValidator::className()]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['select-votes'] = [
            'votes_array',
            'votes_validator'
        ];

        return $scenarios;
    }
}