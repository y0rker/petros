<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.01.2017
 * Time: 15:18
 */

namespace frontend\modules\polls\widgets\LastPolls;


use common\modules\polls\models\Polls;
use yii\base\Widget;

class LastPolls extends Widget
{
    public $count = 6;

    /** @var bool */
    public $current = false;

    /** @var bool */
    public $allPage = false;

    /**
     * @inheritdoc
     */
    public function run()
    {

        $models = Polls::findLastPollsByCount($this->count, $this->current);

        // Рендерим представление
        echo $this->render(
            'index',
            [
                'polls'     => $models,
                'current'   => $this->current,
                'allPage'   => $this->allPage
            ]
        );
    }
}