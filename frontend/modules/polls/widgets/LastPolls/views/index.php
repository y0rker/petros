<?php
/**
 * @var \frontend\modules\polls\models\Polls[] $polls
 * @var \yii\web\View $this
 * @var bool $current
 * @var bool $allPage
 */
use common\modules\polls\models\PollsParams;

$size_polls = count($polls);

?>
<div class="<?= ($allPage) ? 'vote-vadim' : 'swiper-wrapper'; ?>">
    <? $i = 1; foreach ($polls AS $poll):
        echo ($allPage) ? '<div class="col-md-4 col-sm-6">' : '';
        $show_modal = (Yii::$app->user->isGuest && !$poll->complete);
        $link = 'href="'.\yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $poll->id]).'"';
        if ($show_modal) {
            $link = 'href="#" data-toggle="modal" data-target="#needReg" data-return="'.yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $poll->id]).'"';
        }
        $params = PollsParams::getParamsArrayByPollId($poll->id);
        ?>
        <?= $this->render('_item', [
            'link'      => $link,
            'poll'      => $poll,
            'params'    => $params,
        ]); ?>
        <? echo ($allPage) ? '</div>' : ''; ?>
    <? $i++; endforeach; ?>
</div>
<? if (!$current && !$allPage): ?>
    <div class="row" style="margin-top: 15px;">
        <div class="col-lg-12 text-center">
            <a href="<?=\yii\helpers\Url::toRoute(['/polls/default/index']);?>">Все голосования</a>
        </div>
    </div>
<? endif; ?>