<?php
/**
 * @var \frontend\modules\polls\models\Polls $poll
 */
use common\modules\polls\helpers\ParamsHelper;

?>
<div class="swiper-slide">
    <a class="vote-slider__item" <?=$link;?>>
        <div class="vote-slider__header">
            <div class="vote-slider__title"><?=$poll->name;?></div>
        </div>
        <div class="vote-slider__body">
            <div class="vote-slider__text">
                <?
                $values = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_FIRST_BLOCK', $params);
                if (!is_array($values)) echo $values;
                ?>
            </div>
        </div>
        <div class="vote-slider__footer">
            <div class="vote-slider__dates">
                <div class="vote-slider__start">
                    <div class="subinfo">Начало</div>
                    <div class="vote-slider__date"><?=Yii::$app->formatter->asDate($poll->date_from, 'dd.MM.yyy');?></div>
                </div>
                <div class="vote-slider__end">
                    <div class="subinfo">Завершение</div>
                    <div class="vote-slider__date"><?=Yii::$app->formatter->asDate($poll->date_to, 'dd.MM.yyy');?></div>
                </div>
            </div>
            <div class="vote-slider__status">
                <div class="vote-slider__status-text <?=(!$poll->complete) ? 'green' : 'red';?>"><?=(!$poll->complete) ? 'Идёт' : 'Завершено';?></div>
                <div class="vote-slider__status-icon <?= ($poll->isUserVoted()) ? 'vote-slider__status-icon--active' : '';?>" <?=$poll->isUserVoted() ? 'title="Вы проголосовали"' : ''; ?>></div>
            </div>
        </div>
    </a>
</div>