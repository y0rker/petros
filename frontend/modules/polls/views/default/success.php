<?php
$this->title = 'Спасибо, ваш голос зарегистрирован!';
?>

<div class="content">
    <section class="block">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="step">
                        <div class="step__item step__item--past">
                            <div class="step__number">1</div>
                            <div class="step__desc">выбор</div>
                        </div>
                        <div class="step__line"></div>
                        <div class="step__item step__item--past">
                            <div class="step__number">2</div>
                            <div class="step__desc">подтверждение</div>
                        </div>
                        <div class="step__line"></div>
                        <div class="step__item step__item--active">
                            <div class="step__number">3</div>
                            <div class="step__desc">завершение</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12 text-center">
            <div class="vote-end">Спасибо за ваш голос</div>
        </div>
        <div class="col-md-12 text-center"><a class="btn btn-primary" href="/?from=success">На главную</a></div>
    </div>
</div>