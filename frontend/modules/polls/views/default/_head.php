<?
use frontend\helpers\UsernameHelper;
use frontend\theme\ThemeAsset;
use yii\helpers\Url;

$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');
$linkUser = 'href="#" data-toggle="modal" data-target="#needReg"';
if (!Yii::$app->user->isGuest) {
    $linkUser = 'href="'. Url::toRoute(['/backend/']).'"';
}
?>
<div class="col-md-12">
    <div class="nav">
        <div class="nav__items">
            <div class="nav__item"><a class="nav__link" <?= $linkUser;?>><?=UsernameHelper::getCabinetClass();?></a></div>
            <div class="nav__item"><a class="nav__link" href="<?=Url::toRoute(['/site/default/introduction']);?>">Вступительное слово</a></div>
        </div>
        <div class="nav__items nav__items--logo">
            <div class="nav__item nav__item--logo"><a class="link-img" href="/"><img src="<?=$assetUrl;?>/img/logo.png"></a></div>
        </div>
        <div class="nav__items">
            <div class="nav__item"><a class="nav__link" href="<?=Url::toRoute(['/gifts/default/index']);?>">Магазин поощрений</a></div>
            <div class="nav__item"><a class="nav__link" href="<?=Url::toRoute(['/news/default/index']);?>">Новости</a></div>
        </div>
    </div>
</div>
