<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */


use frontend\modules\polls\widgets\LastPolls\LastPolls;

$this->title = 'Голосования';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="block">
    <div class="container-fluid vote-slider__container">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="vote-slider__content vote-slider__content--all vote-slider__content--active">
                        <div class="swiper-container">
                            <div class="row">
                                <?=LastPolls::widget([
                                    'current'   => false,
                                    'allPage'   => true,
                                    'count'     => 0
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>