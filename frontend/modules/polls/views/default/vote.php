<?php

/**
 * @var \yii\web\View $this
 * @var \frontend\modules\polls\models\Polls $model
 * @var \common\modules\polls\models\vote\Vote $vote
 */

use common\modules\polls\helpers\ParamsHelper;
use common\modules\polls\models\PollsParams;
use frontend\helpers\UsernameHelper;
use frontend\theme\ThemeAsset;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Шаг 2';

$linkUser = 'href="#" data-toggle="modal" data-target="#needReg"';
if (!Yii::$app->user->isGuest) {
    $linkUser = 'href="'. Url::toRoute(['/backend/']).'"';
}
$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');

$params = PollsParams::getParamsArrayByPollId($model->id);
?>
<div class="wrapper">
    <section class="header">
        <div class="container">
            <div class="row">
                <?=$this->render('_head'); ?>
                <div class="col-md-8 col-md-offset-2">
                    <div class="offer">
                        <div class="offer__title"><?=(!empty($model->sub_name)) ? $model->sub_name : $model->name;?></div>
                        <div class="offer__subtitle">
                            <?
                            $values = ParamsHelper::getValue('STEP_2', 'MAIN_TEXT', $params);
                            if (!is_array($values)) echo $values;
                            ?>
                        </div>
                    </div>
                    <?
                    $values = ParamsHelper::getValue('STEP_2', 'SUB_FIELDS', $params);
                    if (is_array($values)): ?>
                        <ul class="vote-list" style="font-size: 18px;">
                            <? foreach ($values AS $field): ?>
                                <li class="vote-list__item"><?= $field; ?></li>
                            <? endforeach; ?>
                        </ul>
                        <?
                    elseif (!empty($values)):?>
                        <ul class="vote-list" style="font-size: 18px;">
                            <li class="vote-list__item"><?= $values; ?></li>
                        </ul>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </section>
    <div class="content">
        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="step">
                            <div class="step__item step__item--past">
                                <div class="step__number">1</div>
                                <div class="step__desc">выбор</div>
                            </div>
                            <div class="step__line"></div>
                            <div class="step__item step__item--active">
                                <div class="step__number">2</div>
                                <div class="step__desc">подтверждение</div>
                            </div>
                            <div class="step__line"></div>
                            <div class="step__item">
                                <div class="step__number">3</div>
                                <div class="step__desc">завершение</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'options' => [
                'class' => 'block',
            ],
            'action'    => \yii\helpers\Url::toRoute(['/polls/default/success', 'id' => $model->id])
        ]);
        ?>
            <section class="block">
                <div class="container">
                    <h2 class="block-title">Ваш выбор</h2>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="row">
                                <? foreach ($vote->votes AS $v): ?>
                                    <div class="col-sm-6">
                                        <div class="vote-selected__item">
                                            <? if (!empty($v->photo)): ?>
                                                <img class="vote-selected__photo img-circle" src="<?=$v->urlAttribute('photo');?>" width="44" height="44">
                                            <? endif; ?>
                                            <span class="vote-selected__title"><?=$v->name;?></span>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                            <? if ($model->need_comment): ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="vote-comment">
                                            <div class="vote-comment__head"><a href="<?=\yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $model->id, 'votes' => $vote->votes_array]);?>">Изменить моё решение</a><span class="subinfo">оставить комментарий</span></div>
                                            <div class="vote-comment__body">
                                                <?=$form->field($vote, 'comment')->textarea([
                                                    'class' => 'vote-comment__textarea',
                                                    'rows'  => 5
                                                ])->label(false);?>
                                            </div>
                                            <div class="vote-comment__footer"><span class="subinfo">моё мнение</span></div>
                                        </div>
                                    </div>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            </section>
            <div class="row text-center" style="margin-bottom: 25px; margin-top: 25px;"><a href="<?=\yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $model->id, 'votes' => $vote->votes_array]);?>" class="">Изменить своё решение</a></div>
            <section class="block">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <button class="btn btn-primary" type="submit">Проголосовать</button>
                        </div>
                    </div>
                </div>
            </section>
        <?php
        $vote->votes_array = json_encode($vote->votes_array);
        echo $form->field($vote, 'votes_array')->hiddenInput()->label(false);
        ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>