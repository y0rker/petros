<?php

/**
 * @var \yii\web\View $this
 * @var \frontend\modules\polls\models\Polls $model
 */

use common\modules\polls\helpers\ParamsHelper;
use common\modules\polls\models\PollsParams;
use frontend\helpers\UsernameHelper;
use frontend\theme\ThemeAsset;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Шаг 1';
$linkUser = 'href="#" data-toggle="modal" data-target="#needReg"';
if (!Yii::$app->user->isGuest) {
    $linkUser = 'href="'. Url::toRoute(['/backend/']).'"';
}
$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');

$params = PollsParams::getParamsArrayByPollId($model->id);
?>
<div class="wrapper">
    <section class="header">
        <div class="container">
            <div class="row">
                <?=$this->render('_head'); ?>
                <div class="col-md-8 col-md-offset-2">
                    <div class="offer">
                        <div class="offer__title"><?=(!empty($model->sub_name)) ? $model->sub_name : $model->name;?></div>
                        <div class="offer__subtitle">
                            <?
                            $values = ParamsHelper::getValue('STEP_1', 'MAIN_TEXT', $params);
                            if (!is_array($values)) echo $values; ?>
                        </div>
                    </div>
                    <?
                    $values = ParamsHelper::getValue('STEP_1', 'SUB_FIELDS', $params);
                    if (is_array($values)): ?>
                        <ul class="vote-list" style="font-size: 18px;">
                            <? foreach ($values AS $field): ?>
                                <li class="vote-list__item"><?= $field; ?></li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
                </div>
            </div>
        </div>
    </section>
    <?php
    $gallery = $model->gallery;
    if (!empty($gallery)):
    ?>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-width="100%" data-maxheight="650" data-fit="contain" data-loop="true" data-autoplay="true">
                    <? foreach ($gallery AS $gal): ?>
                        <img src="<?=$gal->urlAttribute('image');?>">
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <? endif; ?>
    <div class="content">
        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="step">
                            <div class="step__item step__item--active">
                                <div class="step__number">1</div>
                                <div class="step__desc">выбор</div>
                            </div>
                            <div class="step__line"></div>
                            <div class="step__item">
                                <div class="step__number">2</div>
                                <div class="step__desc">подтверждение</div>
                            </div>
                            <div class="step__line"></div>
                            <div class="step__item">
                                <div class="step__number">3</div>
                                <div class="step__desc">завершение</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php $form = ActiveForm::begin([
            'enableClientValidation'    => false,
            'enableAjaxValidation'      => true,
            'validateOnChange'          => false,
            'validateOnBlur'            => false,
            'validateOnType'            => false,
            'id'    => 'voteForm',
            'options' => [
                'class' => 'block form-vote',
            ],
            'action' => \yii\helpers\Url::toRoute(['/polls/default/vote', 'id' => $model->id])
        ]); ?>
        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="vote-table-head">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="vote__title"><?
                                        $values = ParamsHelper::getValue('STEP_1', 'MAIN_TEXT_POLL', $params);
                                        if (!is_array($values)) echo $values; ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="list-pager">
                                        <div class="vote__count text-right">
                                            <div class="vote__count__number"><span class="vote__count__current">0</span>/<span
                                                        class="vote__count__max"><?
                                                    $values = ParamsHelper::getValue('STEP_1', 'COUNT_MAX_SELECT', $params);
                                                    if (!is_array($values)) echo $values; ?></span></div>
                                            <div class="vote__count__subtitle">голосов</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="vote-table">
                            <input name="Polls[votes_array][]" type="hidden" value="0" />
                            <? foreach ($model->votes AS $vote):
                                $template = '<div class="vote-table__row">';
                                $template .= '<div class="vote-table__cell vote-table__cell--checkbox">
                                                        <label class="checkbox-circle">
                                                            <input name="Polls[votes_array][]" type="checkbox" value="' . $vote->id . '" ' . (in_array($vote->id, $selected) ? 'checked="checked"' : '') . '>
                                                            <div class="checkbox-circle__bg"></div>
                                                        </label>
                                                    </div>';
                                if (!empty($vote->photo)) {
                                    $template .= '<div class="vote-table__cell vote-table__cell--photo"><a href="#" data-toggle="modal" data-target="#vote' . $vote->id . '"><img class="img-circle" src="' . $vote->urlAttribute('photo') . '" width="126" height="126"></a></div>';
                                }
                                $template .= '<div class="vote-table__cell vote-table__cell--title"><span class="vote-table__title">' . $vote->name . '</span></div>';
                                $template .= '<div class="vote-table__cell vote-table__cell--metro">&nbsp;</div>';
                                if (!empty($vote->description)) {
                                    $template .= '<div class="vote-table__cell vote-table__cell--info"><a href="#" data-toggle="modal" data-target="#vote' . $vote->id . '"><i class="vote-table__info"></i></a></div>';
                                }
                                $template .= '</div>';
                                ?>
                                <?= $form->field($model, 'votes_array[' . $vote->id . ']', [
                                'template' => $template,
                                'options' => [
                                    'class' => 'list__row',
                                    'tag' => 'div',
                                ],
                            ])->checkbox([
                                'value' => 1
                            ], false); ?>
                            <? endforeach; ?>
                            <?=$form->field($model, 'votes_validator', [
                                'options'   => [
                                    'class' => 'text-center',
                                    'style' => 'margin-top: 85px;'
                                ]
                            ])->hiddenInput()->label(false); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <button class="btn btn-primary" type="submit">Далее</button>
                    </div>
                </div>
            </div>
        </section>
        <?php ActiveForm::end(); ?>
        <? foreach ($model->votes AS $vote):
            if (empty($vote->description)) continue;
            ?>
            <div class="modal inmodal" id="vote<?=$vote->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12" style="color: #000;">
                                    <h2><b><?=$vote->name;?></b><button type="button" class="close" data-dismiss="modal" style="float: right;"><span aria-hidden="true">×</span><span class="sr-only">Закрыть</span></button></h2>
                                    <? if (!empty($vote->photo)): ?>
                                        <img src="<?=$vote->urlAttribute('photo');?>" alt="" width="100%" style="margin-bottom: 25px;" class="img-responsive">
                                    <? endif; ?>
                                    <?=$vote->description;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach; ?>
    </div>
</div>