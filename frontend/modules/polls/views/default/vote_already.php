<?php
/**
 * @var \frontend\modules\polls\models\Polls $model
 * @var \yii\web\View $this
 */
use common\modules\polls\helpers\ParamsHelper;
use common\modules\polls\models\PollsParams;
use frontend\theme\ThemeAsset;
use yii\helpers\Url;

$this->title = $model->name;

$linkUser = 'href="#" data-toggle="modal" data-target="#needReg"';
if (!Yii::$app->user->isGuest) {
    $linkUser = 'href="'. Url::toRoute(['/backend/']).'"';
}
$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');

$params = PollsParams::getParamsArrayByPollId($model->id);
$block_2_text = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_SECOND_BLOCK', $params);
$block_2_link = ParamsHelper::getValue('MAIN_BLOCKS', 'LINK_SECOND_BLOCK', $params);

$block_3_text = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_THIRD_BLOCK', $params);
$block_3_link = ParamsHelper::getValue('MAIN_BLOCKS', 'LINK_THIRD_BLOCK', $params);

$gallery = $model->gallery;
$metaImg = '';
if (!empty($gallery)) {
    foreach ($gallery AS $gal) {
        $metaImg = $gal->urlAttribute('image');
        break;
    }
}

$this->params['META_IMAGE'] = Yii::$app->urlManager->getHostInfo().$metaImg;

?>
<section class="header">
    <div class="container">
        <div class="row">
            <?=$this->render('_head'); ?>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="vote-result">
                    <div class="vote-result__header">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h1 class="vote-result__title"><?=$model->name;?></h1>
                            </div>
                        </div>
                    </div>
                    <div class="vote-result__body">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="vote-result__desc">
                                    <?
                                    $values = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_FIRST_BLOCK', $params);
                                    if (!is_array($values))  {
                                        echo $values;
                                        $this->params['DESCRIPTION'] = str_replace('<br>', ' ', $values);
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vote-result__footer">
                        <div class="row">
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <div class="vote-result__block vote-result__block--date">
                                    <div class="subinfo">Даты проведения</div>
                                    <div class="vote-result__text">с <?=Yii::$app->formatter->asDate($model->date_from, 'dd.MM.yyy');?> по <?=Yii::$app->formatter->asDate($model->date_to, 'dd.MM.yyy');?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="vote-result__block vote-result__block--status">
                                    <div class="subinfo">Статус</div>
                                    <div class="vote-result__text <?= ($model->complete) ? 'red' : 'green'; ?>"><?= ($model->complete) ? 'ЗАВЕРШЕНО' : 'Идёт'; ?></div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <? if ($model->isUserVoted()): ?>
                                    <div class="vote-result__block vote-result__block--state">
                                        <div class="vote-result__text green">Вы проголосовали</div>
                                    </div>
                                <? endif; ?>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-1">

                            </div>
                            <div class="col-md-10">
                                <div class="ya-share2"
                                    data-services="vkontakte,facebook,twitter,telegram"
                                    data-title="<?=$this->title;?>"
                                    <? if (!empty($this->params['DESCRIPTION'])): ?>
                                        data-description="<?=$this->params['DESCRIPTION'];?>"
                                    <? endif; ?>
                                    <? if (!empty($metaImg)): ?>
                                        data-image="<?=Yii::$app->urlManager->getHostInfo().$metaImg; ?>"
                                    <? endif; ?>
                                    data-url="<?=Url::current([], true);?>">
                                </div>
                            </div>
                            <div class="col-md-1">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<? if ($model->complete): ?>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Итоги голосования</h2>
                    <p><?=(!is_array($block_2_text)) ? $block_2_text : ''; ?></p>
                    <? if (!empty($block_2_link) && !is_array($block_2_link)):
                        $link_text = (!empty($block_2_link_text)) ? $block_2_link_text : 'ссылка';
                        ?>
                        <a href="<?=$block_2_link;?>"><?=$link_text;?></a>
                    <? endif; ?>
                </div>
            </div>
            <? if (!is_array($block_3_text)): ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="vote-solution">
                            <h2 class="orange">Решение</h2>
                            <p><?=(!is_array($block_3_text)) ? $block_3_text : ''; ?></p>
                            <? if (!empty($block_3_link) && !is_array($block_3_link)):
                                $link_text = (!empty($block_3_link_text)) ? $block_3_link_text : 'ссылка';
                                ?>
                                <a href="<?=$block_3_link;?>"><?=$link_text;?></a>
                            <? endif; ?>
                        </div>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
<? endif; ?>