<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \frontend\modules\polls\models\Polls $model
 */


use common\modules\polls\helpers\ParamsHelper;
use common\modules\polls\models\PollsParams;


$this->title = $model->name;
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="block">
    <div class="container">
        <h2 class="block-title"><?=$this->title;?></h2>
        <?php
            $params = PollsParams::getParamsArrayByPollId($model->id);
            $block_2_text = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_SECOND_BLOCK', $params);
            $block_2_link = ParamsHelper::getValue('MAIN_BLOCKS', 'LINK_SECOND_BLOCK', $params);

            $block_3_text = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_THIRD_BLOCK', $params);
            $block_3_link = ParamsHelper::getValue('MAIN_BLOCKS', 'LINK_THIRD_BLOCK', $params);
        ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="block-info block-info--border">
                    <div class="block-info__wrapper">
                        <h3 class="block-info__title"><?=$model->name;?></h3>
                        <p><?
                            $values = ParamsHelper::getValue('MAIN_BLOCKS', 'TEXT_FIRST_BLOCK', $params);
                            if (!is_array($values)) echo $values;
                            ?></p>
                        <p><span class="subinfo"><?
                                $values = ParamsHelper::getValue('MAIN_BLOCKS', 'SUB_TEXT_FIRST_BLOCK', $params);
                                if (!is_array($values)) echo $values;
                                ?></span></p>
                        <p class="block-info__more"></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <? if (!empty($block_2_link) && !is_array($block_2_link)):
                    $block_2_link_text = ParamsHelper::getValue('MAIN_BLOCKS', 'LINK_TEXT_SECOND_BLOCK', $params);
                    $link_text = (!empty($block_2_link_text)) ? $block_2_link_text : 'ссылка';
                    ?>
                    <a href="<?=$block_2_link;?>" class="block-info block-info--border block-info--animated">
                        <div class="block-info__wrapper">
                            <h3 class="block-info__title">Итоги голосования</h3>
                            <p><?=(!is_array($block_2_text)) ? $block_2_text : ''; ?></p>
                            <p class="block-info__more"><?=$link_text;?></p>
                        </div>
                    </a>
                <? else: ?>
                    <div class="block-info block-info--border">
                        <div class="block-info__wrapper">
                            <h3 class="block-info__title">Итоги голосования</h3>
                            <p><?=(!is_array($block_2_text)) ? $block_2_text : ''; ?></p>
                            <p class="block-info__more"></p>
                        </div>
                    </div>
                <? endif; ?>
            </div>
            <div class="col-sm-4">
                <? if (!empty($block_3_link) && !is_array($block_3_link)):
                    $block_3_link_text = ParamsHelper::getValue('MAIN_BLOCKS', 'LINK_TEXT_THIRD_BLOCK', $params);
                    $link_text = (!empty($block_3_link_text)) ? $block_3_link_text : 'ссылка';
                    ?>
                    <a href="<?=$block_3_link;?>" class="block-info block-info--border block-info--animated">
                        <div class="block-info__wrapper">
                            <h3 class="block-info__title">Решение</h3>
                            <p><?=(!is_array($block_3_text)) ? $block_3_text : ''; ?></p>
                            <p class="block-info__more"><?=$link_text;?></p>
                        </div>
                    </a>
                <? else: ?>
                    <div class="block-info block-info--border">
                        <div class="block-info__wrapper">
                            <h3 class="block-info__title">Решение</h3>
                            <p><?=(!is_array($block_3_text)) ? $block_3_text : ''; ?></p>
                            <p class="block-info__more"></p>
                        </div>
                    </div>
                <? endif; ?>
            </div>
        </div>
    </div>
</section>