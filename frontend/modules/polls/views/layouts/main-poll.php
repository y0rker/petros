<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use frontend\theme\ThemeAsset;
use yii\helpers\Url;

$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>
    <?php
    $linkUser = 'href="#" data-toggle="modal" data-target="#needReg"';
    if (!Yii::$app->user->isGuest) {
        $linkUser = 'href="'. Url::toRoute(['/backend/']).'"';
    }
    ?>
    <? if (Yii::$app->params['test']): ?>
        <div class="test-label">Тестовая эксплуатация</div>
    <? endif; ?>
    <?=$content;?>
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="footer-content"><img class="footer-logo" src="<?=$assetUrl;?>/img/spb.png">Администрация<br> Петроградского района<br> Санкт-Петербурга</div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="footer-content"><span class="small">© При полном или частичном использовании материалов ссылка на портал «Голос Петроградки» обязательна.</span></div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="footer-content footer-content--links"><a href="<?=Url::toRoute(['/pages/default/view', 'page' => 'term']);?>">Пользовательское соглашение</a><span>Обратная связь:</span><a href="mailto:support@golos.adc.spb.ru">support@golos.adc.spb.ru</a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-socials">
                        <div class="socials"><a class="socials__item socials__item--vk" target="_blank" href="https://vk.com/golospetrogradki"></a><a class="socials__item socials__item--fb" target="_blank" href="https://facebook.com/golospetrogradki"></a><a class="socials__item socials__item--ig" target="_blank" href="https://www.instagram.com/golos_petrogradki/"></a><a class="socials__item socials__item--tw" target="_blank" href="https://twitter.com/GolosPetrograd"></a><a class="socials__item socials__item--tm" target="_blank" href="https://t.me/GolosPetrograd"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <? if (Yii::$app->user->isGuest): ?>
        <?=\frontend\modules\site\widgets\LoginWidget\LoginWidget::widget();?>
    <? endif; ?>
    <?=\common\widgets\MetricsWidget\MetricsWidget::widget(); ?>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>