<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

use yii\widgets\ListView;
$this->title = 'Новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="block">
    <div class="container">
        <h2 class="block-title"><?=$this->title;?></h2>
        <div class="row">
            <?= ListView::widget(
                [
                    'dataProvider' => $dataProvider,
                    'layout' => "{items}\n{pager}",
                    'itemView' => '_index_item',
                    'options' => [
                        'class' => 'row'
                    ],
                    'itemOptions' => [
                        'class' => 'col-sm-12 news__item-wrap',
                        'tag' => 'div'
                    ]
                ]
            ); ?>
        </div>
    </div>
</section>