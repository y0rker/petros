<?php

/**
 * News page view.
 *
 * @var \yii\web\View $this View
 * @var \frontend\modules\news\models\News $model Model
 */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = $model['title'];
$this->params['breadcrumbs'] = [
    [
        'label' => 'Новости',
        'url' => ['index']
    ],
    $this->title
]; ?>
<section class="block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="news-single">
                    <h1><?= $model->title ?></h1>
                    <div class="text">
                        <? /*
                        <p><span class="subinfo"><?= $model->created ?></span></p>
                        */ ?>
                        <?php if ($model->preview_url) : ?>
                            <?= Html::img(
                                $model->urlAttribute('preview_url'),
                                ['class' => 'img-responsive img-blog', 'width' => '730', 'height' => '290', 'alt' => $model->title]
                            ) ?>
                        <?php endif; ?>
                        <?= $model->content ?>
                    </div>
                </div>
                <div class="block-footer">
                    <div class="block-footer__item"><a href="<?=Url::toRoute(['/news/default/index']);?>">Перейти на список новостей</a></div>
                </div>
            </div>
        </div>
    </div>
</section>