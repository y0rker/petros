<?php

/**
 * News list item view.
 *
 * @var \yii\web\View $this View
 * @var \frontend\modules\news\models\News $model Model
 */
use yii\helpers\Url;

?>
<div class="col-sm-12 news__item-wrap">
    <a class="news__item block-info block--animated" href="<?=Url::toRoute(['/news/default/view', 'id' => $model->id, 'alias' => $model->alias]);?>">
        <div class="block__wrapper">
            <div class="news__title">
                <p><?=$model->title;?></p>
            </div>
            <p><span class="subinfo"><?= $model->news_date; ?></span></p>
        </div>
    </a>
</div>