<?php

namespace frontend\modules\news\controllers;

use frontend\modules\news\models\News;
use frontend\modules\news\Module;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\HttpException;

/**
 * Default controller.
 *
 * @property Module module
 */
class DefaultController extends Controller
{

    /**
     * News list page.
     */
    function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => News::find()->published()->notPrivate(),
            'pagination' => [
                'pageSize' => $this->module->recordsPerPage
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * News page.
     *
     * @param integer $id News ID
     * @param string $alias News alias
     *
     * @return mixed
     *
     * @throws \yii\web\HttpException 404 if blog was not found
     */
    public function actionView($id, $alias)
    {
        if (($model = News::findOne(['id' => $id, 'alias' => $alias])) !== null) {

            if ($model->private && !Yii::$app->user->can('iogv')) {
                throw new HttpException(404);
            }

            $this->counter($model);

            return $this->render('view', [
                'model' => $model
            ]);
        } else {
            throw new HttpException(404);
        }
    }

    /**
     * Update blog views counter.
     *
     * @param News $model Model
     */
    protected function counter($model)
    {
        $cookieName = 'blogs-views';
        $shouldCount = false;
        $views = Yii::$app->request->cookies->getValue($cookieName);

        if ($views !== null) {
            if (is_array($views)) {
                if (!in_array($model->id, $views)) {
                    $views[] = $model->id;
                    $shouldCount = true;
                }
            } else {
                $views = [$model->id];
                $shouldCount = true;
            }
        } else {
            $views = [$model->id];
            $shouldCount = true;
        }

        if ($shouldCount === true) {
            if ($model->updateViews()) {
                Yii::$app->response->cookies->add(new Cookie([
                    'name' => $cookieName,
                    'value' => $views,
                    'expire' => time() + 86400 * 365
                ]));
            }
        }
    }
}
