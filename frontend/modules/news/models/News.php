<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.05.2017
 * Time: 18:54
 */

namespace frontend\modules\news\models;


use Yii;

/**
 * Class News
 * @package frontend\modules\news\models
 *
 * @property string created
 * @property string updated
 */
class News extends \common\modules\news\models\News
{
    /**
     * @var string Created date
     */
    private $_created;

    /**
     * @var string Updated date
     */
    private $_updated;

    /**
     * @return string Created date
     */
    public function getCreated()
    {
        if ($this->_created === null) {
            $this->_created = Yii::$app->formatter->asDate($this->created_at);
        }
        return $this->_created;
    }

    /**
     * @return string Updated date
     */
    public function getUpdated()
    {
        if ($this->_updated === null) {
            $this->_updated = Yii::$app->formatter->asDate($this->updated_at);
        }
        return $this->_updated;
    }

    /**
     * Update views counter.
     *
     * @return boolean Whether views counter was updated or not
     */
    public function updateViews()
    {
        return $this->updateCounters(['views' => 1]);
    }
}