<?php
/**
 * @var \frontend\modules\news\models\News[] $news
 */

use yii\helpers\Url;
?>
<div class="row">
    <? foreach ($news AS $new): ?>
        <div class="col-sm-12 news__item-wrap">
            <a class="news__item block-info block--animated" href="<?=Url::toRoute(['/news/default/view', 'id' => $new->id, 'alias' => $new->alias]);?>">
                <div class="block__wrapper">
                    <div class="news__title">
                        <p><?=$new->title;?></p>
                    </div>
                    <p><span class="subinfo"><?= $new->news_date; ?></span></p>
                </div>
            </a>
        </div>
    <? endforeach; ?>
    <div class="col-sm-12 news__item-wrap">
        <div class="block-footer">
            <div class="block-footer__item"><a href="<?=Url::toRoute(['/news/default/index']);?>">все новости</a></div>
        </div>
    </div>
</div>