<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.01.2017
 * Time: 16:04
 */

namespace frontend\modules\news\widgets\LastNews;


use frontend\modules\news\models\News;
use yii\base\Widget;

class LastNews extends Widget
{

    public $count = 3;

    /**
     * @inheritdoc
     */
    public function run()
    {

        $models = News::findLastNewsByCount($this->count);

        // Рендерим представление
        echo $this->render(
            'index',
            [
                'news'  => $models,
            ]
        );
    }
}