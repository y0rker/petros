<div class="modal-header">
    <h4 class="modal-title">Регистрация
    </h4>
    <h5 class="modal-subtitle">Придумайте пароль для вашей учетной записи
    </h5>
</div>
<div class="modal-body">
    <form>
        <div class="form-group">
            <label class="label">Пароль (Минимум 6 символов)</label>
            <input class="form-control" type="password" id="registerPhonePassword">
        </div>
        <div class="form-group">
            <label class="label">Подтвердите пароль (Минимум 6 символов)</label>
            <input class="form-control" type="password" id="registerPhoneRePassword">
        </div>
    </form>
</div>
<div class="modal-footer">
    <p>
    <div class="btn-group">
        <button class="btn btn-modal" onclick="login.send();">Далее</button>
    </div>
    </p>
</div>