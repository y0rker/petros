<div class="modal-header">
    <h4 class="modal-title">восстановление пароль
    </h4>
    <h5 class="modal-subtitle">На телефон <b><?=$phone;?></b> отправлен код подтверждения с помощью которого можно изменить пароль
    </h5>
</div>
<div class="modal-body">
    <div class="form-group">
        <label class="label">Код</label>
        <input class="form-control" type="number" id="lostPasswordCode">
    </div>
    <div class="form-group">
        <label class="label">Пароль (Минимум 6 символов)</label>
        <input class="form-control" type="password" id="lostPasswordPassword">
    </div>
    <div class="form-group">
        <label class="label">Подтверждение пароля (Минимум 6 символов)</label>
        <input class="form-control" type="password" id="lostPasswordRePassword">
    </div>
</div>
<div class="modal-footer">
    <p>
    <div class="btn-group">
        <button class="btn btn-modal" onclick="login.lostPasswordNewPassword();">изменить</button>
    </div>
    </p>
</div>