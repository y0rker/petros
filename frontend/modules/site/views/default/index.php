<?php
/**
 * @var \yii\web\View $this
 */

use frontend\theme\ThemeAsset;

$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');
$this->title = 'Голос Петроградки';

$this->params['DESCRIPTION'] = Yii::t('polls', 'MAIN_DESCRIPTION');
$this->params['META_IMAGE'] = Yii::$app->urlManager->getHostInfo().$assetUrl.'img/fb_banner.jpg';

?>
<section class="block">
    <div class="container">
        <h2 class="block-title">О голосованиях</h2>
        <div class="row">
            <div class="col-sm-4">
                <div class="block-info">
                    <div class="about__item">
                        <div class="about__header"><img class="about__img" src="<?=$assetUrl;?>/img/about/time.png" width="40" height="61">
                            <div class="about__title">Даты проведения голосований</div>
                        </div>
                        <div class="about__info">22 мая — 01 ноября<br> 2017 года</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block-info">
                    <div class="about__item">
                        <div class="about__header"><img class="about__img" src="<?=$assetUrl;?>/img/about/people.png" width="93" height="61">
                            <div class="about__title">Участники голосований</div>
                        </div>
                        <div class="about__info">Жители Петроградского района Санкт-Петербурга</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="block-info">
                    <div class="about__item">
                        <div class="about__header"><img class="about__img" src="<?=$assetUrl;?>/img/about/format.png" width="49" height="61">
                            <div class="about__title">Формат голосований</div>
                        </div>
                        <div class="about__info">Онлайн-голосования</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block">
    <div class="container-fluid vote-slider__container" id="current_polls">
        <div class="row">
            <div class="col-md-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="vote-slider-control">
                                <div class="vote-slider-control__item vote-slider-control__item--actual vote-slider-control__item--active">
                                    <div class="vote-slider-control__item__text">
                                        текущие голосования
                                    </div>
                                </div>
                                <div class="vote-slider-control__item vote-slider-control__item--all">
                                    <div class="vote-slider-control__item__text">
                                        Все голосования
                                    </div>
                                </div>
                            </div>
                            <div class="vote-slider">
                                <div class="row">
                                    <div class="vote-slider__content vote-slider__content--actual vote-slider__content--active">
                                        <div class="swiper-container" id="vote-slider-actual">
                                            <?=\frontend\modules\polls\widgets\LastPolls\LastPolls::widget([
                                                'current'   => true,
                                                'count'     => 20
                                            ]); ?>
                                            <div class="swiper-button-prev" id="vote-slider-actual-prev"></div>
                                            <div class="swiper-button-next" id="vote-slider-actual-next"></div>
                                        </div>
                                    </div>
                                    <div class="vote-slider__content vote-slider__content--all">
                                        <div class="swiper-container" id="vote-slider-all">
                                            <?=\frontend\modules\polls\widgets\LastPolls\LastPolls::widget([
                                                'current'   => false,
                                                'count'     => 20
                                            ]); ?>
                                            <div class="swiper-button-prev" id="vote-slider-all-prev"></div>
                                            <div class="swiper-button-next" id="vote-slider-all-next"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="block">
    <div class="container">
        <h2 class="block-title">Новости</h2>
        <?=\frontend\modules\news\widgets\LastNews\LastNews::widget(); ?>
    </div>
</section>
<script>
    window.onload = function () {
        var slider_actual = new Swiper('#vote-slider-actual', {
            direction: 'horizontal',
            loop: false,
            centeredSlides: false,
            prevButton: '#vote-slider-actual-prev',
            nextButton: '#vote-slider-actual-next',
            setWrapperSize: true,
            slidesPerView: 3,
            breakpoints: {
                // when window width is <= 640px
                640: {
                    slidesPerView: 1
                },
                // when window width is <= 1024px
                1024: {
                    slidesPerView: 2
                }
            }
        });

        var slider_all = new Swiper('#vote-slider-all', {
            direction: 'horizontal',
            loop: false,
            centeredSlides: false,
            prevButton: '#vote-slider-all-prev',
            nextButton: '#vote-slider-all-next',
            setWrapperSize: true,
            slidesPerView: 3,
            breakpoints: {
                // when window width is <= 640px
                640: {
                    slidesPerView: 1
                },
                // when window width is <= 1024px
                1024: {
                    slidesPerView: 2
                }
            }
        })

        function toggleVoteSlider(type) {
            if (type === 'all') {
                $('.vote-slider-control__item--actual').removeClass('vote-slider-control__item--active');
                $('.vote-slider-control__item--all').addClass('vote-slider-control__item--active');
                $('.vote-slider__content--actual').removeClass('vote-slider__content--active');
                $('.vote-slider__content--all').addClass('vote-slider__content--active');
                slider_all.update();
            } else if (type == 'actual') {
                $('.vote-slider-control__item--actual').addClass('vote-slider-control__item--active');
                $('.vote-slider-control__item--all').removeClass('vote-slider-control__item--active');
                $('.vote-slider__content--actual').addClass('vote-slider__content--active');
                $('.vote-slider__content--all').removeClass('vote-slider__content--active');
                slider_actual.update();
            }
        };

        $('.vote-slider-control__item--actual').on('click', function () {
            if (!$(this).hasClass('vote-slider-control__item--active')) {
                toggleVoteSlider('actual');
            }
        });

        $('.vote-slider-control__item--all').on('click', function () {
            if (!$(this).hasClass('vote-slider-control__item--active')) {
                toggleVoteSlider('all');
            }
        })
    };
</script>