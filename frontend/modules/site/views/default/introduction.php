<?php
use yii\helpers\Html;


?>
<? if (!empty($page)): ?>
    <?php $this->title = $page->name;?>
    <section class="block">
        <div class="container">
            <h2 class="block-title"><?=$page->name;?></h2>
            <div class="row">
                <div class="col-sm-3">
                    <?php if ($page->photo) : ?>
                        <?= Html::img(
                            $page->urlAttribute('photo'),
                            ['class' => 'img-responsive', 'alt' => $page->first_name.' '.$page->last_name.' '.$page->mid_name]
                        ) ?>
                    <?php endif; ?>
                </div>
                <div class="col-sm-9">
                    <h1><?=$page->subname;?></h1>
                    <div class="text">
                        <?=$page->description;?><br><br>
                        <p><?=$page->first_name.' '.$page->last_name.' '.$page->mid_name;?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>