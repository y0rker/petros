<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 12:37
 */

namespace frontend\modules\site\helpers;


class LoginHelper
{
    const TYPE_LOGIN = 'login';
    const TYPE_REGISTER = 'register';
    const TYPE_REGISTER_ENTER_CODE = 'register_enter_code';
    const TYPE_REGISTER_ENTER_PASSWORD = 'register_enter_password';

    const TYPE_LOST_PHONE = 'lost_phone';
    const TYPE_LOST_CODE = 'lost_code';
    const TYPE_LOST_PASSWORD = 'lost_password';

    const TYPE_LOST_CODE_ALREADY = 'lost_code_already';

    const TYPE_ERROR = 'error';

    /**
     * @param $view
     * @return string
     */
    public static function getJs($view) {
        $str_js = "";
        if ($arr_js = $view->js) {
            ksort($arr_js);
            foreach ($arr_js AS $js) {
                foreach ($js AS $script) {
                    $str_js .= $script;
                }
            }
        }
        $str_js .= '$(\'input[type="tel"]\').inputmask({"mask": "+7 (999) 999-9999"})';
        return $str_js;
    }
}