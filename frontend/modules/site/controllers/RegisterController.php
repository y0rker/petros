<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 21.03.2017
 * Time: 13:01
 */

namespace frontend\modules\site\controllers;


use frontend\helpers\UsernameHelper;
use frontend\modules\site\helpers\LoginHelper;
use frontend\modules\site\models\RegisterForm;
use frontend\modules\site\models\User;
use frontend\modules\site\widgets\LoginWidget\LoginWidget;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class RegisterController extends Controller
{

    public $enableCsrfValidation = false;

    private $user;

    public function actionRegisterPhone() {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RegisterForm(['scenario' => RegisterForm::STEP_ENTER_PHONE]);

        if ($model->load(Yii::$app->request->post())) {
            $model->phone = UsernameHelper::clearUsername($model->phone);
            if ($model->validate()) {
                $model->submit = Yii::$app->request->post('submit', false);
                if ($model->submit) {
                    $sms = $model->sendSmsForRegister();
                    if ($sms['status'] == 200) {
                        $model->setStep(RegisterForm::STEP_ENTER_CODE);
                        $model->setScenario(RegisterForm::STEP_ENTER_CODE);
                        $html = LoginWidget::widget([
                            'type'      => LoginHelper::TYPE_REGISTER_ENTER_CODE,
                            'byAjax'    => true,
                            'model'     => $model
                        ]);
                        $str_js = LoginHelper::getJs($this->getView());
                        return [
                            'status'    => 200,
                            'html'      => $html,
                            'js'        => $str_js
                        ];
                    } else {
                        return [
                            'status'    => $sms['status'],
                            'html'      => $sms['message'],
                            'js'        => ''
                        ];
                    }
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return [
            'status'    => 404,
            'html'      => $html = LoginWidget::widget([
                'type'      => LoginHelper::TYPE_ERROR,
                'byAjax'    => true,
                'message'   => 'Страница не найдена',
            ])
        ];
    }

    public function actionEnterCode() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new RegisterForm(['scenario' => RegisterForm::STEP_ENTER_CODE]);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->submit = Yii::$app->request->post('submit', false);
                if ($model->submit) {
                    $code = $model->checkValidCode();
                    if ($code['status'] == 200) {
                        $model->setStep(RegisterForm::STEP_ENTER_PASSWORD);
                        $model->setScenario(RegisterForm::STEP_ENTER_PASSWORD);
                        $html = LoginWidget::widget([
                            'type'      => LoginHelper::TYPE_REGISTER_ENTER_PASSWORD,
                            'byAjax'    => true,
                            'model'     => $model
                        ]);
                        $str_js = LoginHelper::getJs($this->getView());
                        return [
                            'status'    => 200,
                            'html'      => $html,
                            'js'        => $str_js
                        ];
                    } else {
                        return [
                            'status'    => $code['status'],
                            'html'      => $code['message'],
                            'js'        => ''
                        ];
                    }
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }


        return [
            'status'    => 404,
            'html'      => $html = LoginWidget::widget([
                'type'      => LoginHelper::TYPE_ERROR,
                'byAjax'    => true,
                'message'   => 'Страница не найдена',
            ])
        ];
    }

    public function actionNewPassword() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new RegisterForm(['scenario' => RegisterForm::STEP_ENTER_PASSWORD]);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->submit = Yii::$app->request->post('submit', false);
                if ($model->submit) {
                    $user = User::findByUsername($model->phone);
                    $user->setScenario('signup');
                    $user->password = $model->password;
                    $user->repassword = $model->repassword;
                    if ($user->save()) {
                        if (Yii::$app->user->login($user)) {
                            return [
                                'status'    => 302,
                                'url'       => Url::toRoute(['/backend', 'complete_registration' => true])
                            ];
                        }
                    } else {
                        return [
                            'status'    => 500,
                            'html'      => 'Ошибка при сохранении пользователя',
                            'js'        => ''
                        ];
                    }
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return [
            'status'    => 404,
            'html'      => $html = LoginWidget::widget([
                'type'      => LoginHelper::TYPE_ERROR,
                'byAjax'    => true,
                'message'   => 'Страница не найдена',
            ])
        ];
    }

    public function actionLostPasswordPhone() {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RegisterForm(['scenario' => RegisterForm::STEP_LOST_PHONE]);

        if ($model->load(Yii::$app->request->post())) {
            $model->phone = UsernameHelper::clearUsername($model->phone);
            if ($model->validate()) {
                $model->submit = Yii::$app->request->post('submit', false);
                if ($model->submit) {
                    $sms = $model->sendSmsForLost();
                    if ($sms['status'] == 200) {
                        $model->setStep(RegisterForm::STEP_LOST_PASSWORD);
                        $model->setScenario(RegisterForm::STEP_LOST_PASSWORD);
                        $html = LoginWidget::widget([
                            'type'      => LoginHelper::TYPE_LOST_CODE,
                            'byAjax'    => true,
                            'model'     => $model
                        ]);
                        $str_js = LoginHelper::getJs($this->getView());
                        return [
                            'status'    => 200,
                            'html'      => $html,
                            'js'        => $str_js
                        ];
                    } else {
                        return [
                            'status'    => $sms['status'],
                            'html'      => $sms['message'],
                            'js'        => ''
                        ];
                    }
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return [
            'status'    => 404,
            'html'      => $html = LoginWidget::widget([
                'type'      => LoginHelper::TYPE_ERROR,
                'byAjax'    => true,
                'message'   => 'Страница не найдена',
            ])
        ];
    }

    public function actionLostPasswordAlready() {

        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = new RegisterForm(['scenario' => RegisterForm::STEP_LOST_PASSWORD]);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->clearCode) {
                $model->phone = UsernameHelper::clearUsername($model->phone);
            }
            if ($model->validate()) {
                $model->submit = Yii::$app->request->post('submit', false);
                if ($model->submit) {
                    $user = User::findByUsername($model->phone);
                    if (!empty($user) && !empty($model->code) && ($user->sms_code_wake_up == $model->code || YII_ENV == 'dev')) {
                        $user->setScenario('password');
                        $user->password = $model->password;
                        $user->repassword = $model->repassword;
                        if ($user->save()) {
                            if (Yii::$app->user->login($user)) {
                                return [
                                    'status'    => 302,
                                    'url'       => Url::toRoute(['/backend'])
                                ];
                            }
                        }
                    }
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return [
            'status'    => 404,
            'html'      => $html = LoginWidget::widget([
                'type'      => LoginHelper::TYPE_ERROR,
                'byAjax'    => true,
                'message'   => 'Страница не найдена',
            ])
        ];
    }
}