<?php
namespace frontend\modules\site\controllers;


use common\modules\pages\models\Page;
use frontend\helpers\UsernameHelper;
use frontend\modules\site\helpers\LoginHelper;
use frontend\modules\site\models\LoginForm;
use frontend\modules\site\widgets\LoginWidget\LoginWidget;
use linslin\yii2\curl\Curl;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

class DefaultController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    /**
     * Site "Home" page.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionIntroduction() {
        $page = Page::findByAlias('introduction');
        return $this->render('introduction', [
            'page'  => $page
        ]);
    }

    /**
     * Login user.
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->rememberMe = true;
            $model->username = UsernameHelper::clearUsername($model->username);
            if ($model->validate()) {
                if ($model->login()) {
                    if (!empty($model->return_url)) {
                        return $this->redirect($model->return_url, 200);
                    } else {
                        return $this->redirect(Url::to(['/backend']), 200);
                    }
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->goHome();
    }

    public function actionLoginWidget() {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $html = LoginWidget::widget([
            'type'      => Yii::$app->request->get('type', 'login'),
            'byAjax'    => true
        ]);
        $str_js = LoginHelper::getJs($this->getView());
        $data = [
            'status'    => 200,
            'html'      => $html,
            'js'        => $str_js,
        ];
        return $data;
    }

    public function actionSms() {
        $post = [
            'phoneNumber'   => "79219713678",
            "template"      => "NashSPbTemplate",
            "data"          => [
                "textmsg"   => "Код подтверждения Вашей учетной записи: 1232 Портал Наш Санкт-Петербург http://gorod.gov.spb.ru",
            ],
            "email"         => []
        ];
        $post = json_encode($post);
        $options = [
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_FOLLOWLOCATION  => false,
            CURLOPT_TIMEOUT         => 500,
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post)
            ]
        ];
        $proxySettings = Yii::$app->params['proxySettings'];
        if (!empty($proxySettings)) {
            $options[CURLOPT_PROXY] = $proxySettings['host'];
            $options[CURLOPT_PROXYUSERPWD] = $proxySettings['logpass'];
        }

        $curl = new Curl();
        $curl->setOptions($options);
        $find_from_api = $curl->post("http://app-test.mfc.adc.vpn:8081/SendMailApp/sendMessage");
        print_r($curl);
        print_r($find_from_api);
    }
}