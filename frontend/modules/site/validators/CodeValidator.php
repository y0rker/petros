<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 16:30
 */

namespace frontend\modules\site\validators;


use frontend\modules\site\models\RegisterForm;
use frontend\modules\site\models\User;
use yii\validators\Validator;

class CodeValidator extends Validator
{

    /**
     * @param RegisterForm $model
     * @param string $attribute
     * @return bool
     */
    public function validateAttribute($model, $attribute)
    {
        if (YII_ENV == 'prod') {
            $user = User::findByUsername($model->phone);
            if (!empty($user) && !empty($model->$attribute)) {
                if ($user->sms_code == $model->$attribute) {
                    return true;
                } else {
                    $this->addError($model, $attribute, 'Не верный код');
                }
            } else {
                $this->addError($model, $attribute, 'Не найден пользователь ' . $model->phone);
            }
            return false;
        }
        return true;
    }
}