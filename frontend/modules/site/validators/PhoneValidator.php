<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 14:28
 */

namespace frontend\modules\site\validators;


use yii\validators\Validator;

class PhoneValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!ctype_digit($model->$attribute) || strlen($model->$attribute) != 11) {
            $this->addError($model, $attribute, 'Не верно заполнен телефон');
        }
    }
}