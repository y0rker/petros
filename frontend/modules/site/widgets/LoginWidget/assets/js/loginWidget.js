var loginWidget = {
    widgetWrapper: null,
    init: function () {
        jQuery("input[type='tel']").inputmask({"mask": "+7 (999) 999-9999"});
        this.widgetWrapper = jQuery("#needReg");
        this.widgetWrapper.on('submit', function (e) {
            e.preventDefault();
            var target = e.target;
            loginWidget.widgetWrapper.addClass('modal-primary--loader');
            setTimeout(
                function () {
                    jQuery.ajax({
                        url: target.action,
                        type: "POST",
                        dataType: "json",
                        data: jQuery(target).serialize()+'&submit=true',
                        success: function(data) {
                            loginWidget.renderBody(data);
                        },
                        error: function(response) {
                            console.log(response);
                        }
                    });
                }, 300
            );
            return false;
        });
        this.widgetWrapper.find('a.widgetLink').on('click', this.loadWidgetPage);
        this.initPopup();
    },
    initPopup: function () {
        this.widgetWrapper.on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var return_url = button.data('return') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('#return_url').val(return_url);
        })
    },
    loadWidgetPage: function (e) {
        e.preventDefault();
        var link = jQuery(e.currentTarget);
        loginWidget.widgetWrapper.addClass('modal-primary--loader');
        setTimeout(
            function () {
                jQuery.ajax({
                    url: link.attr('href'),
                    dataType: 'json'
                }).done(function(data) {
                    loginWidget.renderBody(data);
                });
            }, 300
        );
    },
    renderBody: function(data) {
        var errorWrapper = loginWidget.widgetWrapper.find('.modal-content-error');
        if (data.status === 302) {
            window.location.href = data.url;
        } else {
            if (data.status === 200) {
                loginWidget.widgetWrapper.find('.modal-content-wrapper').html(data.html);
                errorWrapper.addClass('modal-content-error--hidden');
                eval(data.js);
            } else {
                errorWrapper.removeClass('modal-content-error--hidden');
                errorWrapper.html(data.html);
            }
            loginWidget.widgetWrapper.removeClass('modal-primary--loader');
            loginWidget.widgetWrapper.find('a.widgetLink').on('click', this.loadWidgetPage);
        }
    }
};