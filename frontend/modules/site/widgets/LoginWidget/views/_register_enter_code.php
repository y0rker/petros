<?php
/**
 * @var RegisterForm $model
 */
use frontend\modules\site\models\RegisterForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'action'    => \yii\helpers\Url::to(['/site/register/enter-code']),
    'id'                        => 'register-enter-code',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnSubmit'          => true,
    'validateOnChange'          => false,
    'validateOnBlur'            => false,
    'validateOnType'            => false
]);
echo $form->field($model, 'phone', [
    'options'   => [
        'tag'   => false,
    ],
    'template'  => '{input}'
])->hiddenInput()->label(false);
?>
    <div class="modal-header">
        <h4 class="modal-title">Регистрация
        </h4>
        <h5 class="modal-subtitle">Введите код подтверждения Вашего телефона, направленный Вам в sms-сообщении по указанному номеру телефона
        </h5>
    </div>
    <div class="modal-body">
        <?= $form->field($model, 'code')->textInput([
            'placeholder' => $model->getAttributeLabel('code'),
            'type'  => 'number'
        ])->label(false) ?>
    </div>
    <div class="modal-footer">
        <p>
            <div class="btn-group">
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-modal']) ?>
            </div>
        </p>
    </div>
<?php ActiveForm::end(); ?>