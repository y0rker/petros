<?php
use frontend\modules\site\helpers\LoginHelper;
use frontend\modules\site\models\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="modal-header">
    <h4 class="modal-title">Вход в личный кабинет
    </h4>
    <h5 class="modal-subtitle">Введите номер вашего мобильного телефона и пароль, указанный при регистрации на портале
    </h5>
</div>
<?php $form = ActiveForm::begin([
    'action'    => \yii\helpers\Url::to(['/site/default/login']),
    'id'                    => 'login-form',
    'enableAjaxValidation'  => true,
    'validateOnSubmit'      => true,
    'validateOnChange'      => false,
    'validateOnBlur'        => false,
    'validateOnType'        => false,
    'enableClientValidation' => false
]);
$model = new LoginForm();
echo $form->field($model, 'return_url', [
    'template'  => '{input}'
])->hiddenInput([
    'id'    => 'return_url',
])->label(false);
?>
<div class="modal-body">
    <?= $form->field($model, 'username')->textInput([
            'placeholder' => $model->getAttributeLabel('username'),
        'type'  => 'tel'
    ])->label(false) ?>
    <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')])->label(false) ?>
</div>
<div class="modal-footer">
    <p>
    <div class="btn-group">
        <?= Html::submitButton('Войти', ['class' => 'btn btn-modal']) ?>
    </div>
    </p>
    <p>
        <a href="<?=\yii\helpers\Url::toRoute(['/site/default/login-widget', 'type' => LoginHelper::TYPE_LOST_PHONE]);?>" class="widgetLink">Забыли пароль?</a>
    </p>
    <p>
        Пройдите <a href="<?=\yii\helpers\Url::toRoute(['/site/default/login-widget', 'type' => LoginHelper::TYPE_REGISTER]);?>" class="widgetLink">регистрацию</a>, если вы еще не зарегистрированы
    </p>
</div>
<?php ActiveForm::end(); ?>
<? /*
<div class="modal modal-primary fade" role="dialog" id="lostPass">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="lostPassBody">
            <div class="modal-header">
                <h4 class="modal-title">Регистрация
                </h4>
                <h5 class="modal-subtitle">Для регистрации на портале “Голос Петроградки” необходимо указать ваш номер мобильного телефона
                </h5>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group" id="lostPassWrapper">
                        <label class="label">Телефон</label>
                        <input class="form-control" type="tel" id="lostPassPhone">
                    </div>
                    <div id="lostPassCode" class="form-group" style="display: none;">
                        <label class="label">Введите код для продолжения восстановления пароля</label>
                        <input class="form-control" id="lostPassCodeInput">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <p>
                <div class="btn-group">
                    <button class="btn btn-modal" onclick="login.lostPasswordPhone();" id="lostPassBtn">Получить код</button>
                </div>
                </p>
                <p><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#lostPassNew">Уже есть код?</a></p>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-primary fade" role="dialog" id="lostPassNew">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">изменение данных
                </h4>
                <h5 class="modal-subtitle">На ваш телефон отправлен код подтверждения с помощью которого можно изменить пароль
                </h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="label">Телефон</label>
                    <input class="form-control" type="tel" id="lostPasswordAlreadyPhone">
                </div>
                <div class="form-group">
                    <label class="label">Код</label>
                    <input class="form-control" type="number" id="lostPasswordAlreadyCode">
                </div>
                <div class="form-group">
                    <label class="label">Пароль (Минимум 6 символов)</label>
                    <input class="form-control" type="password" id="lostPasswordAlreadyPassword">
                </div>
                <div class="form-group">
                    <label class="label">Подтверждение пароля (Минимум 6 символов)</label>
                    <input class="form-control" type="password" id="lostPasswordAlreadyRePassword">
                </div>
            </div>
            <div class="modal-footer">
                <p>
                    <div class="btn-group">
                        <button class="btn btn-modal" onclick="login.lostPasswordAlreadyCode();">подтвердить</button>
                    </div>
                </p>
            </div>
        </div>
    </div>
</div>
 */ ?>