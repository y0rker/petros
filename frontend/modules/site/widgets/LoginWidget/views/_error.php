<?php
/**
 * @var string $message
 */
?>
<div class="modal-header">
    <h4 class="modal-title">Ошибка
    </h4>
    <h5 class="modal-subtitle"><?=$message;?></h5>
</div>