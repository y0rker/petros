<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 12:40
 */

/**
 * @var string $type
 * @var \yii\web\View $this
 * @var boolean $byAjax
 * @var string $message
 * @var \yii\base\Model $model
 */

use frontend\modules\site\helpers\LoginHelper;
use frontend\modules\site\widgets\LoginWidget\Asset;

?>
<? if (!$byAjax): ?>
<div class="modal modal-primary fade" role="dialog" id="needReg">
    <div class="modal-dialog" role="document">
        <div class="loader"></div>
        <div class="modal-content">
        <div class="modal-content-error modal-content-error--hidden">
            Ошибка!
        </div>
            <div class="modal-content-wrapper">

<? endif; ?>
            <? switch ($type) {
                case LoginHelper::TYPE_LOGIN:
                    // Рендерим представление
                    echo $this->render(
                        '_login'
                    );
                break;
                case LoginHelper::TYPE_REGISTER:
                    // Рендерим представление
                    echo $this->render(
                        '_register'
                    );
                break;
                case LoginHelper::TYPE_LOST_PHONE:
                    // Рендерим представление
                    echo $this->render(
                        '_lost_password'
                    );
                break;
                case LoginHelper::TYPE_REGISTER_ENTER_CODE:
                    // Рендерим представление
                    echo $this->render(
                        '_register_enter_code', [
                            'model' => $model
                        ]
                    );
                break;
                case LoginHelper::TYPE_REGISTER_ENTER_PASSWORD:
                    // Рендерим представление
                    echo $this->render(
                        '_register_enter_password', [
                            'model' => $model
                        ]
                    );
                break;
                case LoginHelper::TYPE_LOST_CODE:
                    // Рендерим представление
                    echo $this->render(
                        '_lost_password_code', [
                            'model' => $model
                        ]
                    );
                break;
                case LoginHelper::TYPE_LOST_CODE_ALREADY:
                    // Рендерим представление
                    echo $this->render(
                        '_lost_password_already'
                    );
                break;
                case LoginHelper::TYPE_ERROR:
                    // Рендерим представление
                    echo $this->render(
                        '_error', ['message' => $message]
                    );
                break;
            } ?>
<? if (!$byAjax): ?>
            </div>
        </div>
    </div>
</div>
<? endif; ?>