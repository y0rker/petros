<?php
/**
 * @var RegisterForm $model
 */
use frontend\modules\site\models\RegisterForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'action'    => \yii\helpers\Url::to(['/site/register/new-password']),
    'id'                    => 'register-new-password',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnSubmit'          => true,
    'validateOnChange'          => false,
    'validateOnBlur'            => false,
    'validateOnType'            => false
]);
$model->setStep(RegisterForm::STEP_ENTER_PASSWORD);
echo $form->field($model, 'phone', [
    'options'   => [
        'tag'   => false,
    ],
    'template'  => '{input}'
])->hiddenInput()->label(false);
echo $form->field($model, 'code', [
    'options'   => [
        'tag'   => false,
    ],
    'template'  => '{input}'
])->hiddenInput()->label(false);
?>
    <div class="modal-header">
        <h4 class="modal-title">Регистрация
        </h4>
        <h5 class="modal-subtitle">Введите пароль, который будете использовать при входе в личный кабинет
        </h5>
    </div>
    <div class="modal-body">
        <?= $form->field($model, 'password')->textInput([
            'placeholder' => $model->getAttributeLabel('password'),
            'type'  => 'password'
        ])->label(false) ?>
        <?= $form->field($model, 'repassword')->textInput([
            'placeholder' => $model->getAttributeLabel('repassword'),
            'type'  => 'password'
        ])->label(false) ?>
    </div>
    <div class="modal-footer">
        <p>
        <div class="btn-group">
            <?= Html::submitButton('Продолжить', ['class' => 'btn btn-modal']) ?>
        </div>
        </p>
    </div>
<?php ActiveForm::end(); ?>