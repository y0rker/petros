<?php
/**
 *
 */
use frontend\modules\site\helpers\LoginHelper;
use frontend\modules\site\models\RegisterForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
    <div class="modal-header">
        <h4 class="modal-title">Восстановление пароля
        </h4>
        <h5 class="modal-subtitle">Для восстановления пароля на портале “Голос Петроградки” необходимо указать ваш номер мобильного телефона
        </h5>
    </div>
<?php $form = ActiveForm::begin([
    'action'    => \yii\helpers\Url::to(['/site/register/lost-password-phone']),
    'id'                    => 'registerForm',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnSubmit'          => true,
    'validateOnChange'          => false,
    'validateOnBlur'            => false,
    'validateOnType'            => false
]);
$model = new RegisterForm(['scenario' => RegisterForm::STEP_LOST_PHONE]);
?>
    <div class="modal-body">
        <?= $form->field($model, 'phone')->textInput([
            'placeholder'   => $model->getAttributeLabel('phone'),
            'type'          => 'tel'
        ])->label(false) ?>
    </div>
    <div class="modal-footer">
        <p>
        <div class="btn-group">
            <?= Html::submitButton('Далее', ['class' => 'btn btn-modal']) ?>
        </div>
        </p>
        <p>
            <a href="<?=\yii\helpers\Url::toRoute(['/site/default/login-widget', 'type' => LoginHelper::TYPE_LOST_CODE_ALREADY]);?>" class="widgetLink">Уже есть код?</a>
        </p>
    </div>
<?php ActiveForm::end(); ?>