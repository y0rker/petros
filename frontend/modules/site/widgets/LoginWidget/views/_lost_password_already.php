<?php
/**
 * @var RegisterForm $model
 */
use frontend\modules\site\models\RegisterForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin([
    'action'    => \yii\helpers\Url::to(['/site/register/lost-password-already']),
    'id'                    => 'lost-code-enter-password',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnSubmit'          => true,
    'validateOnChange'          => false,
    'validateOnBlur'            => false,
    'validateOnType'            => false
]);
$model = new RegisterForm(['scenario' => RegisterForm::STEP_LOST_PASSWORD]);
$model->clearCode = true;
echo $form->field($model, 'clearCode', [
    'options'   => [
        'tag'   => false,
    ],
    'template'  => '{input}'
])->hiddenInput()->label(false);
?>
    <div class="modal-header">
        <h4 class="modal-title">Восстановление пароля
        </h4>
        <h5 class="modal-subtitle">На телефон <?=$model->phone;?> отправлен код подтверждения с помощью которого можно изменить пароль
        </h5>
    </div>
    <div class="modal-body">
        <?= $form->field($model, 'phone')->textInput([
            'placeholder' => $model->getAttributeLabel('phone'),
            'type'  => 'tel'
        ])->label(false) ?>
        <?= $form->field($model, 'code')->textInput([
            'placeholder' => $model->getAttributeLabel('code'),
            'type'  => 'number'
        ])->label(false) ?>
        <?= $form->field($model, 'password')->textInput([
            'placeholder' => $model->getAttributeLabel('password'),
            'type'  => 'password'
        ])->label(false) ?>
        <?= $form->field($model, 'repassword')->textInput([
            'placeholder' => $model->getAttributeLabel('repassword'),
            'type'  => 'password'
        ])->label(false) ?>

    </div>
    <div class="modal-footer">
        <p>
        <div class="btn-group">
            <?= Html::submitButton('Изменить', ['class' => 'btn btn-modal']) ?>
        </div>
        </p>
    </div>
<?php ActiveForm::end(); ?>