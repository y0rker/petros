<?php
/**
 *
 */

use frontend\modules\site\helpers\LoginHelper;
use frontend\modules\site\models\RegisterForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="modal-header">
    <h4 class="modal-title">Регистрация
    </h4>
    <h5 class="modal-subtitle">Для регистрации на портале “Голос Петроградки” необходимо указать ваш номер мобильного телефона
    </h5>
</div>
<?php $form = ActiveForm::begin([
    'action'    => \yii\helpers\Url::to(['/site/register/register-phone']),
    'id'                        => 'registerForm',
    'enableAjaxValidation'      => true,
    'enableClientValidation'    => false,
    'validateOnSubmit'          => true,
    'validateOnChange'          => false,
    'validateOnBlur'            => false,
    'validateOnType'            => false
]);
$model = new RegisterForm(['scenario' => RegisterForm::STEP_ENTER_PHONE]);
?>
<div class="modal-body">
    <?= $form->field($model, 'phone')->textInput([
        'placeholder'   => $model->getAttributeLabel('phone'),
        'type'          => 'tel'
    ])->label(false) ?>
    <? $urlTos = Url::toRoute(['/pages/default/view', 'page' => 'term']);?>
    <?= $form->field($model, 'tos', [
        'options'   => [
            'class' => 'form-group checkbox-arrow',
        ],
        'template'  => '{input}{label}{error}'
    ])->checkbox([
        'class' => 'form-control'
    ], false)
        ->label(
        'Я согласен с условиями <a target="_blank" href="'.$urlTos.'">пользовательского соглашения</a>',
        [
            'class' => 'label'
        ]);?>
</div>
<div class="modal-footer">
    <p>
    <div class="btn-group">
        <?= Html::submitButton('Далее', ['class' => 'btn btn-modal']) ?>
    </div>
    </p>
    <p>
        <a href="<?=\yii\helpers\Url::toRoute(['/site/default/login-widget', 'type' => LoginHelper::TYPE_LOST_PHONE]);?>" class="widgetLink">Забыли пароль?</a>
    </p>
</div>
<?php ActiveForm::end(); ?>