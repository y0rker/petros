<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 21.03.2017
 * Time: 12:55
 */

namespace frontend\modules\site\widgets\LoginWidget;


use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@frontend/modules/site/widgets/LoginWidget/assets';
    public $js = [
        'js/loginWidget.js?v=3',
    ];
}