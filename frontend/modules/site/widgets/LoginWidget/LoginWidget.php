<?php
namespace frontend\modules\site\widgets\LoginWidget;
use frontend\modules\site\helpers\LoginHelper;
use yii\base\Widget;

/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 06.03.2017
 * Time: 12:33
 */
class LoginWidget extends Widget
{

    public $type = LoginHelper::TYPE_LOGIN;
    public $byAjax = false;
    public $message = '';
    public $model;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!$this->byAjax) {
            $this->asset();
        }
        echo $this->render('index', [
            'type'      => $this->type,
            'byAjax'    => $this->byAjax,
            'message'   => $this->message,
            'model'     => $this->model
        ]);
    }

    private function asset() {
        $view = $this->getView();
        Asset::register($view);
        $view->registerJs('loginWidget.init()');
    }
}