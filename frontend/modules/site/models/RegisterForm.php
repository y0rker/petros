<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 14:19
 */

namespace frontend\modules\site\models;


use common\modules\users\models\Profile;
use common\modules\users\traits\ModuleTrait;
use frontend\modules\site\validators\CodeLostValidator;
use frontend\modules\site\validators\CodeValidator;
use frontend\modules\site\validators\PhoneValidator;
use Yii;
use yii\base\Model;

class RegisterForm extends Model
{
    use ModuleTrait;

    const STEP_ENTER_PHONE = 'enter_phone';
    const STEP_ENTER_CODE = 'enter_code';
    const STEP_ENTER_PASSWORD = 'enter_password';

    const STEP_LOST_PHONE = 'lost_password_enter_phone';
    const STEP_LOST_CODE = 'lost_password_code';
    const STEP_LOST_PASSWORD = 'lost_password_password';

    public $clearCode = false;

    /**
     * @var string $phone Phone
     */
    public $phone;

    /**
     * @var string $code Code
     */
    public $code;
    /**
     * @var string $password Password
     */
    public $password;

    /**
     * @var string $repassword Repeat password
     */
    public $repassword;

    /**
     * @var boolean $submit Если мы сабмитим форму, а не валидируем
     */
    public $submit = false;

    /**
     * @var string $_step Текущий шаг регистрации
     */
    private $_step = self::STEP_ENTER_PHONE;

    /**
     * @var bool
     */
    public $tos;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Required
            [['phone', 'tos', 'code', 'password', 'repassword'], 'required'],
            [['phone'], PhoneValidator::className()],

            [['code'], CodeValidator::className(), 'on' => [
                self::STEP_ENTER_CODE,
                self::STEP_ENTER_PASSWORD
            ]],

            [['code'], CodeLostValidator::className(), 'on' => [
                self::STEP_LOST_CODE,
                self::STEP_LOST_PASSWORD
            ]],

            [['tos'], 'boolean'],

            [['tos'], 'integer', 'min' => 1, 'tooSmall' => 'Вы не подтвердили пользовательское соглашение'],

            // String
            [['password', 'repassword'], 'string', 'min' => 6, 'max' => 30],
            // Repassword
            ['repassword', 'compare', 'compareAttribute' => 'password'],

            [['clearCode'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'phone'         => Yii::t('users', 'ATTR_USERNAME'),
            'code'          => 'Код',
            'password'      => 'Пароль',
            'repassword'    => 'Повторите пароль'
        ];
    }

    public function scenarios()
    {
        return [
            self::STEP_ENTER_PHONE      => ['phone', 'tos'],
            self::STEP_ENTER_CODE       => ['phone', 'code'],
            self::STEP_ENTER_PASSWORD   => ['phone', 'code', 'password', 'repassword'],

            self::STEP_LOST_PHONE      => ['phone'],
            self::STEP_LOST_CODE       => ['phone', 'code'],
            self::STEP_LOST_PASSWORD   => ['phone', 'code', 'password', 'repassword', 'clearCode'],
        ];
    }

    /**
     * @param string $step
     */
    public function setStep(string $step) {
        $this->_step = $step;
    }

    /**
     * @return array
     */
    public function sendSmsForRegister() {
        $user = User::findByUsername($this->phone);
        if (!empty($user)) {
            return [
                'status'    => 400,
                'message'   => 'Пользователь уже существует. Воспользуйтесь восстановлением пароля'
            ];
        } else {
            $user = new User(['scenario' => 'signup']);
            $user->status = User::STATUS_ACTIVE;
            $user->generatePassword();
            $user->generateCode();
            $user->username = $this->phone;
            $profile = new Profile();

            if ($user->validate() && $profile->validate()) {
                $user->populateRelation('profile', $profile);
                if ($user->save(false)) {
                    $return = $user->sendSms();
                    if (!empty($return) && (!empty($return->sendStatus) || !empty($return['sendStatus']))) {
                        return [
                            'status'    => 200
                        ];
                    } else {
                        $user->delete();
                    }
                }
            }
        }
        return [
            'status'    => 500,
            'message'   => 'Ошибка при отправке СМС'
        ];
    }

    public function sendSmsForLost() {
        /**
         * @var User $user
         */
        $user = User::findByUsername($this->phone);
        if (!empty($user)) {
            $user->setScenario('lost_code');
            $user->generateLostCode();
            $user->save();
            $return = $user->sendSmsLost();
            if (!empty($return) && (!empty($return->sendStatus) || !empty($return['sendStatus']))) {
                return [
                    'status'    => 200
                ];
            }
        } else {
            return [
                'status'    => 400,
                'message'   => 'Пользователя с таким телефоном не существует'
            ];
        }
        return [
            'status'    => 500,
            'message'   => 'Ошибка при отправке СМС'
        ];
    }

    /**
     * @return array
     */
    public function checkValidCode() {
        if (YII_ENV == 'dev') {
            return [
                'status'    => 200
            ];
        }
        $user = User::findByUsername($this->phone);
        if (!empty($user) && !empty($this->code)) {
            if ($user->sms_code == $this->code) {
                return [
                    'status'    => 200
                ];
            } else {
                return [
                    'status'    => 403,
                    'message'   => 'Не верный код'
                ];
            }
        }
        return [
            'status'    => 403,
            'message'   => 'Такой пользователь не найден'
        ];
    }
}