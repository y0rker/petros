<?php

namespace frontend\modules\site\models;

use linslin\yii2\curl\Curl;
use common\helpers\Security;
use common\modules\users\models\Profile;
use common\modules\users\Module;
use Yii;

/**
 * Class User
 * @package frontend\modules\site\models
 * User is the model behind the signup form.
 *
 * @property string $username Username
 * @property string $email E-mail
 *
 * @property Profile $profile Profile
 * @property Module module
 */
class User extends \common\modules\users\models\User
{
    /**
     * @var string $password Password
     */
    public $password;

    /**
     * @var string $repassword Repeat password
     */
    public $repassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Required
            [['username', 'password', 'repassword'], 'required'],

            // Trim
            [['username', 'password', 'repassword'], 'trim'],

            // String
            [['password', 'repassword'], 'string', 'min' => 6, 'max' => 30],

            // Unique
            [['username'], 'unique'],

            // Username
            ['username', 'match', 'pattern' => '/^[a-zA-Z0-9_-]+$/'],
            ['username', 'string', 'min' => 11, 'max' => 11],

            // Repassword
            ['repassword', 'compare', 'compareAttribute' => 'password'],

            [['sms_code', 'sms_code_wake_up'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'signup' => ['username', 'email', 'password', 'repassword'],
            'password'  => ['password', 'repassword'],
            'lost_code' => ['sms_code_wake_up']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        return array_merge($labels, [
            'password'      => Yii::t('users', 'ATTR_PASSWORD'),
            'repassword'    => Yii::t('users', 'ATTR_REPASSWORD')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord || (!$this->isNewRecord && $this->password)) {
                $this->setPassword($this->password);
                $this->generateAuthKey();
                $this->generateToken();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if ($this->profile !== null) {
                $this->profile->user_id = $this->id;
                $this->profile->save(false);
            }

            $auth = Yii::$app->authManager;
            $role = $auth->getRole(self::ROLE_DEFAULT);
            $auth->assign($role, $this->id);
        }
    }

    public function generatePassword() {
        $pss = substr(Security::generateExpiringRandomString(), 0, 30);
        $this->password = $pss;
        $this->repassword = $pss;
    }

    public function generateCode() {
        $this->sms_code = rand(1000, 9999);
    }

    public function generateLostCode() {
        $this->sms_code_wake_up = rand(1000, 9999);
    }

    public function sendSms() {
        if (YII_ENV == 'dev') {
            return [
                'sendStatus'    => true
            ];
        }
        $post = [
            'phoneNumber'   => $this->username,
            "template"      => "NashSPbTemplate",
            "data"          => [
                "textmsg"   => "Код подтверждения Вашего телефона: ".$this->sms_code." Портала Голос Петроградки",
            ],
            "email"         => []
        ];
        $post = json_encode($post);
        $options = [
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_FOLLOWLOCATION  => false,
            CURLOPT_TIMEOUT         => 500,
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post)
            ]
        ];
        $proxySettings = Yii::$app->params['proxySettings'];
        if (!empty($proxySettings)) {
            $options[CURLOPT_PROXY] = $proxySettings['host'];
            $options[CURLOPT_PROXYUSERPWD] = $proxySettings['logpass'];
        }

        $curl = new Curl();
        $curl->setOptions($options);
        $url = Yii::$app->params['sms_url'];
        return json_decode($curl->post($url));
    }

    public function sendSmsLost() {
        if (YII_ENV == 'dev') {
            return [
                'sendStatus'    => true
            ];
        }
        $post = [
            'phoneNumber'   => $this->username,
            "template"      => "NashSPbTemplate",
            "data"          => [
                "textmsg"   => "Код подтверждения Вашего телефона: ".$this->sms_code_wake_up." Портал Голос Петроградки",
            ],
            "email"         => []
        ];
        $post = json_encode($post);
        $options = [
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_FOLLOWLOCATION  => false,
            CURLOPT_TIMEOUT         => 500,
            CURLOPT_POST            => true,
            CURLOPT_POSTFIELDS => $post,
            CURLOPT_HTTPHEADER => [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($post)
            ]
        ];
        $proxySettings = Yii::$app->params['proxySettings'];
        if (!empty($proxySettings)) {
            $options[CURLOPT_PROXY] = $proxySettings['host'];
            $options[CURLOPT_PROXYUSERPWD] = $proxySettings['logpass'];
        }

        $curl = new Curl();
        $curl->setOptions($options);
        $url = Yii::$app->params['sms_url'];
        return json_decode($curl->post($url));
    }
}
