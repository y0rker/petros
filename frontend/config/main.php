<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'name' => 'Голос Петроградки',
    'defaultRoute' => 'site/default/index',
    'modules' => [
        'site' => [
            'class' => \frontend\modules\site\Module::className()
        ],
        'news' => [
            'class' => \frontend\modules\news\Module::className()
        ],
        'pages' => [
            'class' => \frontend\modules\pages\Module::className()
        ],
        'gifts' => [
            'class' => \frontend\modules\gifts\Module::className()
        ],
        'polls' => [
            'class' => \frontend\modules\polls\Module::className()
        ],
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'sdi8s#fnj98jwiqiw;qfh!fjgh0d8f',
            'baseUrl' => ''
        ],
        'urlManager' => [
            'rules' => [
                '' => 'site/default/index',
                '<_a:(about|contacts|captcha|introduction|sms|login|login-widget)>' => 'site/default/<_a>',
                // Модуль [[Polls]]
                '<_m:polls>' => '<_m>/default/index',
                '<_m:polls>/<id:\d+>' => '<_m>/default/view',
                '<_m:polls>/<id:\d+>/vote' => '<_m>/default/vote',
                '<_m:polls>/<id:\d+>/success' => '<_m>/default/success',
                'registerPhone' => 'site/register/register-phone',
                'lostPasswordAlready' => 'site/register/lost-password-already',
                'lostPasswordPhone' => 'site/register/lost-password-phone',
                'lostPassword' => 'site/register/lost-password',
                'enterCode' => 'site/register/enter-code',
                'newPassword' => 'site/register/new-password',
                'news' => 'news/default/index',
                'news/<id:\d+>-<alias:[a-zA-Z0-9_-]{1,100}+>' => 'news/default/view',
                'pages/<page:[a-zA-Z0-9_-]{1,100}+>' => 'pages/default/view',
                'gifts' => 'gifts/default/index',
                'gifts/<id:\d+>' => 'gifts/default/view',
                'gifts/<id:\d+>/buy' => 'gifts/default/buy',
                'gifts/<id:\d+>/buy-success' => 'gifts/default/buy-success',
            ]
        ],
        'view' => [
            'theme' => 'frontend\theme\Theme'
        ],
        'errorHandler' => [
            'errorAction' => 'site/default/error'
        ],
        'assetManager' => [
            'linkAssets' => true
        ],
    ],
    'params' => $params,
];
