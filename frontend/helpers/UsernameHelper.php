<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 27.03.2017
 * Time: 8:12
 */

namespace frontend\helpers;


use Yii;
use yii\helpers\Html;

class UsernameHelper
{
    public static function getCabinetClass() {
        if (Yii::$app->user->isGuest) {
            return 'Личный кабинет';
        } else {
            $profile = Yii::$app->user->identity->profile;
            $name = Yii::$app->user->identity->username;
            if (!empty($profile->last_name)) {
                $name = $profile->last_name;
                if (!empty($profile->first_name)) {
                    $name .= ' '.$profile->first_name;
                }
            }
            return Html::encode($name);
        }
    }

    public static function clearUsername($username) {
        preg_match_all("/[0-9]+/i", $username, $newUsername);
        return ($newUsername) ? implode("", $newUsername[0]) : false;
    }
}