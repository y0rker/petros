<?php

/**
 * Head layout.
 * @var \yii\web\View $this
 */

use frontend\theme\ThemeAsset;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<title><?= Html::encode($this->title); ?></title>
    <link href="/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<?= Html::csrfMetaTags(); ?>
<?php $this->head();
ThemeAsset::register($this);

$this->registerMetaTag(
    [
        'charset' => Yii::$app->charset
    ]
);
$this->registerMetaTag(
    [
        'name' => 'viewport',
        'content' => 'width=device-width, initial-scale=1.0'
    ]
);
$this->registerMetaTag(
    [
        'name' => 'yandex-verification',
        'content' => 'd7ea8f3d939ddaea'
    ]
);
$this->registerLinkTag(
    [
        'rel' => 'canonical',
        'href' => Url::canonical()
    ]
); ?>
<? if (!empty($this->params['DESCRIPTION'])): ?>
<meta name="description" content="<?=$this->params['DESCRIPTION'];?>">
<? endif; ?>
<meta property="og:site_name" content="Голос Петроградки">
<meta property="og:title" content="<?=$this->title;?>">
<? if (!empty($this->params['DESCRIPTION'])): ?>
<meta property="og:description" content="<?=$this->params['DESCRIPTION'];?>">
<? endif; ?>
<? if (!empty($this->params['META_IMAGE'])): ?>
<meta property="og:image" content="<?=$this->params['META_IMAGE'];?>">
<? endif; ?>
<meta property="og:type" content="website">
<meta property="og:url" content="<?=Url::current([], true);?>">
<meta property="og:locale" content="ru_RU">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="<?=Yii::$app->params['twitter'];?>">
<? if (!empty($this->params['META_IMAGE'])): ?>
<meta name="twitter:image:src" content="<?=$this->params['META_IMAGE'];?>">
<? endif; ?>
<meta name="twitter:title" content="<?=$this->title;?>">
<? if (!empty($this->params['DESCRIPTION'])): ?>
<meta name="twitter:description" content="<?=$this->params['DESCRIPTION'];?>">
<? endif; ?>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-148752-hqcBU';</script>
<sсript src="https://vk.com/js/api/openapi.js?144"></sсript>
<?=$this->registerJs("if (window.VK) {VK.Retargeting.Init('VK-RTRG-96471-KZ24cpR');}"); ?>