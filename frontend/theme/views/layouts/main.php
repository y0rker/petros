<?php

/**
 * Theme main layout.
 *
 * @var \yii\web\View $this View
 * @var string $content Content
 */

use frontend\helpers\UsernameHelper;
use frontend\theme\ThemeAsset;
use yii\helpers\Url;

$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <?= $this->render('//layouts/head') ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>
    <?php
    $linkUser = 'href="#" data-toggle="modal" data-target="#needReg"';
    if (!Yii::$app->user->isGuest) {
        $linkUser = 'href="'. Url::toRoute(['/backend/']).'"';
    }
    ?>
    <? if (Yii::$app->params['test']): ?>
        <div class="test-label">Тестовая эксплуатация</div>
    <? endif; ?>
    <div class="wrapper">
        <section class="header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav">
                            <div class="nav__items">
                                <div class="nav__item"><a class="nav__link" <?=$linkUser;?>><?=UsernameHelper::getCabinetClass();?></a></div>
                                <div class="nav__item"><a class="nav__link" href="<?=Url::toRoute(['/site/default/introduction']);?>">Вступительное слово</a></div>
                            </div>
                            <div class="nav__items nav__items--logo">
                                <div class="nav__item nav__item--logo"><a class="link-img" href="/"><img src="<?=$assetUrl;?>/img/logo.png"></a></div>
                            </div>
                            <div class="nav__items">
                                <div class="nav__item"><a class="nav__link" href="<?=Url::toRoute(['/gifts/default/index']);?>">Магазин поощрений</a></div>
                                <div class="nav__item"><a class="nav__link" href="<?=Url::toRoute(['/news/default/index']);?>">Новости</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="offer">
                            <div class="offer__title" style="font-size: 52px;"><?=(!empty($this->params['layout-title'])) ? $this->params['layout-title'] : 'прими участие<br> в жизни своего района'; ?></div>
                            <div class="offer__subtitle"><?=(!empty($this->params['layout-subtitle'])) ? $this->params['layout-subtitle'] : 'На данном портале мы будем размещать вопросы и предложения администрации Петроградского района для сбора общественного мнения'; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="content">
            <?=$content;?>
        </div>
    </div>
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <div class="footer-content"><img class="footer-logo" src="<?=$assetUrl;?>/img/spb.png">Администрация<br> Петроградского района<br> Санкт-Петербурга</div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <div class="footer-content"><span class="small">© При полном или частичном использовании материалов ссылка на портал «Голос Петроградки» обязательна.</span></div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="footer-content footer-content--links"><a href="<?=Url::toRoute(['/pages/default/view', 'page' => 'term']);?>">Пользовательское соглашение</a><span>Обратная связь:</span><a href="mailto:support@golos.adc.spb.ru">support@golos.adc.spb.ru</a></div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-socials">
                        <div class="socials"><a class="socials__item socials__item--vk" target="_blank" href="https://vk.com/golospetrogradki"></a><a class="socials__item socials__item--fb" target="_blank" href="https://facebook.com/golospetrogradki"></a><a class="socials__item socials__item--ig" target="_blank" href="https://www.instagram.com/golos_petrogradki/"></a><a class="socials__item socials__item--tw" target="_blank" href="https://twitter.com/GolosPetrograd"></a><a class="socials__item socials__item--tm" target="_blank" href="https://t.me/GolosPetrograd"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <? if (Yii::$app->user->isGuest): ?>
        <?=\frontend\modules\site\widgets\LoginWidget\LoginWidget::widget();?>
    <? endif; ?>
    <?php $this->endBody(); ?>
    <?=\common\widgets\MetricsWidget\MetricsWidget::widget(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>