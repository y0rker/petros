<?php

namespace frontend\theme;

/**
 * Class Theme
 * @package vova07\themes\admin
 */
class Theme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@frontend/views' => '@frontend/theme/views',
        '@frontend/modules' => '@frontend/modules'
    ];
}
