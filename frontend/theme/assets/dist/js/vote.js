function checkVoteForm() {
    var currVotes = $(".vote__count__current").text();
    currVotes = parseInt(currVotes);
    if (currVotes == 0) {
        $("#voteFormError").children('div').text('Необходимо выбрать хотя бы один вариант');
    }
    return currVotes > 0;
}