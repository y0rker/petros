<?php
namespace frontend\theme;


use yii\web\AssetBundle;

class ThemeAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@frontend/theme/assets/dist';

    /**
     * @inheritdoc
     */
    public $css = [
        'css/index.css?v=19',
        'css/my.css?v=3',
        'https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css',
        'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/css/swiper.css'
    ];

    public $js = [
        'js/main.js?v=7',
        'js/vote.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.phone.extensions.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.4.2/js/swiper.jquery.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js',
        'https://yastatic.net/share2/share.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}