<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 28.02.2017
 * Time: 16:31
 */

namespace backend;


use yii\web\AssetBundle;
use yii\web\View;

class ThemeAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets';

    public $css = [
        'dist/css/font-awesome.min.css',
        'dist/css/ionicons.min.css',
        'dist/css/AdminLTE.css',
        'dist/css/custom.css',
        'dist/css/style.min.css'
    ];

    public $js = [
        'dist/js/app.min.js',
        'https://yastatic.net/share2/share.js'
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset'
    ];

    public $jsOptions = ['position' => View::POS_HEAD];
}