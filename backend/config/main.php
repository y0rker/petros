<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'name'      => 'Голос Петроградки',
    'basePath' => dirname(__DIR__),
    'modules' => [
        'admin' => [
            'class' => \backend\modules\base\Module::className()
        ],
        'users' => [
            'class' => \backend\modules\users\Module::className()
        ],
        'news' => [
            'class' => \backend\modules\news\Module::className()
        ],
        'polls' => [
            'class' => \backend\modules\polls\Module::className()
        ],
        'pages' => [
            'class' => \backend\modules\pages\Module::className()
        ],
        'gifts' => [
            'class' => \backend\modules\gifts\Module::className()
        ],
        'gridview' =>  [
            'class' => \kartik\grid\Module::className()
        ]
    ],
    'components' => [
        'request' => [
            'cookieValidationKey' => '7fdsf%dbYd&djsb#sn0mlsfo(kj^kf98dfh',
            'baseUrl' => '/backend'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'theme' => 'backend\Theme'
        ],
        'errorHandler' => [
            'errorAction' => 'admin/default/error'
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' => [
                '' => 'users/profile/index',
                '<_m>/<_c>/<_a>' => '<_m>/<_c>/<_a>'
            ]
        ],
        'assetManager' => [
            'linkAssets' => true
        ],
        'i18n' => [
            'translations' => [
                'admin' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/modules/base/messages',
                ],
                'admin*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@backend/base/messages',
                    'forceTranslation' => true,
                    'fileMap' => [
                        'admin' => 'admin.php',
                        'widgets/box' => 'box.php'
                    ]
                ],
            ]
        ],
    ],
    'params' => $params,
];
