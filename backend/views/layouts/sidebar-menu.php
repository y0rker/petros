<?php

/**
 * Sidebar menu layout.
 *
 * @var \yii\web\View $this View
 */

use backend\modules\base\widgets\Menu;
use common\modules\users\models\User;

$user = User::findOne(Yii::$app->user->getId());

echo Menu::widget(
    [
        'options' => [
            'class' => 'sidebar-menu'
        ],
        'items' => [
            [
                'label' => 'Личная информация',
                'url' => ['/users/profile/index'],
                'icon' => 'fa-user',
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'История голосований',
                'url' => ['/users/profile/result'],
                'icon' => 'fa-history',
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'Текущие голосования',
                'url' => '/#current_polls',
                'icon' => 'fa-list',
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'Магазин поощрений',
                'url' => '/gifts/',
                'icon' => 'fa-gift',
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'Мои заказы',
                'url' => ['/users/profile/gifts'],
                'icon' => 'fa-gift',
                'visible' => !Yii::$app->user->isGuest,
            ],
            [
                'label' => 'Результаты',
                'url' => ['/polls/result/index'],
                'icon' => 'fa-flag-checkered',
                'visible' => Yii::$app->user->can('iogv'),
            ],
            [
                'label' => 'Заказы',
                'url' => ['/gifts/default/orders'],
                'icon' => 'fa-flag-checkered',
                'visible' => $user->is_shop,
            ],
            [
                'label' => 'Администрирование',
                'icon'  => 'fa-lock',
                'url'   => '#',
                'visible'   => Yii::$app->user->can('superadmin'),
                'items' => [
                    [
                        'label' => Yii::t('admin', 'Users'),
                        'url' => ['/users/default/index'],
                        'icon' => 'fa-group',
                        'visible' => Yii::$app->user->can('administrateUsers') || Yii::$app->user->can('BViewUsers'),
                    ],
                    [
                        'label' => Yii::t('admin', 'News'),
                        'url' => ['/news/default/index'],
                        'icon' => 'fa-book',
                        'visible' => Yii::$app->user->can('administrateNews') || Yii::$app->user->can('BViewNews'),
                    ],
                    [
                        'label' => Yii::t('admin', 'Polls'),
                        'url' => ['/polls/default/index'],
                        'icon' => 'fa-navicon',
                        'visible' => Yii::$app->user->can('administratePolls') || Yii::$app->user->can('BViewPolls'),
                        'items' => [
                            [
                                'label' => 'Список',
                                'url' => ['/polls/default/index'],
                                'visible'   => Yii::$app->user->can('administratePolls')
                            ],
                            [
                                'label' => 'Параметры',
                                'url' => ['/polls/params/index'],
                                'visible'   => Yii::$app->user->can('administratePolls')
                            ]
                        ],
                    ],
                    [
                        'label' => 'Страницы',
                        'url' => ['/pages/default/index'],
                        'icon' => 'fa-file',
                        'visible' => Yii::$app->user->can('administratePages') || Yii::$app->user->can('BViewPages'),
                    ],
                    [
                        'label' => 'Магазин поощрений',
                        'url' => ['/gifts/default/index'],
                        'icon' => 'fa-gift',
                        'visible' => Yii::$app->user->can('administrateGifts')  || Yii::$app->user->can('BViewGifts'),
                    ],
                ]
            ],
            /*[
                'label' => Yii::t('admin', 'Comments'),
                'url' => ['/comments/default/index'],
                'icon' => 'fa-comments',
                'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels') || Yii::$app->user->can('BViewComments'),
                'items' => [
                    [
                        'label' => Yii::t('admin', 'Comments'),
                        'url' => ['/comments/default/index'],
                        'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewComments'),
                    ],
                    [
                        'label' => Yii::t('admin', 'Models management'),
                        'url' => ['/comments/models/index'],
                        'visible' => Yii::$app->user->can('administrateComments') || Yii::$app->user->can('BViewCommentsModels'),
                    ]
                ]
            ],
            [
                'label' => Yii::t('admin', 'Access control'),
                'url' => '#',
                'icon' => 'fa-gavel',
                'visible' => Yii::$app->user->can('administrateRbac') || Yii::$app->user->can('BViewRoles') || Yii::$app->user->can('BViewPermissions') || Yii::$app->user->can('BViewRules'),
                'items' => [
                    [
                        'label' => Yii::t('admin', 'Permissions'),
                        'url' => ['/rbac/permissions/index'],
                        'visible' => Yii::$app->user->can('administrateRbac') || Yii::$app->user->can('BViewPermissions')
                    ],
                    [
                        'label' => Yii::t('admin', 'Roles'),
                        'url' => ['/rbac/roles/index'],
                        'visible' => Yii::$app->user->can('administrateRbac') || Yii::$app->user->can('BViewRoles')
                    ],
                    [
                        'label' => Yii::t('admin', 'Rules'),
                        'url' => ['/rbac/rules/index'],
                        'visible' => Yii::$app->user->can('administrateRbac') || Yii::$app->user->can('BViewRules')
                    ]
                ]
            ],*/
        ]
    ]
);