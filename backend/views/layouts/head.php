<?php

/**
 * Head layout.
 * @var \yii\web\View $this
 */

use backend\ThemeAsset;
use yii\helpers\Html;
use yii\helpers\Url;

?>
<title><?= Html::encode($this->title); ?></title>
<?= Html::csrfMetaTags(); ?>
<?php $this->head(); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<?php
ThemeAsset::register($this);
$this->registerMetaTag(
    [
        'charset' => Yii::$app->charset
    ]
);
$this->registerMetaTag(
    [
        'name' => 'viewport',
        'content' => 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'
    ]
);
$this->registerLinkTag(
    [
        'rel' => 'canonical',
        'href' => Url::canonical()
    ]
); ?>
<? if (!empty($this->params['DESCRIPTION'])): ?>
    <meta name="description" content="<?=$this->params['DESCRIPTION'];?>">
<? endif; ?>
<meta property="og:site_name" content="Голос Петроградки">
<meta property="og:title" content="<?=$this->title;?>">
<? if (!empty($this->params['DESCRIPTION'])): ?>
    <meta property="og:description" content="<?=$this->params['DESCRIPTION'];?>">
<? endif; ?>
<? if (!empty($this->params['META_IMAGE'])): ?>
    <meta property="og:image" content="<?=$this->params['META_IMAGE'];?>">
<? endif; ?>
<meta property="og:type" content="website">
<meta property="og:url" content="<?=Url::current([], true);?>">
<meta property="og:locale" content="ru_RU">
<meta name="twitter:card" content="summary">
<meta name="twitter:site" content="<?=Yii::$app->params['twitter'];?>">
<? if (!empty($this->params['META_IMAGE'])): ?>
    <meta name="twitter:image:src" content="<?=$this->params['META_IMAGE'];?>">
<? endif; ?>
<meta name="twitter:title" content="<?=$this->title;?>">
<? if (!empty($this->params['DESCRIPTION'])): ?>
    <meta name="twitter:description" content="<?=$this->params['DESCRIPTION'];?>">
<? endif; ?>
<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-148752-hqcBU';</script>
<sсript src="https://vk.com/js/api/openapi.js?144"></sсript>
<?=$this->registerJs("if (window.VK) {VK.Retargeting.Init('VK-RTRG-96471-KZ24cpR');}"); ?>