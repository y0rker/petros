<?php

/**
 * Poll form view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \backend\modules\polls\models\Polls $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;
use backend\modules\polls\models\PollsParams;
use kartik\date\DatePicker;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;

$optionInput = [
    'template' => '<div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>',
]

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>
<div class="col-lg-12">
    <? if (!$model->isNewRecord): ?>
        <div class="row">
            <div class="col-sm-12">
                <a target="_blank" href="/polls/<?=$model->id;?>/" class="btn btn-sm btn-primary m-t-n-xs">Просмотр на сайте&nbsp;&nbsp;<i class="fa fa-link"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr>
            </div>
        </div>
    <? endif; ?>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'sub_name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'points') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'date_from', $optionInput)->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Дата начала ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                    'autoclose'         => true,
                ],
            ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'date_to', $optionInput)->widget(DatePicker::className(), [
                'options' => ['placeholder' => 'Дата окончания ...'],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                    'todayHighlight' => true,
                    'autoclose'         => true,
                ],
            ]);?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'visible')->dropDownList($statusArray) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'need_comment')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'complete')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'private')->checkbox() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="nav-tabs-custom">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                    <? $i = 1; ?>
                    <?
                    $params = PollsParams::getAllParams();
                    foreach ($params AS $param): ?>
                        <li<?=($i == 1)? ' class="active"' : ''; ?>><a href="#param<?=$param->id;?>" data-toggle="tab"><?=$param->name;?></a></li>
                    <? $i++; endforeach; ?>
                    <li class="pull-left header"><i class="fa fa-th-list"></i> Параметры</li>
                </ul>
                <div class="tab-content">
                    <? $i = 1; foreach ($params AS $param): ?>
                        <div class="chart tab-pane<?=($i == 1)? ' active': '';?>" id="param<?=$param->id;?>" style="position: relative; min-height: 300px; overflow: auto;">
                            <? foreach ($param->itemsList AS $item): ?>
                                <div class="col-lg-12">
                                    <? if ($item->multiple): ?>
                                        <? if ($item->big_text): ?>
                                            <?= MultipleInput::widget([
                                                'name' => 'Polls[params]',
                                                'value' => (!empty($model->params[$item->id])) ? $model->params[$item->id] : null,
                                                'allowEmptyList'    => false,
                                                'addButtonOptions'  => [
                                                    'class' => 'btn-primary'
                                                ],
                                                'columns' => [
                                                    [
                                                        'name'  => $item->id,
                                                        'title' => $item->name,
                                                        'type'  => Imperavi::className(),
                                                        'options'   => [
                                                            'attribute'     => 'value',
                                                            'settings' => [
                                                                'minHeight'     => 300,
                                                            ],
                                                        ]
                                                    ]
                                                ]
                                            ]) ?>
                                        <? else: ?>
                                            <?= MultipleInput::widget([
                                                'name' => 'Polls[params]',
                                                'value' => (!empty($model->params[$item->id])) ? $model->params[$item->id] : null,
                                                'allowEmptyList'    => false,
                                                'addButtonOptions'  => [
                                                    'class' => 'btn-primary'
                                                ],
                                                'columns' => [
                                                    [
                                                        'name'  => $item->id,
                                                        'title' => $item->name,
                                                        'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
                                                        'options'   => [
                                                            'placeholder'   => 'Введите значение'
                                                        ]
                                                    ]
                                                ]
                                            ]) ?>
                                        <? endif; ?>
                                    <? else: ?>
                                        <? if ($item->big_text): ?>
                                            <?=$form->field($model, 'params['.$item->id.'][0]')->widget(
                                                Imperavi::className(),
                                                [
                                                    'options' => [
                                                        'id' => 'params'.$param->id.'_'.$item->id,
                                                    ],
                                                    'settings' => [
                                                        'minHeight'     => 300,
                                                    ]
                                                ]
                                            )->label($item->name);?>
                                        <? else :?>
                                            <?=$form->field($model, 'params['.$item->id.'][0]')->textInput([
                                                'id' => 'params'.$param->id.'_'.$item->id
                                            ])->label($item->name);?>
                                        <? endif; ?>
                                    <? endif; ?>
                                    <hr>
                                </div>
                            <? endforeach; ?>
                        </div>
                    <? $i++; endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    $model->isNewRecord ? Yii::t('news', 'BACKEND_CREATE_SUBMIT') : Yii::t(
        'news',
        'BACKEND_UPDATE_SUBMIT'
    ),
    [
        'class' => $model->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>