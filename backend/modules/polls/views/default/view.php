<?php

/**
 * Polls view.
 *
 * @var yii\web\View $this View
 * @var \backend\modules\polls\models\Polls $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */


use backend\modules\base\widgets\Box;

$this->title = Yii::t('polls', 'BACKEND_VIEW_TITLE');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_VIEW_SUBTITLE');
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
];
$countVotes = count($model->votesFront);
?>
<div class="box box-success">
    <div class="box-header">
        <h3 class="box-title"><?=$model->name;?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-aqua">
                        <i class="fa fa-star-o"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Количество баллов</span>
                        <span class="info-box-number"><?=$model->points;?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-green">
                        <i class="fa fa-star-o"></i>
                    </span>
                    <div class="info-box-content">
                        <span class="info-box-text">Количество проголосовавших</span>
                        <span class="info-box-number"><?=$countVotes;?></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-4">
                <!-- Info Boxes Style 2 -->
                <? foreach ($model->votes AS $vote):
                    $count = $vote->count;
                    if ($countVotes > 0) {
                        $percent = $count * 100 / $countVotes;
                    } else {
                        $percent = 0;
                    }
                    ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <? if (!empty($vote->photo)): ?>
                                <img width="90px" style="float: left;" src="<?=$vote->urlAttribute('photo');?>" />
                            <? endif; ?>

                            <div class="info-box-content" <?=(empty($vote->photo)) ? 'style="margin-left: 0;"' : ''; ?>>
                                <span class="info-box-text"><?=$vote->name;?></span>
                                <span class="info-box-number"><?=$count;?></span>
                                <div class="progress progress-xs active" style="margin-bottom: 5px;">
                                    <div class="progress-bar progress-bar-success progress-bar-striped" style="width: <?=$percent;?>%"></div>
                                </div>
                                <span class="progress-description">
                                    ~<?=round($percent);?>% из <?=$countVotes;?> (проголосовавших)
                                  </span>
                            </div>
                        <!-- /.info-box-content -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <hr>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>
