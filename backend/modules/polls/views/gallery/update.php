<?php

/**
 * Polls update view.
 *
 * @var yii\web\View $this View
 * @var \backend\modules\polls\models\PollsGallery $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */


use backend\modules\base\widgets\Box;

$this->title = Yii::t('polls', 'BACKEND_UPDATE_TITLE_GALLERY');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_UPDATE_SUBTITLE_GALLERY');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('polls', 'BACKEND_INDEX_TITLE'),
        'url' => ['/polls/default/index'],
    ],
    [
        'label' => $model->poll->name,
        'url' => ['/polls/default/update', 'id' => $model->poll_id],
    ],
    [
        'label' => 'Галерея',
        'url' => ['index', 'poll_id' => $model->poll_id],
    ],
    $this->params['subtitle']
];
$boxButtons = ['{cancel}'];

if (Yii::$app->user->can('BCreatePolls')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BDeletePolls')) {
    $boxButtons[] = '{delete}';
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-success'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'subOption' => 'poll_id',
                'subOptionValue' => $model->poll_id
            ]
        );
        echo $this->render(
            '_form',
            [
                'model' => $model,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>
