<?php

/**
 * Polls gallery list view.
 *
 * @var \yii\web\View $this View
 * @var integer $poll_id
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var \backend\modules\polls\models\Polls $poll
 * @var \backend\modules\polls\models\search\PollsGallerySearch $searchModel Search model
 */

use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;
$this->title = Yii::t('polls', 'BACKEND_INDEX_TITLE');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_INDEX_SUBTITLE');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('polls', 'BACKEND_INDEX_TITLE'),
        'url' => ['/polls/default/index'],
    ],
    [
        'label' => $poll->name,
        'url' => ['/polls/default/update', 'id' => $poll->id],
    ],
    [
        'label' => 'Галерея',
    ],
];
$gridId = 'polls-gallery-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        'id',
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a(
                    $model['name'],
                    ['update', 'id' => $model['id']]
                );
            }
        ],
    ]
];

$boxButtons = $actions = [];
$showActions = false;

if (Yii::$app->user->can('BCreatePolls')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BUpdatePolls')) {
    $actions[] = '{update}';
    $showActions = $showActions || true;
}
if (Yii::$app->user->can('BDeletePolls')) {
    $boxButtons[] = '{batch-delete}';
    $actions[] = '{delete}';
    $showActions = $showActions || true;
}

if ($showActions === true) {
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions)
    ];
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>

<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId,
                'subOption'     => 'poll_id',
                'subOptionValue' => $poll->id
            ]
        ); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>