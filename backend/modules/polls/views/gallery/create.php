<?php

/**
 * Polls create view.
 *
 * @var \yii\web\View $this View
 * @var \backend\modules\polls\models\PollsGallery $model Model
 * @var \backend\modules\polls\models\Polls $poll
 * @var Box $box Box widget instance
 * @var integer $poll_id
 */

use backend\modules\base\widgets\Box;

$this->title = Yii::t('polls', 'BACKEND_CREATE_TITLE_GALLERY');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_CREATE_SUBTITLE_GALLERY');
$this->params['breadcrumbs'] = [
    [
        'label' => Yii::t('polls', 'BACKEND_INDEX_TITLE'),
        'url' => ['/polls/default/index'],
    ],
    [
        'label' => $poll->name,
        'url' => ['/polls/default/update', 'id' => $model->poll_id],
    ],
    [
        'label' => 'Галерея',
        'url' => ['index', 'poll_id' => $model->poll_id],
    ],
    $this->params['subtitle']
]; ?>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-primary'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => '{cancel}'
            ]
        );
        echo $this->render(
            '_form',
            [
                'model' => $model,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>