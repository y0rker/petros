<?php

/**
 * Poll form view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \backend\modules\polls\models\PollsGallery $model Model
 * @var Box $box Box widget instance
 */

use backend\modules\base\widgets\Box;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use vova07\fileapi\Widget as FileAPI;

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'name') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?= $form->field($model, 'image')->widget(
                    FileAPI::className(),
                    [
                        'settings' => [
                            'url' => ['/polls/gallery/fileapi-upload']
                        ],
                    ]
                ) ?>
            </div>
        </div>
    </div>
<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    $model->isNewRecord ? Yii::t('news', 'BACKEND_CREATE_SUBMIT') : Yii::t(
        'news',
        'BACKEND_UPDATE_SUBMIT'
    ),
    [
        'class' => $model->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>