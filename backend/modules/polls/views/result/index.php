<?php

/**
 * Polls list view.
 *
 * @var \yii\web\View $this View
 * @var Polls[] $polls
 */

use backend\modules\polls\models\Polls;

$this->title = Yii::t('polls', 'BACKEND_VOTE_INDEX_TITLE');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_VOTE_INDEX_SUBTITLE');
$this->params['breadcrumbs'] = [
    $this->title
];
?>
<? foreach ($polls AS $poll): ?>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?=count($poll->votesFront);?></h3>
                <p>Количество проголосовавших</p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
            <a href="<?=\yii\helpers\Url::toRoute(['/polls/default/view', 'id' => $poll->id]);?>" class="small-box-footer">
                <b><?=$poll->name;?></b>
            </a>
        </div>
    </div>
<? endforeach; ?>
