<?php

/**
 * Polls votes create view.
 *
 * @var \yii\web\View $this View
 * @var \backend\modules\polls\models\PollsVotes $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;

$this->title = 'Вариант';
$this->params['subtitle'] = 'Создание варианта';
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
]; ?>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-primary'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => '{cancel}'
            ]
        );
        echo $this->render(
            '_form',
            [
                'model' => $model,
                'statusArray' => $statusArray,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>