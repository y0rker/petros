<?php

/**
 * Poll votes form view.
 *
 * @var \yii\base\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \backend\modules\polls\models\PollsVotes $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;
use vova07\imperavi\Widget as Imperavi;
use vova07\fileapi\Widget as FileAPI;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$optionInput = [
    'template' => '<div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>',
]

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'description')->widget(
                Imperavi::className(),
                [
                    'settings' => [
                        'minHeight' => 200,
                        'imageGetJson' => Url::to(['/polls/votes/imperavi-get']),
                        'imageUpload' => Url::to(['/polls/votes/imperavi-image-upload']),
                        'fileUpload' => Url::to(['/polls/votes/imperavi-file-upload'])
                    ]
                ]
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'photo')->widget(
                FileAPI::className(),
                [
                    'settings' => [
                        'url' => ['/polls/votes/fileapi-upload']
                    ],
                ]
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'visible')->dropDownList($statusArray) ?>
        </div>
    </div>
</div>
<?= $form->field($model, 'poll_id')->hiddenInput()->label(false); ?>
<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    $model->isNewRecord ? Yii::t('news', 'BACKEND_CREATE_SUBMIT') : Yii::t(
        'news',
        'BACKEND_UPDATE_SUBMIT'
    ),
    [
        'class' => $model->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>