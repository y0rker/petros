<?php

/**
 * Polls update view.
 *
 * @var yii\web\View $this View
 * @var \backend\modules\polls\models\Polls $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */


use backend\modules\base\widgets\Box;

$this->title = Yii::t('polls', 'BACKEND_UPDATE_TITLE_PARAMS');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_UPDATE_SUBTITLE_PARAMS');
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
];
$boxButtons = ['{cancel}'];

if (Yii::$app->user->can('BCreatePolls')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BDeletePolls')) {
    $boxButtons[] = '{delete}';
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-success'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons
            ]
        );
        echo $this->render(
            '_form',
            [
                'model' => $model,
                'statusArray' => $statusArray,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>
