<?php

/**
 * Polls list view.
 *
 * @var \yii\web\View $this View
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var \backend\modules\polls\models\search\pollsParamsSearch $searchModel Search model
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;
$this->title = Yii::t('polls', 'BACKEND_INDEX_TITLE_PARAMS');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_INDEX_SUBTITLE_PARAMS');
$this->params['breadcrumbs'] = [
    $this->title
];
$gridId = 'polls-params-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        'id',
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a(
                    $model['name'],
                    ['update', 'id' => $model['id']]
                );
            }
        ],
        [
            'attribute' => 'visible',
            'format' => 'html',
            'value' => function ($model) {
                /**
                 * @var \backend\modules\polls\models\Polls $model
                 */
                $class = ($model->visible == $model::STATUS_PUBLISHED) ? 'label-success' : 'label-danger';
                return '<span class="label ' . $class . '">' . $model->visibleName . '</span>';
            },
            'filter' => Html::activeDropDownList(
                $searchModel,
                'visible',
                $statusArray,
                [
                    'class' => 'form-control',
                    'prompt' => Yii::t('news', 'BACKEND_PROMPT_STATUS')
                ]
            )
        ]
    ]
];

$boxButtons = $actions = [];
$showActions = false;

if (Yii::$app->user->can('BCreatePolls')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BUpdatePolls')) {
    $actions[] = '{update}';
    $showActions = $showActions || true;
}
if (Yii::$app->user->can('BDeletePolls')) {
    $boxButtons[] = '{batch-delete}';
    $actions[] = '{delete}';
    $showActions = $showActions || true;
}

if ($showActions === true) {
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions)
    ];
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>

<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId
            ]
        ); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>