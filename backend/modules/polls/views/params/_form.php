<?php

/**
 * Poll form view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \backend\modules\polls\models\Polls $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use unclead\multipleinput\MultipleInputColumn;

$optionInput = [
    'template' => '<div class="row"><div class="col-sm-12">{input}{error}{hint}</div></div>',
]

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'code') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'visible')->dropDownList($statusArray) ?>
        </div>
    </div>
    <?= $form->field($model, 'items', $optionInput)->widget(MultipleInput::className(), [
        'allowEmptyList'    => true,
        'addButtonOptions'  => [
            'class' => 'btn-primary'
        ],
        'columns' => [
            [
                'name'  => 'name',
                'title' => 'Наименование',
                'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
                'options'   => [
                    'placeholder'   => 'Введите наименование'
                ]
            ],
            [
                'name'  => 'code',
                'title' => 'Код',
                'type'  => MultipleInputColumn::TYPE_TEXT_INPUT,
                'options'   => [
                    'placeholder'   => 'Введите код'
                ]
            ],
            [
                'name'  => 'big_text',
                'title' => 'Большой текст',
                'type'  => MultipleInputColumn::TYPE_CHECKBOX,
            ],
            [
                'name'  => 'multiple',
                'title' => 'Множественность',
                'type'  => MultipleInputColumn::TYPE_CHECKBOX,
            ]
        ]
    ]) ?>
</div>
<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    $model->isNewRecord ? Yii::t('news', 'BACKEND_CREATE_SUBMIT') : Yii::t(
        'news',
        'BACKEND_UPDATE_SUBMIT'
    ),
    [
        'class' => $model->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>