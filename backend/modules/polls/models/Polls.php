<?php
namespace backend\modules\polls\models;

use common\modules\polls\models\PollsParamsItemsValues;
use Yii;
use yii\behaviors\SluggableBehavior;

/**
 *
 */
class Polls extends \common\modules\polls\models\Polls
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        return array_merge($behaviors, [
                'sluggableBehavior' => [
                'class' => SluggableBehavior::className(),
                'attribute' => 'name',
                'slugAttribute' => 'alias'
            ]
        ]);
    }

    /**
     * @return array Status array.
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_UNPUBLISHED 	=> Yii::t('news', 'STATUS_UNPUBLISHED'),
            self::STATUS_PUBLISHED 		=> Yii::t('news', 'STATUS_PUBLISHED'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['visible', 'in', 'range' => array_keys(self::getStatusArray())];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin-create'] = [
            'name',
            'visible',
            'need_comment',
            'points',
            'params',
            'complete',
            'private',
            'date_from',
            'date_to',
            'sub_name'
        ];
        $scenarios['admin-update'] = [
            'name',
            'visible',
            'need_comment',
            'points',
            'params',
            'complete',
            'private',
            'date_from',
            'date_to',
            'sub_name'
        ];

        return $scenarios;
    }

    /**
     * @return string Readable iogv status
     */
    public function getVisibleName()
    {
        $statuses = self::getStatusArray();

        return $statuses[$this->visible];
    }

    public function loadParams() {
        $params = PollsParamsItemsValues::getParamsByPollId($this->id);
        foreach ($params AS $param) {
            $this->params[$param->poll_param_item_id][] = $param->value;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        PollsParamsItemsValues::deleteAll([
            'poll_id'   => $this->id
        ]);
        foreach ($this->params AS $value_id => $value) {
            if (is_array($value)) {
                foreach ($value AS $val) {
                    $itemModel = new PollsParamsItemsValues();
                    $itemModel->poll_id = $this->id;
                    $itemModel->poll_param_item_id = $value_id;
                    $itemModel->value = $val;
                    $itemModel->save();
                }
            } else {
                $itemModel = new PollsParamsItemsValues();
                $itemModel->poll_id = $this->id;
                $itemModel->poll_param_item_id = $value_id;
                $itemModel->value = $value;
                $itemModel->save();
            }
        }
    }
}