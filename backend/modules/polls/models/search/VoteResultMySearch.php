<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 23.01.2017
 * Time: 16:04
 */

namespace backend\modules\polls\models\search;


use common\modules\polls\models\vote\Vote;
use yii\data\ActiveDataProvider;

class VoteResultMySearch extends Vote
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Integer
            [['id'], 'integer'],
            // Date
            [['created_at', 'updated_at'], 'date', 'format' => 'd.m.Y']
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find()->orderBy('created_at DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere(
            [
                'user_id'   => \Yii::$app->user->id
            ]
        );

        if (!($this->load($params))) {
            return $dataProvider;
        }
        $query->andFilterWhere(
            [
                'id' 		=> $this->id,
                'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => $this->created_at,
                'FROM_UNIXTIME(updated_at, "%d.%m.%Y")' => $this->updated_at
            ]
        );


        return $dataProvider;
    }
}