<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.06.2017
 * Time: 12:24
 */

namespace backend\modules\polls\models\search;


use backend\modules\polls\models\PollsGallery;
use yii\data\ActiveDataProvider;

class PollsGallerySearch extends PollsGallery
{
    public $poll_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Integer
            ['id', 'integer'],
            // String
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find();

        if (!empty($this->poll_id)) {
            $query->andWhere([
                'poll_id'   => $this->poll_id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'id' 		=> $this->id,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}