<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.01.2017
 * Time: 12:27
 */

namespace backend\modules\polls\models\search;


use backend\modules\polls\models\PollsVotes;
use yii\data\ActiveDataProvider;

class PollsVotesSearch extends PollsVotes
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Integer
            [['id', 'count'], 'integer'],
            // String
            [['name'], 'string', 'max' => 255],
            // Status
            ['visible', 'in', 'range' => array_keys(self::getStatusArray())],
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!empty($params['poll_id'])) {
            $query->andFilterWhere(
                [
                    'poll_id' 	=> $params['poll_id'],
                ]
            );
        }

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'id' 		=> $this->id,
                'visible' 	=> $this->visible,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}