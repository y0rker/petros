<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.06.2017
 * Time: 12:23
 */

namespace backend\modules\polls\models;


class PollsGallery extends \common\modules\polls\models\PollsGallery
{
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin-create'] = [
            'name',
            'image',
            'poll_id'
        ];
        $scenarios['admin-update'] = [
            'name',
            'image',
            'poll_id'
        ];

        return $scenarios;
    }
}