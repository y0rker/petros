<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 17:22
 */

namespace backend\modules\polls\models;


use common\modules\polls\models\PollsParamsItems;
use Yii;

/**
 * Class PollsParams
 * @package backend\modules\polls\models
 *
 * @property PollsParamsItems[] itemsList
 */
class PollsParams extends \common\modules\polls\models\PollsParams
{

    public $items;

    /**
     * @return array Status array.
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_UNPUBLISHED 	=> Yii::t('news', 'STATUS_UNPUBLISHED'),
            self::STATUS_PUBLISHED 		=> Yii::t('news', 'STATUS_PUBLISHED'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['visible', 'in', 'range' => array_keys(self::getStatusArray())];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin-create'] = [
            'name',
            'visible',
            'items',
            'code'
        ];
        $scenarios['admin-update'] = [
            'name',
            'visible',
            'items',
            'code'
        ];

        return $scenarios;
    }

    /**
     * @return string Readable iogv status
     */
    public function getVisibleName()
    {
        $statuses = self::getStatusArray();

        return $statuses[$this->visible];
    }

    public function setNeededAttr() {
        $this->items = [];
        $values = PollsParamsItems::findAllItemsByPollParamsId($this->id);
        foreach($values as $item) {
            $this->items[] = [
                'name'      => $item->name,
                'code'      => $item->code,
                'multiple'  => $item->multiple,
                'big_text'  => $item->big_text
            ];
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        PollsParamsItems::deleteAll(['poll_param_id' => $this->id]);
        if (!empty($this->items) && is_array($this->items)) {
            foreach ($this->items AS $item) {
                $paramsItemModel = new PollsParamsItems();
                $paramsItemModel->poll_param_id = $this->id;
                $paramsItemModel->name = $item['name'];
                $paramsItemModel->multiple = $item['multiple'];
                $paramsItemModel->code = $item['code'];
                $paramsItemModel->big_text = $item['big_text'];
                $paramsItemModel->save();
            }
        }
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return self[]
     */
    public static function getAllParams() {
        return self::find()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemsList() {
        return $this->hasMany(PollsParamsItems::className(), ['poll_param_id' => 'id']);
    }
}