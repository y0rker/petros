<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.01.2017
 * Time: 12:28
 */

namespace backend\modules\polls\models;


use Yii;

/**
 * Class PollsVotes
 * @package backend\modules\polls\models
 */
class PollsVotes extends \common\modules\polls\models\PollsVotes
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin-create'] = [
            'name',
            'visible',
            'poll_id',
            'description',
            'photo'
        ];
        $scenarios['admin-update'] = [
            'name',
            'visible',
            'poll_id',
            'description',
            'photo'
        ];

        return $scenarios;
    }

    /**
     * @return array Status array.
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_UNPUBLISHED 	=> Yii::t('news', 'STATUS_UNPUBLISHED'),
            self::STATUS_PUBLISHED 		=> Yii::t('news', 'STATUS_PUBLISHED'),
        ];
    }

    /**
     * @return string Readable iogv status
     */
    public function getVisible()
    {
        $statuses = self::getStatusArray();

        return $statuses[$this->visible];
    }
}