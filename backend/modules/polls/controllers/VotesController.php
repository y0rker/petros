<?php

namespace backend\modules\polls\controllers;

use backend\modules\base\components\Controller;
use backend\modules\polls\models\PollsVotes;
use backend\modules\polls\models\search\PollsVotesSearch;
use backend\modules\polls\Module;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\HttpException;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

use Yii;

/**
 * Default backend controller.
 *
 * @property Module module
 */
class VotesController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => $this->module->photoTempPath
            ],
        ];
    }

    /**
     * Poll list page.
     */
    public function actionIndex()
    {
        $searchModel    = new PollsVotesSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->get());
        $statusArray    = PollsVotes::getStatusArray();

        return $this->render('index', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
            'statusArray'   => $statusArray,
        ]);
    }

    /**
     * Poll's list page.
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model'			=> $model,
        ]);
    }

    /**
     * Create post page.
     */
    public function actionCreate()
    {
        $poll_id = Yii::$app->request->get('poll_id', 0);
        if (!Yii::$app->request->isPost && empty($poll_id)) {
            throw new HttpException(403);
        }
        $model = new PollsVotes(['scenario' => 'admin-create']);
        $model->poll_id = $poll_id;

        $statusArray = PollsVotes::getStatusArray();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('site', 'BACKEND_FLASH_FAIL_ADMIN_CREATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('create', [
            'model' 		=> $model,
            'statusArray' 	=> $statusArray,
        ]);
    }

    /**
     * Update post page.
     *
     * @param integer $id Poll ID
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');
        $statusArray = PollsVotes::getStatusArray();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('admin', 'BACKEND_FLASH_FAIL_ADMIN_UPDATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('update', [
            'model' 		=> $model,
            'statusArray' 	=> $statusArray,
        ]);
    }

    /**
     * Delete post page.
     *
     * @param integer $id Poll ID
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple Poll page.
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $models = $this->findModel($ids);
            foreach ($models as $model) {
                $model->delete();
            }
            return $this->redirect(['index']);
        } else {
            throw new HttpException(400);
        }
    }

    /**
     * Find model by ID.
     *
     * @param integer|array $id Poll ID
     *
     * @return \backend\modules\polls\models\PollsVotes Model
     *
     * @throws HttpException 404 error if Poll not found
     */
    protected function findModel($id)
    {
        if (is_array($id)) {
            $model = PollsVotes::findAll($id);
        } else {
            $model = PollsVotes::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}