<?php

namespace backend\modules\polls\controllers;

use backend\modules\base\components\Controller;
use backend\modules\polls\models\Polls;
use yii\filters\AccessControl;

/**
 * Default backend controller.
 */
class ResultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['iogv'],
                ]
            ]
        ];

        return $behaviors;
    }

    /**
     * Poll list page.
     */
    public function actionIndex()
    {

        $polls = Polls::findAllPolls();

        return $this->render('index', [
            'polls' 	=> $polls,
        ]);
    }
}