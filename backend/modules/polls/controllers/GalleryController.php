<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.06.2017
 * Time: 12:24
 */

namespace backend\modules\polls\controllers;

use backend\modules\base\components\Controller;
use backend\modules\polls\models\Polls;
use backend\modules\polls\models\PollsGallery;
use backend\modules\polls\models\search\PollsGallerySearch;
use backend\modules\polls\Module;
use Yii;
use yii\web\HttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

/**
 * Class GalleryController
 * @package backend\modules\polls\controllers
 *
 * @property Module module
 */
class GalleryController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => $this->module->galleryTempPath
            ],
        ];
    }

    /**
     * Poll gallery list page.
     * @param int $poll_id
     * @return string
     *
     * @throws HttpException 404 error if Poll not found
     */
    public function actionIndex($poll_id)
    {
        if (empty($poll_id)) {
            throw new HttpException(404);
        }
        $searchModel    = new PollsGallerySearch();
        $searchModel->poll_id = $poll_id;
        $dataProvider   = $searchModel->search(Yii::$app->request->get());

        $pollModel = Polls::findOne($poll_id);

        return $this->render('index', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
            'poll'          => $pollModel
        ]);
    }

    /**
     * Create post page.
     * @param $poll_id
     * @return array|string|Response
     *
     * @throws HttpException 404 error if Poll not found
     */
    public function actionCreate($poll_id)
    {
        if (empty($poll_id)) {
            throw new HttpException(404);
        }
        $model = new PollsGallery(['scenario' => 'admin-create']);
        $model->poll_id = $poll_id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('site', 'BACKEND_FLASH_FAIL_ADMIN_CREATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        $pollModel = Polls::findOne($poll_id);

        return $this->render('create', [
            'model' 		=> $model,
            'poll'          => $pollModel
        ]);
    }

    /**
     * Update post page.
     *
     * @param integer $id Poll ID
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('admin', 'BACKEND_FLASH_FAIL_ADMIN_UPDATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('update', [
            'model' 		=> $model,
        ]);
    }

    /**
     * Delete post page.
     *
     * @param integer $id Poll ID
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple Poll page.
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $models = $this->findModel($ids);
            foreach ($models as $model) {
                $model->delete();
            }
            return $this->redirect(['index']);
        } else {
            throw new HttpException(400);
        }
    }

    /**
     * Find model by ID.
     *
     * @param integer|array $id Poll ID
     *
     * @return PollsGallery Model
     *
     * @throws HttpException 404 error if Poll not found
     */
    protected function findModel($id)
    {
        if (is_array($id)) {
            $model = PollsGallery::findAll($id);
        } else {
            $model = PollsGallery::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}