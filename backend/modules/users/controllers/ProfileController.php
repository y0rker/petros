<?php

namespace backend\modules\users\controllers;

use backend\modules\base\components\Controller;
use backend\modules\gifts\models\search\GiftUserSearch;
use backend\modules\polls\models\search\VoteResultMySearch;
use backend\modules\users\models\User;
use common\modules\users\models\UserGifts;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Default backend controller.
 */
class ProfileController extends Controller
{

    public $enableCsrfValidation = false;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['get', 'put', 'post']
            ]
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ]
            ]
        ];

        return $behaviors;
    }

    /**
     * Update user page.
     *
     *
     * @return mixed View
     */
    public function actionIndex()
    {
        $user = $this->findModel(Yii::$app->user->id);
        $user->setScenario('admin-update');
        $profile = $user->profile;

        if ($user->load(Yii::$app->request->post()) && $profile->load(Yii::$app->request->post())) {
            if ($user->validate() && $profile->validate()) {
                $user->populateRelation('profile', $profile);
                if (!$user->save(false)) {
                    Yii::$app->session->setFlash('danger', Yii::t('users', 'BACKEND_FLASH_FAIL_ADMIN_CREATE'));
                }
                return $this->refresh();
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return array_merge(ActiveForm::validate($user), ActiveForm::validate($profile));
            }
        }

        $isCompleteRegistration = Yii::$app->request->get('complete_registration', 0);

        if ($isCompleteRegistration == 1) {
            $view = $this->getView();
            $view->registerJs('fbq(\'track\', \'CompleteRegistration\', {
            value: 25.00,
            currency: \'USD\'
            });');
            $view->registerJs('VK.Retargeting.Add(25118056);VK.Retargeting.Event(\'click-reg-button\');');
        }

        return $this->render('index', [
            'user'          => $user,
            'profile'       => $profile,
        ]);
    }

    /**
     * @throws \yii\base\InvalidParamException
     */
    public function actionGifts()
    {
        $searchModel    = new GiftUserSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->get(), true);

        return $this->render('gifts', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
        ]);
    }

    /**
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCancel($id)
    {
        $gift = UserGifts::findOne($id);
        if (null === $gift) {
            throw new NotFoundHttpException();
        }
        if ($gift->user_id !== Yii::$app->getUser()->id) {
            throw new ForbiddenHttpException();
        }
        $gift->status = false;
        $gift->save();

        return $this->redirect(['/users/profile/gifts']);
    }

    public function actionResult() {
        $searchModel    = new VoteResultMySearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->get());

        return $this->render('result', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
        ]);
    }

    /**
     * Find model by ID
     *
     * @param integer|array $id User ID
     *
     * @return User ProfileModel
     * @throws HttpException 404 error if user was not found
     */
    protected function findModel($id)
    {
        if (is_array($id)) {
            /** @var User $user */
            $model = User::findIdentities($id);
        } else {
            /** @var User $user */
            $model = User::findIdentity($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}