<?php

namespace backend\modules\users\models;

use common\modules\users\models\Profile;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * User search model.
 */
class UserSearch extends User
{
    /**
     * @var string Name
     */
    public $first_name;

    /**
     * @var string Surname
     */
    public $last_name;

    /**
     * @var string Surname
     */
    public $middle_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // String
            [['middle_name', 'last_name', 'first_name', 'username', 'email'], 'string'],
            // Role
            ['role', 'in', 'range' => array_keys(self::getRoleArray())],
            // Status
            ['status', 'in', 'range' => array_keys(self::getStatusArray())],
            // Date
            [['created_at', 'updated_at'], 'date', 'format' => 'd.m.Y']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find()->joinWith(['profile']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['first_name'] = [
            'asc' => [Profile::tableName() . '.first_name' => SORT_ASC],
            'desc' => [Profile::tableName() . '.first_name' => SORT_DESC]
        ];
        $dataProvider->sort->attributes['last_name'] = [
            'asc' => [Profile::tableName() . '.last_name' => SORT_ASC],
            'desc' => [Profile::tableName() . '.last_name' => SORT_DESC]
        ];
        $dataProvider->sort->attributes['middle_name'] = [
            'asc' => [Profile::tableName() . '.middle_name' => SORT_ASC],
            'desc' => [Profile::tableName() . '.middle_name' => SORT_DESC]
        ];

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'id' => $this->id,
                'status_id' => $this->status,
                'role' => $this->role,
                'FROM_UNIXTIME(created_at, "%d.%m.%Y")' => $this->created_at,
                'FROM_UNIXTIME(updated_at, "%d.%m.%Y")' => $this->updated_at
            ]
        );

        $query->andFilterWhere(['like', Profile::tableName() . '.first_name', $this->first_name]);
        $query->andFilterWhere(['like', Profile::tableName() . '.last_name', $this->last_name]);
        $query->andFilterWhere(['like', Profile::tableName() . '.middle_name', $this->middle_name]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}
