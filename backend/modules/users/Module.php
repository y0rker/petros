<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 13:17
 */

namespace backend\modules\users;


class Module extends \common\modules\users\Module
{
    public $isBackend = true;
}