<?php

/**
 * Users list view.
 *
 * @var \yii\web\View $this View
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var UserSearch $searchModel Search model
 * @var array $roleArray Roles array
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\ActionColumn;
use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use backend\modules\users\models\UserSearch;
use kartik\date\DatePicker;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;

$this->title = Yii::t('users', 'BACKEND_INDEX_TITLE');
$this->params['subtitle'] = Yii::t('users', 'BACKEND_INDEX_SUBTITLE');
$this->params['breadcrumbs'] = [
    $this->title
];
$gridId = 'users-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        'id',
        [
            'attribute' => 'username',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a($model['username'], ['update', 'id' => $model['id']], ['data-pjax' => 0]);
            }
        ],
        'email',
        [
            'attribute' => 'last_name',
            'value' => 'profile.last_name',
            'label' => Yii::t('users', 'ATTR_LAST_NAME')
        ],
        [
            'attribute' => 'first_name',
            'value' => 'profile.first_name',
            'label' => Yii::t('users', 'ATTR_FIRST_NAME')
        ],
        [
            'attribute' => 'middle_name',
            'value' => 'profile.middle_name',
            'label' => Yii::t('users', 'ATTR_MIDDLE_NAME')
        ],
        [
            'attribute' => 'status',
            'format' => 'html',
            'value' => function ($model) {
                if ($model->status === $model::STATUS_ACTIVE) {
                    $class = 'label-success';
                } elseif ($model->status === $model::STATUS_INACTIVE) {
                    $class = 'label-warning';
                } else {
                    $class = 'label-danger';
                }

                return '<span class="label ' . $class . '">' . $model->statusName . '</span>';
            },
            'filter' => Html::activeDropDownList(
                $searchModel,
                'status',
                $statusArray,
                ['class' => 'form-control', 'prompt' => Yii::t('users', 'BACKEND_PROMPT_STATUS')]
            )
        ],
        [
            'attribute' => 'role',
            'filter' => Html::activeDropDownList(
                $searchModel,
                'role',
                $roleArray,
                ['class' => 'form-control', 'prompt' => Yii::t('users', 'BACKEND_PROMPT_ROLE')]
            )
        ],
        [
            'attribute' => 'created_at',
            'format' => 'date',
            'filter' => DatePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                    ]
                ]
            )
        ],
        [
            'attribute' => 'updated_at',
            'format' => 'date',
            'filter' => DatePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'format' => 'dd.mm.yyyy',
                    ]
                ]
            )
        ]
    ]
];

$boxButtons = $actions = [];
$showActions = false;

if (Yii::$app->user->can('BCreateUsers')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BUpdateUsers')) {
    $actions[] = '{update}';
    $showActions = $showActions || true;
}
if (Yii::$app->user->can('BDeleteUsers')) {
    $boxButtons[] = '{batch-delete}';
    $actions[] = '{delete}';
    $showActions = $showActions || true;
}

if ($showActions === true) {
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions)
    ];
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>
<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId
            ]
        ); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>