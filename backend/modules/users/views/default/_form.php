<?php

/**
 * User form view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var User $model Model
 * @var Profile $profile Profile
 * @var array $roleArray Roles array
 * @var array $statusArray Statuses array
 * @var Box $box Box widget instance
 */

use backend\modules\base\widgets\Box;
use backend\modules\users\models\User;
use common\modules\users\models\Profile;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>
    <div class="col-lg-12">
        <div class="row">
            <div class="col-sm-4">
                <?= $form->field($profile, 'first_name') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($profile, 'last_name') ?>
            </div>
            <div class="col-sm-4">
                <?= $form->field($profile, 'middle_name') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($user, 'username') ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($user, 'email') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?= $form->field($user, 'password')->passwordInput() ?>
            </div>
            <div class="col-sm-6">
                <?= $form->field($user, 'repassword')->passwordInput() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <?=
                $form->field($user, 'status')->dropDownList(
                    $statusArray,
                    [
                        'prompt' => Yii::t('users', 'BACKEND_PROMPT_STATUS')
                    ]
                ) ?>
            </div>
            <div class="col-sm-6">
                <?=
                $form->field($user, 'role')->dropDownList(
                    $roleArray,
                    [
                        'prompt' => Yii::t('users', 'BACKEND_PROMPT_ROLE')
                    ]
                ) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <?=
                $form->field($user, 'is_shop')->checkbox() ?>
            </div>
        </div>
    </div>
<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    $user->isNewRecord ? Yii::t('users', 'BACKEND_CREATE_SUBMIT') : Yii::t('users', 'BACKEND_UPDATE_SUBMIT'),
    [
        'class' => $user->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>