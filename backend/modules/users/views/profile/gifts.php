<?php

/**
 * Gifts list view.
 *
 * @var \yii\web\View $this View
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var \backend\modules\gifts\models\search\GiftSearch $searchModel Search model
 */

use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;

$this->title = 'Магазин поощрений';
$this->params['subtitle'] = 'Список поощрений';
$this->params['breadcrumbs'] = [
    $this->title
];
$gridId = 'pages-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        'id',
        'points',
        'gift_id'   => [
            'attribute' => 'gift_id',
            'value' => function ($user_gift) {
                return !empty($user_gift->gift) ? $user_gift->gift->name : null;
            }
        ],
        'status:boolean',
        'approve:boolean',
        'created',
        'status'   => [
            'format'    => 'html',
            'label' => 'Статус',
            'value' => function ($model) {
                if ($model->status) {
                    return Html::a('Отменить заказ', [
                        '/users/profile/cancel',
                        'id' => $model->id
                    ], [
                        'class' => 'btn btn-xs btn-danger'
                    ]);
                }
                return 'Заказ отменен';
            }
        ]
    ]
];

$boxButtons = $actions = [];
$showActions = false;

if ($showActions === true) {
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions)
    ];
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>

<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId
            ]); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>