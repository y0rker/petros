<?php

/**
 * Polls my result list view.
 *
 * @var \yii\web\View $this View
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var \backend\modules\polls\models\search\VoteResultMySearch $searchModel Search model
 */


use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use kartik\date\DatePicker;

$this->title = Yii::t('polls', 'BACKEND_VOTE_INDEX_TITLE');
$this->params['subtitle'] = Yii::t('polls', 'BACKEND_VOTE_INDEX_SUBTITLE');
$this->params['breadcrumbs'] = [
    $this->title
];
$gridId = 'polls-result-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'attribute' => 'poll_id',
            'format' => 'html',
            'value' =>
                function ($model) {
                    /**
                     * @var \common\modules\polls\models\vote\Vote $model Model
                     * @return string
                     */
                    return $model->poll->name;
                },
        ],
        [
            'attribute' => 'votes',
            'format' => 'html',
            'value' =>
                function ($model) {
                    /**
                     * @var \common\modules\polls\models\vote\Vote $model Model
                     */
                    $return_string = '';
                    $i = 0;
                    foreach ($model->votesRel AS $vote) {
                        if ($i > 0) $return_string .= '; ';
                        if (!empty($vote->pollVote)) {
                            $return_string .= $vote->pollVote->name;
                            $i++;
                        }
                    }
                    return $return_string;
                },
        ],
        [
            'attribute' => 'points',
            'format' => 'html',
            'label' => 'Заработанные баллы',
            'value' =>
                function ($model) {
                    /**
                     * @var \common\modules\polls\models\vote\Vote $model Model
                     * @return string
                     */
                    return $model->poll->points;
                },
        ],
        [
            'attribute' => 'created_at',
            'format' => 'date',
            'filter' => DatePicker::widget(
                [
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'options' => [
                        'class' => 'form-control'
                    ],
                    'pluginOptions' => [
                        'dateFormat' => 'dd.mm.yy',
                    ]
                ]
            )
        ],
    ]
];

$boxButtons = $actions = [];
$showActions = false;
$boxButtons = null; ?>

<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId
            ]
        ); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>