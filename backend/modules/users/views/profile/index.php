<?php

/**
 * User view.
 *
 * @var \yii\web\View $this View
 * @var \backend\modules\users\models\User $user
 * @var \common\modules\users\models\Profile $profile
 */

use frontend\theme\ThemeAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = 'Личная информация';

$this->params['subtitle'] = 'Вы вошли как <b>'.$user->fullName.'</b>';
$this->params['breadcrumbs'] = [
    $this->title
];
$assetUrl = Yii::$app->assetManager->getAssetUrl(Yii::$app->assetManager->getBundle(ThemeAsset::className()), '');
$this->params['META_IMAGE'] = Yii::$app->urlManager->getHostInfo().$assetUrl.'img/fb_banner.jpg';
?>
<div class="col-md-6">
    <?php $form = ActiveForm::begin(); ?>
    <!-- Widget: user widget style 1 -->
    <div class="box box-widget widget-user">
        <!-- Add the bg color to the header using any of the bg-* classes -->
        <div class="widget-user-header bg-black" style="height: 140px;background: url('<?=$assetUrl;?>img/bg.jpg') center center;">
            <h3 class="widget-user-username"><?=$user->fullName;?></h3>
            <h5 class="widget-user-desc">Вы накопили баллов: <b class="label bg-yellow"><?=$user->points;?></b></h5>
            <div class="widget-user-image" style="margin: 10px 0px;left: auto;position: relative;top: auto;float: right;">
                <a href="/?from=backend" class="btn btn-block btn-success">
                    <i class="fa fa-undo"></i> На главную страницу
                </a>
            </div>
        </div>
        <div class="box-footer">
            <div class="row">
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <h5 class="description-header"><span class="label bg-red">+10 баллов</span></h5>
                        <div class="description-text"><?= $form->field($profile, 'last_name') ?></div>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                    <div class="description-block">
                        <h5 class="description-header"><span class="label bg-red">+10 баллов</span></h5>
                        <div class="description-text"><?= $form->field($profile, 'first_name') ?></div>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                    <div class="description-block">
                        <h5 class="description-header"><span class="label bg-red">+10 баллов</span></h5>
                        <div class="description-text"><?= $form->field($profile, 'middle_name') ?></div>
                    </div>
                    <!-- /.description-block -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 border-right">
                    <h5><span class="label bg-red">+40 баллов</span></h5>
                    <?= $form->field($user, 'username')->textInput([
                        'disabled'  => 'disabled'
                    ]) ?>
                </div>
                <div class="col-sm-4 border-right">
                    <h5><span class="label bg-red">+30 баллов</span></h5>
                    <?= $form->field($user, 'email') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    Рассказать о проекте
                    <div class="ya-share2"
                         data-services="vkontakte,facebook,twitter,telegram"
                         data-title="<?=Yii::$app->name;?>"
                         data-image="<?=Yii::$app->urlManager->getHostInfo().$assetUrl.'img/fb_banner.jpg';?>"
                         data-description="<?=Yii::t('polls', 'MAIN_DESCRIPTION');?>"
                         data-url="<?=Yii::$app->urlManager->getHostInfo();?>">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-right">
                    <?= Html::submitButton(
                        $user->isNewRecord ? Yii::t('users', 'BACKEND_CREATE_SUBMIT') : Yii::t('users', 'BACKEND_UPDATE_SUBMIT'),
                        [
                            'class' => $user->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
                        ]
                    ) ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    <!-- /.widget-user -->
</div>
