<?php

namespace backend\modules\pages\controllers;

use backend\modules\base\components\Controller;
use backend\modules\pages\models\Page;
use backend\modules\pages\Module;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\HttpException;
use backend\modules\pages\models\search\PageSearch;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

use Yii;

/**
 * Default backend controller.
 *
 * @property Module module
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => $this->module->photoTempPath
            ],
        ];
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect(Url::to('/'));
    }

    /**
     * Page list page.
     */
    public function actionIndex()
    {
        $searchModel    = new PageSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->get());
        $statusArray    = Page::getStatusArray();

        return $this->render('index', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
            'statusArray'   => $statusArray,
        ]);
    }

    public function actionShop() {
        return $this->render('shop', [
        ]);
    }

    /**
     * Page's list page.
     * @param $id
     * @return string
     * @throws HttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model'			=> $model,
        ]);
    }

    /**
     * Create Page page.
     */
    public function actionCreate()
    {
        $model = new Page(['scenario' => 'admin-create']);

        $statusArray = Page::getStatusArray();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('site', 'BACKEND_FLASH_FAIL_ADMIN_CREATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('create', [
            'model' 		=> $model,
            'statusArray' 	=> $statusArray,
        ]);
    }

    /**
     * Update Page page.
     *
     * @param integer $id Page ID
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');
        $statusArray = Page::getStatusArray();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('admin', 'BACKEND_FLASH_FAIL_ADMIN_UPDATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('update', [
            'model' 		=> $model,
            'statusArray' 	=> $statusArray,
        ]);
    }

    /**
     * Delete Page page.
     *
     * @param integer $id Page ID
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple Page page.
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $models = $this->findModel($ids);
            foreach ($models as $model) {
                $model->delete();
            }
            return $this->redirect(['index']);
        } else {
            throw new HttpException(400);
        }
    }

    /**
     * Find model by ID.
     *
     * @param integer|array $id Page ID
     *
     * @return \common\modules\pages\models\Page Model
     *
     * @throws HttpException 404 error if Page not found
     */
    protected function findModel($id)
    {
        if (is_array($id)) {
            $model = Page::findAll($id);
        } else {
            $model = Page::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}