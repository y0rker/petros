<?php

/**
 * Page form view.
 *
 * @var \yii\web\View $this View
 * @var \yii\widgets\ActiveForm $form Form
 * @var \backend\modules\pages\models\Page $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use vova07\imperavi\Widget as Imperavi;
use vova07\fileapi\Widget as FileAPI;

?>
<?php $form = ActiveForm::begin(); ?>
<?php $box->beginBody(); ?>
<div class="col-lg-12">
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'subname') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'code') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <?= $form->field($model, 'first_name') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'last_name') ?>
        </div>
        <div class="col-sm-4">
            <?= $form->field($model, 'mid_name') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'description')->widget(
                Imperavi::className(),
                [
                    'settings' => [
                        'minHeight' => 200,
                        'imageGetJson' => Url::to(['/pages/default/imperavi-get']),
                        'imageUpload' => Url::to(['/pages/default/imperavi-image-upload']),
                        'fileUpload' => Url::to(['/pages/default/imperavi-file-upload'])
                    ]
                ]
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'photo')->widget(
                FileAPI::className(),
                [
                    'settings' => [
                        'url' => ['/pages/default/fileapi-upload']
                    ],
                ]
            ) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= $form->field($model, 'visible')->dropDownList($statusArray) ?>
        </div>
    </div>
</div>
<?php $box->endBody(); ?>
<?php $box->beginFooter(); ?>
<?= Html::submitButton(
    $model->isNewRecord ? Yii::t('news', 'BACKEND_CREATE_SUBMIT') : Yii::t(
        'news',
        'BACKEND_UPDATE_SUBMIT'
    ),
    [
        'class' => $model->isNewRecord ? 'btn btn-primary btn-large' : 'btn btn-success btn-large'
    ]
) ?>
<?php $box->endFooter(); ?>
<?php ActiveForm::end(); ?>