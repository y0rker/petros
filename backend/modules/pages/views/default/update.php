<?php

/**
 * Page update view.
 *
 * @var yii\web\View $this View
 * @var \backend\modules\pages\models\Page $model Model
 * @var Box $box Box widget instance
 * @var array $statusArray Statuses array
 */

use backend\modules\base\widgets\Box;

$this->title = Yii::t('pages', 'BACKEND_UPDATE_TITLE');
$this->params['subtitle'] = Yii::t('pages', 'BACKEND_UPDATE_SUBTITLE');
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
];
$boxButtons = ['{cancel}'];

if (Yii::$app->user->can('BCreatePages')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BDeletePages')) {
    $boxButtons[] = '{delete}';
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-success'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons
            ]
        );
        echo $this->render(
            '_form',
            [
                'model' => $model,
                'statusArray' => $statusArray,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>
