<?php
namespace backend\modules\pages\models;

use Yii;

/**
 * Class Page
 * @package backend\modules\pages\models
 * Модель страниц.
 *
 * @property string visible
 *
 */
class Page extends \common\modules\pages\models\Page
{

    /**
     * @return array Status array.
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_UNPUBLISHED 	=> Yii::t('news', 'STATUS_UNPUBLISHED'),
            self::STATUS_PUBLISHED 		=> Yii::t('news', 'STATUS_PUBLISHED'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['visible', 'in', 'range' => array_keys(self::getStatusArray())];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin-create'] = [
            'name',
            'description',
            'code',
            'subname',
            'description',
            'first_name',
            'last_name',
            'mid_name',
            'photo',
            'visible',
        ];
        $scenarios['admin-update'] = [
            'name',
            'description',
            'code',
            'subname',
            'description',
            'first_name',
            'last_name',
            'mid_name',
            'photo',
            'visible',
        ];

        return $scenarios;
    }

    /**
     * @return string Readable iogv status
     */
    public function getVisible()
    {
        $statuses = self::getStatusArray();

        return $statuses[$this->visible];
    }

}