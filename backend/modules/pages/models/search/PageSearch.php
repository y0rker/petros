<?php

namespace backend\modules\pages\models\search;

use backend\modules\pages\models\Page;
use yii\data\ActiveDataProvider;

/**
 * Page search model.
 */
class PageSearch extends Page
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Integer
            ['id', 'integer'],
            // String
            [['name'], 'string', 'max' => 255],
            // Status
            ['visible', 'in', 'range' => array_keys(self::getStatusArray())],
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'id' 		=> $this->id,
                'visible' 	=> $this->visible,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}