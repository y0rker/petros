<?php
namespace backend\modules\pages;

use Yii;
/**
 * Backend-Pages [[Page]]
 */
class Module extends \common\modules\pages\Module
{
    public $controllerNamespace = 'backend\modules\pages\controllers';
}