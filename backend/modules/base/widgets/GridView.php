<?php

namespace backend\modules\base\widgets;


/**
 * Class GridView
 * @package vova07\themes\admin\widgets
 * Theme GridView widget.
 */
class GridView extends \kartik\grid\GridView
{
    public $bordered  = true;
    public $striped = true;
    public $hover = true;
    public $layout    = "<div class='well well-sm'>{summary}</div>\n{items}\n{pager}";
    public $pager = [
        'firstPageLabel'  => true,
        'lastPageLabel'  => true,
    ];
}
