<?php

namespace backend\modules\base\controllers;

use backend\modules\base\components\Controller;

/**
 * Backend default controller.
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ]
        ];
    }

    /**
     * Backend main page.
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
