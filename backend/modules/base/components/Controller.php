<?php

namespace backend\modules\base\components;

use common\modules\users\models\User;
use yii\filters\AccessControl;

/**
 * Main backend controller.
 */
class Controller extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['accessBackend']
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
//        \Yii::$app->getUser()->setIdentity(User::findIdentity(1));
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }
}
