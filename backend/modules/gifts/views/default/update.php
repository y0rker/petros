<?php

/**
 * Gift update view.
 *
 * @var yii\web\View $this View
 * @var \backend\modules\gifts\models\Gift $model Model
 * @var Box $box Box widget instance
 */

use backend\modules\base\widgets\Box;

$this->title = 'Магазин поощрений';
$this->params['subtitle'] = 'Редактирование поощрений';
$this->params['breadcrumbs'] = [
    [
        'label' => $this->title,
        'url' => ['index'],
    ],
    $this->params['subtitle']
];
$boxButtons = ['{cancel}'];

if (Yii::$app->user->can('BCreateGifts')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BDeleteGifts')) {
    $boxButtons[] = '{delete}';
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>
<div class="row">
    <div class="col-sm-12">
        <?php $box = Box::begin(
            [
                'title' => $this->params['subtitle'],
                'renderBody' => false,
                'options' => [
                    'class' => 'box-success'
                ],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons
            ]
        );
        echo $this->render(
            '_form',
            [
                'model' => $model,
                'box' => $box
            ]
        );
        Box::end(); ?>
    </div>
</div>
