<?php

/**
 * Gifts list view.
 *
 * @var \yii\web\View $this View
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var \backend\modules\gifts\models\search\GiftSearch $searchModel Search model
 */

use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;

$this->title = 'Магазин поощрений';
$this->params['subtitle'] = 'Список заказов';
$this->params['breadcrumbs'] = [
    $this->title
];
$gridId = 'pages-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        'id',
        'user_id'   => [
                'attribute' => 'user_id',
            'value' => function ($model) {
                return $model->user->allName;
            }
        ],
        'gift_id'   => [
            'attribute' => 'gift_id',
            'value' => function ($model) {
                return $model->gift->name;
            }
        ],
        'points',
        'status:boolean',
        'approve:boolean',
        'created',
        'approve'   => [
                'format'    => 'html',
                'label' => 'Выдача',
                'value' => function ($model) {
                    if (!$model->status) {
                        return null;
                    }
                    if (!$model->approve) {
                        return Html::a('Выдать товар', [
                            '/gifts/default/approve',
                            'id' => $model->id
                        ], [
                            'class' => 'btn btn-xs btn-success'
                        ]);
                    } else {
                        return Html::a('Отменить выдачу ', [
                            '/gifts/default/disapprove',
                            'id' => $model->id
                        ], [
                            'class' => 'btn btn-xs btn-danger'
                        ]);
                    }
                }
        ],
        'status'   => [
            'format'    => 'html',
            'label' => 'Статус',
            'value' => function ($model) {
                if (!$model->status) {
                    return Html::a('Восстановить заказ', [
                        '/gifts/default/return',
                        'id' => $model->id
                    ], [
                        'class' => 'btn btn-xs btn-success'
                    ]);
                } else {
                    return Html::a('Отменить заказ', [
                        '/gifts/default/cancel',
                        'id' => $model->id
                    ], [
                        'class' => 'btn btn-xs btn-danger'
                    ]);
                }
            }
        ]
    ]
];

$boxButtons = $actions = [];
$showActions = false;

if ($showActions === true) {
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions)
    ];
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>

<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId
            ]); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>