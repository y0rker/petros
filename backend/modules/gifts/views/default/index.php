<?php

/**
 * Gifts list view.
 *
 * @var \yii\web\View $this View
 * @var \yii\data\ActiveDataProvider $dataProvider Data provider
 * @var \backend\modules\gifts\models\search\GiftSearch $searchModel Search model
 */

use backend\modules\base\widgets\Box;
use backend\modules\base\widgets\GridView;
use common\modules\users\models\UserGifts;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\helpers\Html;

$this->title = 'Магазин поощрений';
$this->params['subtitle'] = 'Список поощрений';
$this->params['breadcrumbs'] = [
    $this->title
];
$gridId = 'pages-grid';
$gridConfig = [
    'id' => $gridId,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        [
            'class' => CheckboxColumn::classname()
        ],
        'id',
        [
            'attribute' => 'name',
            'format' => 'html',
            'value' => function ($model) {
                return Html::a(
                    $model['name'],
                    ['update', 'id' => $model['id']]
                );
            }
        ],
        'points',
        'count',
        'ost'   => [
            'label' => 'Осталось',
            'value' => function ($model) {
                $countOrders = UserGifts::find()->where([
                    'gift_id'   => $model->id,
                    'status'    => true
                ])->count();
                if ($model->count > $countOrders) {
                    return $model->count - $countOrders;
                }

                return 0;
            }
        ]
    ]
];

$boxButtons = $actions = [];
$showActions = false;

if (Yii::$app->user->can('BCreateGifts')) {
    $boxButtons[] = '{create}';
}
if (Yii::$app->user->can('BUpdateGifts')) {
    $actions[] = '{update}';
    $showActions = $showActions || true;
}
if (Yii::$app->user->can('BDeleteGifts')) {
    $boxButtons[] = '{batch-delete}';
    $actions[] = '{delete}';
    $showActions = $showActions || true;
}

if ($showActions === true) {
    $gridConfig['columns'][] = [
        'class' => ActionColumn::className(),
        'template' => implode(' ', $actions)
    ];
}
$boxButtons = !empty($boxButtons) ? implode(' ', $boxButtons) : null; ?>

<div class="row">
    <div class="col-xs-12">
        <?php Box::begin(
            [
                'title' => $this->params['subtitle'],
                'bodyOptions' => [
                    'class' => 'table-responsive'
                ],
                'buttonsTemplate' => $boxButtons,
                'grid' => $gridId
            ]); ?>
        <?= GridView::widget($gridConfig); ?>
        <?php Box::end(); ?>
    </div>
</div>