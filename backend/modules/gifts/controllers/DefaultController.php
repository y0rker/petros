<?php

namespace backend\modules\gifts\controllers;

use backend\modules\base\components\Controller;
use backend\modules\gifts\models\Gift;
use backend\modules\gifts\models\search\GiftSearch;
use backend\modules\gifts\models\search\GiftUserSearch;
use backend\modules\gifts\Module;
use common\modules\users\models\UserGifts;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\HttpException;
use vova07\fileapi\actions\UploadAction as FileAPIUpload;

use Yii;

/**
 * Default backend controller.
 *
 * @property Module module
 */
class DefaultController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'fileapi-upload' => [
                'class' => FileAPIUpload::className(),
                'path' => $this->module->photoTempPath
            ],
        ];
    }

    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->redirect(Url::to('/'));
    }

    /**
     * Page list page.
     */
    public function actionIndex()
    {
        $searchModel    = new GiftSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->get());

        return $this->render('index', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
        ]);
    }

    public function actionOrders()
    {
        $searchModel    = new GiftUserSearch();
        $dataProvider   = $searchModel->search(Yii::$app->request->get());

        return $this->render('orders', [
            'dataProvider' 	=> $dataProvider,
            'searchModel' 	=> $searchModel,
        ]);
    }

    /**
     *
     */
    public function actionReturn($id)
    {
        $gift = UserGifts::findOne($id);
        $gift->status = true;
        $gift->save();

        return $this->redirect(['/gifts/default/orders']);
    }

    /**
     *
     */
    public function actionCancel($id)
    {
        $gift = UserGifts::findOne($id);
        $gift->status = false;
        $gift->save();

        return $this->redirect(['/gifts/default/orders']);
    }

    /**
     *
     */
    public function actionApprove($id)
    {
        $gift = UserGifts::findOne($id);
        $gift->approve = true;
        $gift->save();

        return $this->redirect(['/gifts/default/orders']);
    }

    /**
     *
     */
    public function actionDisapprove($id)
    {
        $gift = UserGifts::findOne($id);
        $gift->approve = false;
        $gift->save();

        return $this->redirect(['/gifts/default/orders']);
    }

    /**
     * Create Page page.
     */
    public function actionCreate()
    {
        $model = new Gift(['scenario' => 'admin-create']);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->redirect(['update', 'id' => $model->id]);
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('site', 'BACKEND_FLASH_FAIL_ADMIN_CREATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('create', [
            'model' 		=> $model,
        ]);
    }

    /**
     * Update Page page.
     *
     * @param integer $id Page ID
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->setScenario('admin-update');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save(false)) {
                    return $this->refresh();
                } else {
                    Yii::$app->session->setFlash('danger', Yii::t('admin', 'BACKEND_FLASH_FAIL_ADMIN_UPDATE'));
                    return $this->refresh();
                }
            } elseif (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            }
        }

        return $this->render('update', [
            'model' 		=> $model,
        ]);
    }

    /**
     * Delete Page page.
     *
     * @param integer $id Page ID
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Delete multiple Page page.
     *
     * @return mixed
     * @throws \yii\web\HttpException
     */
    public function actionBatchDelete()
    {
        if (($ids = Yii::$app->request->post('ids')) !== null) {
            $models = $this->findModel($ids);
            foreach ($models as $model) {
                $model->delete();
            }
            return $this->redirect(['index']);
        } else {
            throw new HttpException(400);
        }
    }

    /**
     * Find model by ID.
     *
     * @param integer|array $id Page ID
     *
     * @return \common\modules\gifts\models\Gift Model
     *
     * @throws HttpException 404 error if Page not found
     */
    protected function findModel($id)
    {
        if (is_array($id)) {
            $model = Gift::findAll($id);
        } else {
            $model = Gift::findOne($id);
        }
        if ($model !== null) {
            return $model;
        } else {
            throw new HttpException(404);
        }
    }
}