<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.05.2017
 * Time: 18:31
 */

namespace backend\modules\gifts\models\search;


use backend\modules\gifts\models\Gift;
use yii\data\ActiveDataProvider;

class GiftSearch extends Gift
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Integer
            ['id', 'integer'],
            // String
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                'id' 		=> $this->id,
            ]
        );

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}