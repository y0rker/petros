<?php
/**
 * User: y0rker
 * Date: 12.12.2017
 * Time: 1:39
 */

namespace backend\modules\gifts\models\search;

use common\modules\gifts\models\Gift;
use common\modules\users\models\Profile;
use common\modules\users\models\User;
use common\modules\users\models\UserGifts;
use yii\data\ActiveDataProvider;

/**
 * Class GiftUserSearch
 *
 * @package backend\modules\gifts\models\search
 */
class GiftUserSearch extends UserGifts
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Integer
            [['id'], 'integer'],
            [['status', 'approve'], 'boolean'],
            [['user_id', 'gift_id'], 'safe']
        ];
    }

    /**
     * Creates data provider instance with search query applied.
     *
     * @param array $params Search params
     * @param bool  $by_user
     *
     * @return ActiveDataProvider DataProvider
     */
    public function search($params, $by_user = false)
    {
        $user = \Yii::$app->getUser();
        $query = self::find()
            ->joinWith('user')->joinWith('gift')->joinWith('user.profile');
        if ($by_user) {
            $user_id = (!empty($user)) ? $user->id : 0;
            $query->andWhere([
                self::tableName() . '.user_id'   => $user_id
            ]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params))) {
            return $dataProvider;
        }

        $query->andFilterWhere(
            [
                self::tableName() . '.id'            => $this->id,
                self::tableName() . '.status'        => $this->status,
                self::tableName() . '.approve'       => $this->approve
            ]
        );

        if (!empty($this->user_id)) {
            $query->andWhere([
                'or',
                [
                    'like',
                    User::tableName() . '.username',
                    $this->user_id
                ],
                [
                    'like',
                    Profile::tableName() . '.first_name',
                    $this->user_id
                ],
                [
                    'like',
                    Profile::tableName() . '.last_name',
                    $this->user_id
                ],
                [
                    'like',
                    Profile::tableName() . '.middle_name',
                    $this->user_id
                ],
            ]);
        }

        if (!empty($this->gift_id)) {
            $query->andWhere([
                'like',
                Gift::tableName() . '.name',
                $this->gift_id
            ]);
        }

        return $dataProvider;
    }
}
