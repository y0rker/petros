<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.05.2017
 * Time: 18:31
 */

namespace backend\modules\gifts\models;


class Gift extends \common\modules\gifts\models\Gift
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['admin-create'] = [
            'name',
            'photo',
            'points',
            'points_title',
            'count'
        ];
        $scenarios['admin-update'] = [
            'name',
            'photo',
            'points',
            'points_title',
            'count'
        ];

        return $scenarios;
    }
}