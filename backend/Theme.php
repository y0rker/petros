<?php
namespace backend;
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 16.01.2017
 * Time: 14:20
 */
class Theme extends \yii\base\Theme
{
    /**
     * @inheritdoc
     */
    public $pathMap = [
        '@backend/views' => '@backend/views',
        '@backend/modules' => '@backend/modules'
    ];
}