<?php
namespace common\modules\pages\models\query;

use common\modules\pages\models\Page;
use yii\db\ActiveQuery;

/**
 * Class PageQuery
 * @package common\modules\pages\models\query
 *  Класс кастомных запросов модели [[Page]]
 */
class PageQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые страницы.
     * @return $this
     */
    public function published()
    {
        $this->andWhere(Page::tableName() . '.visible = :visible', [':visible' => Page::STATUS_PUBLISHED]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые страницы.
     * @return $this
     */
    public function unpublished()
    {
        $this->andWhere(Page::tableName() . '.visible = :visible', [':visible' => Page::STATUS_UNPUBLISHED]);
        return $this;
    }

}