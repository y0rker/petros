<?php
namespace common\modules\pages\models;

use common\modules\pages\models\query\PageQuery;
use common\modules\pages\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Page
 * @package common\modules\pages\models
 * Модель опроса.
 *
 * @property integer $id ID
 * @property string $name Наименования
 * @property string $code
 * @property string $subname
 * @property string $description Текст
 * @property string $first_name
 * @property string $last_name
 * @property string $mid_name
 * @property string $photo
 * @property boolean $visible Статус публикации
 *
 *
 * @property \common\modules\pages\Module module
 *
 */
class Page extends ActiveRecord
{

    use ModuleTrait;

    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pages}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PageQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'photo' => [
                        'path' => $this->module->photoPath,
                        'tempPath' => $this->module->photoTempPath,
                        'url' => $this->module->photoUrl
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subname', 'first_name', 'last_name', 'mid_name'],'safe'],
            // Обязательные поля
            [['name', 'description', 'code', 'visible'], 'required'],
            // Visible
            [
                ['visible'],
                'default',
                'value' => self::STATUS_PUBLISHED
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			            => Yii::t('pages', 'ID'),
            'name'                      => Yii::t('pages', 'ATTR_NAME'),
            'use_alternative_template'  => Yii::t('pages', 'ATTR_ALTERNATE_TEMPLATE'),
            'description'               => Yii::t('pages', 'ATTR_DESCRIPTION'),
            'visible'                   => Yii::t('pages', 'ATTR_VISIBLE'),
            'first_name'                => 'Имя',
            'last_name'                 => 'Фамилия',
            'mid_name'                  => 'Отчество',
            'photo'                     => 'Фотография',
            'subname'                   => 'Дополнительный заголовок',
            'code'                      => 'Код внутренний'
        ];
    }

    /**
     * @param $alias
     * @return self|ActiveRecord|null
     */
    public static function findByAlias($alias) {
        return self::find()->where([
            'code'  => $alias
        ])->published()->one();
    }
}