<?php
return [
    // АДМИНКА
    'BACKEND_INDEX_TITLE'       => 'Страницы',
    'BACKEND_INDEX_SUBTITLE'    => 'Список страниц',
    'BACKEND_CREATE_TITLE'      => 'Страницы',
    'BACKEND_CREATE_SUBTITLE'   => 'Создание страницы',
    'BACKEND_UPDATE_TITLE'      => 'Страницы',
    'BACKEND_UPDATE_SUBTITLE'   => 'Обновление страницы',
    // АТТРИБУТЫ
    'ID'                => 'ID',
    'ATTR_NAME'         => 'Наименование страницы',
    'ATTR_ALIAS'        => 'Алиас страницы',
    'ATTR_DESCRIPTION'  => 'Описание',
    'ATTR_VISIBLE'      => 'Видимость',
    'ATTR_ALTERNATE_TEMPLATE'   => 'Альтернативное отображение'
];