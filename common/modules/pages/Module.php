<?php
namespace common\modules\pages;


/**
 * Страницы [[Pages]]
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Кол-во на странице
     */
    public $recordsPerPage = 20;

    /**
     * @var string Image path
     */
    public $photoPath = '@statics/web/pages/images/';

    /**
     * @var string Images temporary path
     */
    public $photoTempPath = '@statics/temp/pages/images/';

    /**
     * @var string Image URL
     */
    public $photoUrl = '/statics/pages/images';
}