<?php
return [
    'accessBackend' => [
        'type' => 2,
        'description' => 'Can access backend',
    ],
    'administrateRbac' => [
        'type' => 2,
        'description' => 'Can administrate all "RBAC" module',
        'children' => [
            'BViewRoles',
            'BCreateRoles',
            'BUpdateRoles',
            'BDeleteRoles',
            'BViewPermissions',
            'BCreatePermissions',
            'BUpdatePermissions',
            'BDeletePermissions',
            'BViewRules',
            'BCreateRules',
            'BUpdateRules',
            'BDeleteRules',
        ],
    ],
    'BViewRoles' => [
        'type' => 2,
        'description' => 'Can view roles list',
    ],
    'BCreateRoles' => [
        'type' => 2,
        'description' => 'Can create roles',
    ],
    'BUpdateRoles' => [
        'type' => 2,
        'description' => 'Can update roles',
    ],
    'BDeleteRoles' => [
        'type' => 2,
        'description' => 'Can delete roles',
    ],
    'BViewPermissions' => [
        'type' => 2,
        'description' => 'Can view permissions list',
    ],
    'BCreatePermissions' => [
        'type' => 2,
        'description' => 'Can create permissions',
    ],
    'BUpdatePermissions' => [
        'type' => 2,
        'description' => 'Can update permissions',
    ],
    'BDeletePermissions' => [
        'type' => 2,
        'description' => 'Can delete permissions',
    ],
    'BViewRules' => [
        'type' => 2,
        'description' => 'Can view rules list',
    ],
    'BCreateRules' => [
        'type' => 2,
        'description' => 'Can create rules',
    ],
    'BUpdateRules' => [
        'type' => 2,
        'description' => 'Can update rules',
    ],
    'BDeleteRules' => [
        'type' => 2,
        'description' => 'Can delete rules',
    ],
    'user' => [
        'type' => 1,
        'description' => 'User',
    ],
    'iogv' => [
        'type' => 1,
        'description' => 'Iogv',
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Admin',
        'children' => [
            'user',
            'iogv',
        ],
    ],
    'superadmin' => [
        'type' => 1,
        'description' => 'Super admin',
        'children' => [
            'admin',
            'accessBackend',
            'administrateRbac',
            'administratePages',
            'administratePolls',
            'administrateNews',
        ],
    ],
    'administratePages' => [
        'type' => 2,
        'description' => 'Can administrate all "Pages" module',
        'children' => [
            'BViewPages',
            'BCreatePages',
            'BUpdatePages',
            'BDeletePages',
            'viewPages',
            'createPages',
            'updatePages',
            'deletePages',
        ],
    ],
    'BViewPages' => [
        'type' => 2,
        'description' => 'Can view backend pages list',
    ],
    'BCreatePages' => [
        'type' => 2,
        'description' => 'Can create backend pages',
    ],
    'BUpdatePages' => [
        'type' => 2,
        'description' => 'Can update backend pages',
    ],
    'BDeletePages' => [
        'type' => 2,
        'description' => 'Can delete backend pages',
    ],
    'viewPages' => [
        'type' => 2,
        'description' => 'Can view pages list',
    ],
    'createPages' => [
        'type' => 2,
        'description' => 'Can create pages',
    ],
    'updatePages' => [
        'type' => 2,
        'description' => 'Can update pages',
    ],
    'deletePages' => [
        'type' => 2,
        'description' => 'Can delete pages',
    ],
    'administratePolls' => [
        'type' => 2,
        'description' => 'Can administrate all "Polls" module',
        'children' => [
            'BViewPolls',
            'BCreatePolls',
            'BUpdatePolls',
            'BDeletePolls',
        ],
    ],
    'BViewPolls' => [
        'type' => 2,
        'description' => 'Can view backend polls list',
    ],
    'BCreatePolls' => [
        'type' => 2,
        'description' => 'Can create backend polls',
    ],
    'BUpdatePolls' => [
        'type' => 2,
        'description' => 'Can update backend polls',
    ],
    'BDeletePolls' => [
        'type' => 2,
        'description' => 'Can delete backend polls',
    ],
    'administrateNews' => [
        'type' => 2,
        'description' => 'Can administrate all "News" module',
        'children' => [
            'BViewNews',
            'BCreateNews',
            'BUpdateNews',
            'BDeleteNews',
            'viewNews',
            'createNews',
            'updateNews',
            'updateOwnNews',
            'deleteNews',
            'deleteOwnNews',
        ],
    ],
    'BViewNews' => [
        'type' => 2,
        'description' => 'Can view backend news list',
    ],
    'BCreateNews' => [
        'type' => 2,
        'description' => 'Can create backend news',
    ],
    'BUpdateNews' => [
        'type' => 2,
        'description' => 'Can update backend news',
    ],
    'BDeleteNews' => [
        'type' => 2,
        'description' => 'Can delete backend news',
    ],
    'viewNews' => [
        'type' => 2,
        'description' => 'Can view news list',
    ],
    'createNews' => [
        'type' => 2,
        'description' => 'Can create news',
    ],
    'updateNews' => [
        'type' => 2,
        'description' => 'Can update news',
        'children' => [
            'updateOwnNews',
        ],
    ],
    'updateOwnNews' => [
        'type' => 2,
        'description' => 'Can update own user profile',
        'ruleName' => 'author',
    ],
    'deleteNews' => [
        'type' => 2,
        'description' => 'Can delete news',
        'children' => [
            'deleteOwnNews',
        ],
    ],
    'deleteOwnNews' => [
        'type' => 2,
        'description' => 'Can delete own news profile',
        'ruleName' => 'author',
    ],
];
