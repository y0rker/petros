<?php

namespace common\modules\news\traits;

use common\modules\news\Module;
use Yii;

/**
 * Class ModuleTrait
 * @package vova07\blogs\traits
 * Implements `getModule` method, to receive current module instance.
 */
trait ModuleTrait
{
    /**
     * @var Module|null Module instance
     */
    private $_module;

    /**
     * @return Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $this->_module = Yii::$app->getModule('news');
        }
        return $this->_module;
    }
}
