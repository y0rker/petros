<?php

namespace common\modules\news\commands;

use Yii;
use yii\console\Controller;

/**
 * News RBAC controller.
 */
class RbacController extends Controller
{
    /**
     * @inheritdoc
     */
    public $defaultAction = 'add';

    /**
     * @var array Main module permission array
     */
    public $mainPermission = [
        'name' => 'administrateNews',
        'description' => 'Can administrate all "News" module'
    ];

    /**
     * @var array Permission
     */
    public $permissions = [
        'BViewNews' => [
            'description' => 'Can view backend news list'
        ],
        'BCreateNews' => [
            'description' => 'Can create backend news'
        ],
        'BUpdateNews' => [
            'description' => 'Can update backend news'
        ],
        'BDeleteNews' => [
            'description' => 'Can delete backend news'
        ],
        'viewNews' => [
            'description' => 'Can view news list'
        ],
        'createNews' => [
            'description' => 'Can create news'
        ],
        'updateNews' => [
            'description' => 'Can update news'
        ],
        'updateOwnNews' => [
            'description' => 'Can update own user profile',
            'rule' => 'author'
        ],
        'deleteNews' => [
            'description' => 'Can delete news'
        ],
        'deleteOwnNews' => [
            'description' => 'Can delete own news profile',
            'rule' => 'author'
        ]
    ];

    /**
     * Add comments RBAC.
     */
    public function actionAdd()
    {
        $auth = Yii::$app->authManager;
        $superadmin = $auth->getRole('superadmin');
        $mainPermission = $auth->createPermission($this->mainPermission['name']);
        if (isset($this->mainPermission['description'])) {
            $mainPermission->description = $this->mainPermission['description'];
        }
        if (isset($this->mainPermission['rule'])) {
            $mainPermission->ruleName = $this->mainPermission['rule'];
        }
        $auth->add($mainPermission);

        foreach ($this->permissions as $name => $option) {
            $permission = $auth->createPermission($name);
            if (isset($option['description'])) {
                $permission->description = $option['description'];
            }
            if (isset($option['rule'])) {
                $permission->ruleName = $option['rule'];
            }
            $auth->add($permission);
            $auth->addChild($mainPermission, $permission);
        }

        $auth->addChild($superadmin, $mainPermission);

        $updateNews = $auth->getPermission('updateNews');
        $updateOwnNews = $auth->getPermission('updateOwnNews');
        $deleteNews = $auth->getPermission('deleteNews');
        $deleteOwnNews = $auth->getPermission('deleteOwnNews');

        $auth->addChild($updateNews, $updateOwnNews);
        $auth->addChild($deleteNews, $deleteOwnNews);

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Remove comments RBAC.
     */
    public function actionRemove()
    {
        $auth = Yii::$app->authManager;
        $permissions = array_keys($this->permissions);

        foreach ($permissions as $name => $option) {
            $permission = $auth->getPermission($name);
            $auth->remove($permission);
        }

        $mainPermission = $auth->getPermission($this->mainPermission['name']);
        $auth->remove($mainPermission);

        return static::EXIT_CODE_NORMAL;
    }
}
