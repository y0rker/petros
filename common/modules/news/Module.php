<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 15:23
 */

namespace common\modules\news;


class Module extends \yii\base\Module
{
    /**
     * @var integer Posts per page
     */
    public $recordsPerPage = 20;

    /**
     * @var string Preview path
     */
    public $previewPath = '@statics/web/news/previews/';

    /**
     * @var string Image path
     */
    public $imagePath = '@statics/web/news/images/';

    /**
     * @var string Files path
     */
    public $filePath = '@statics/web/news/files';

    /**
     * @var string Files path
     */
    public $contentPath = '@statics/web/news/content';

    /**
     * @var string Images temporary path
     */
    public $imagesTempPath = '@statics/temp/news/images/';

    /**
     * @var string Preview URL
     */
    public $previewUrl = '/statics/news/previews';

    /**
     * @var string Image URL
     */
    public $imageUrl = '/statics/news/images';

    /**
     * @var string Files URL
     */
    public $fileUrl = '/statics/news/files';

    /**
     * @var string Files URL
     */
    public $contentUrl = '/statics/news/content';
}