<?php

namespace common\modules\news\models;

use common\behaviors\PurifierBehavior;
use common\modules\news\Module;
use common\modules\news\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class News
 * @package common\modules\blogs\models
 * Blog model.
 *
 * @property integer $id ID
 * @property string $title Title
 * @property string $alias Alias
 * @property string $snippet Intro text
 * @property string $content Content
 * @property string $image_url
 * @property string $preview_url
 * @property integer $views Views
 * @property integer $status_id Status
 * @property integer $created_at Created time
 * @property integer $updated_at Updated time
 * @property boolean $private
 * @property string $news_date
 *
 * @property Module module
 *
 */
class News extends ActiveRecord
{
    use ModuleTrait;

    /** Unpublished status **/
    const STATUS_UNPUBLISHED = 0;
    /** Published status **/
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%blogs}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new NewsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ],
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'preview_url' => [
                        'path' => $this->module->previewPath,
                        'tempPath' => $this->module->imagesTempPath,
                        'url' => $this->module->previewUrl
                    ],
                    'image_url' => [
                        'path' => $this->module->imagePath,
                        'tempPath' => $this->module->imagesTempPath,
                        'url' => $this->module->imageUrl
                    ]
                ]
            ],
            'purifierBehavior' => [
                'class' => PurifierBehavior::className(),
                'attributes' => [
                    self::EVENT_BEFORE_VALIDATE => [
                        'snippet',
                        'content' => [
                            'HTML.AllowedElements' => '',
                            'AutoFormat.RemoveEmpty' => true
                        ]
                    ]
                ],
                'textAttributes' => [
                    self::EVENT_BEFORE_VALIDATE => ['title', 'alias']
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Required
            [['title', 'content', 'alias'], 'required'],
            [['private', 'news_date'], 'safe'],
            // Trim
            [['title', 'snippet', 'content'], 'trim'],
            // Status
            [
                'status_id',
                'default',
                'value' => self::STATUS_PUBLISHED
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('news', 'ATTR_ID'),
            'title'         => Yii::t('news', 'ATTR_TITLE'),
            'alias'         => Yii::t('news', 'ATTR_ALIAS'),
            'snippet'       => Yii::t('news', 'ATTR_SNIPPET'),
            'content'       => Yii::t('news', 'ATTR_CONTENT'),
            'views'         => Yii::t('news', 'ATTR_VIEWS'),
            'status_id'     => Yii::t('news', 'ATTR_STATUS'),
            'created_at'    => Yii::t('news', 'ATTR_CREATED'),
            'updated_at'    => Yii::t('news', 'ATTR_UPDATED'),
            'preview_url'   => Yii::t('news', 'ATTR_PREVIEW_URL'),
            'image_url'     => Yii::t('news', 'ATTR_IMAGE_URL'),
            'private'       => Yii::t('news', 'ATTR_PRIVATE'),
            'news_date'     => Yii::t('news', 'ATTR_NEWS_DATE'),
        ];
    }

    /**
     * @param $count
     * @return self[]|ActiveRecord[]
     */
    public static function findLastNewsByCount($count) {
        return self::find()->published()->limit($count)->orderBy('created_at DESC')->notPrivate()->all();
    }
}
