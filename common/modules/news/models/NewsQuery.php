<?php

namespace common\modules\news\models;

use common\modules\news\traits\ModuleTrait;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class NewsQuery
 * @package common\modules\news\models
 */
class NewsQuery extends ActiveQuery
{
    use ModuleTrait;

    /**
     * Select published posts.
     *
     * @return $this
     */
    public function published()
    {
        $this->andWhere(['status_id' => News::STATUS_PUBLISHED]);

        return $this;
    }

    /**
     * Выбираем только не привытные опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notPrivate()
    {
        if (!Yii::$app->user->can('iogv')) {
            $this->andWhere(News::tableName() . '.private != :private', [':private' => News::STATUS_PUBLISHED]);
        }
        return $this;
    }

    /**
     * Select unpublished posts.
     *
     * @return $this
     */
    public function unpublished()
    {
        $this->andWhere(['status_id' => News::STATUS_UNPUBLISHED]);

        return $this;
    }
}
