<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 19.05.2017
 * Time: 18:27
 */

namespace common\modules\polls\helpers;


class ParamsHelper
{
    /**
     * @param string $param_code
     * @param string $item_code
     * @param array $params
     * @return array|string
     */
    public static function getValue(string $param_code, string $item_code, array $params) {
        $values = [];
        foreach ($params AS $param) {
            if ($param['param_code'] == $param_code && $param['item_code'] == $item_code) {
                $values[] = $param['value'];
            }
        }
        if (count($values) == 1) {
            return $values[0];
        } else {
            return $values;
        }
    }
}