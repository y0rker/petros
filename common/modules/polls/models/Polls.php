<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 16:37
 */

namespace common\modules\polls\models;


use common\modules\polls\models\query\PollsQuery;
use common\modules\polls\models\vote\Vote;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Polls
 * @package common\modules\polls\models
 * Модель опросов.
 *
 * @property integer $id
 * @property string $name
 * @property string $sub_name
 * @property string $alias
 * @property integer $points
 * @property boolean $visible
 * @property boolean $need_comment
 * @property integer $created_at
 * @property integer $updated_at
 * @property boolean $complete
 * @property boolean $private
 * @property integer $date_from
 * @property integer $date_to
 *
 * @property string visibleName
 * @property PollsVotes[] votes
 * @property Vote[] votesFront
 * @property PollsGallery[] gallery
 *
 */
class Polls extends ActiveRecord
{

    public $params = [];

    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polls}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PollsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'id',
                'need_comment',
                'count_balls',
                'complete',
                'private',
                'date_from',
                'date_to',
                'sub_name'
            ],'safe'],
            // Обязательные поля
            [['name', 'visible'], 'required'],
            // Visible
            [
                ['visible', 'status'],
                'default',
                'value' => self::STATUS_PUBLISHED
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
            'name'              => Yii::t('polls', 'ATTR_NAME'),
            'visible'           => Yii::t('polls', 'ATTR_VISIBLE'),
            'need_comment'      => Yii::t('polls', 'ATTR_NEED_COMMENT'),
            'points'            => Yii::t('polls', 'ATTR_POINTS'),
            'complete'          => Yii::t('polls', 'ATTR_COMPLETE'),
            'private'           => Yii::t('polls', 'ATTR_PRIVATE'),
            'date_from'         => Yii::t('polls', 'ATTR_DATE_FROM'),
            'date_to'           => Yii::t('polls', 'ATTR_DATE_TO'),
            'sub_name'          => Yii::t('polls', 'ATTR_SUB_NAME'),
        ];
    }

    /**
     * @return PollsVotes[]|\yii\db\ActiveQuery
     */
    public function getVotes()
    {
        return $this->hasMany(PollsVotes::className(), ['poll_id' => 'id'])->andWhere([
            PollsVotes::tableName().'.visible'   => PollsVotes::STATUS_PUBLISHED
        ]);
    }

    /**
     * @return PollsVotes[]|\yii\db\ActiveQuery
     */
    public function getVotesFront()
    {
        return $this->hasMany(Vote::className(), ['poll_id' => 'id']);
    }

    /**
     * @return PollsGallery[]|\yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasMany(PollsGallery::className(), ['poll_id' => 'id']);
    }

    /**
     * @return self[]|ActiveRecord[]
     */
    public static function findAllPolls() {
        return self::find()->all();
    }

    /**
     * @param int $count
     * @param bool $current
     * @return Polls[]
     */
    public static function findLastPollsByCount(int $count, bool $current = false): array {
        $query = self::find();
        if ($count > 0) {
            $query->limit($count);
        }
        if ($current) {
            $query = $query->active();
        }
        $query = $query->published()->notPrivate();
        $list = $query->orderBy('complete ASC')->all();
        return $list;
    }

    /**
     * @return bool
     */
    public function isUserVoted(): bool {
        if (!Yii::$app->user->isGuest) {
            $countVotes = Vote::findVotePollByUser($this->id);
            if ($countVotes > 0) {
                return true;
            }
        }
        return false;
    }
}