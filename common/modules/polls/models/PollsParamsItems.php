<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 17:31
 */

namespace common\modules\polls\models;


use common\modules\polls\models\query\PollsParamsItemsQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class PollsParamsItems
 * @package common\modules\polls\models
 *
 * @property integer $id
 * @property integer $poll_param_id
 * @property string $name
 * @property boolean $multiple
 * @property string $code
 * @property boolean big_text
 */
class PollsParamsItems extends ActiveRecord
{
    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    const MULTIPLE_TRUE = 1;
    const MULTIPLE_FALSE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polls_params_items}}';
    }

    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PollsParamsItemsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'multiple', 'big_text'],'safe'],
            // Обязательные поля
            [['name', 'code', 'poll_param_id'], 'required'],
            // Visible
            [
                ['visible'],
                'default',
                'value' => self::STATUS_PUBLISHED
            ],
            // Multiple
            [
                ['multiple'],
                'default',
                'value' => self::MULTIPLE_FALSE
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
            'name'              => Yii::t('polls', 'ATTR_NAME'),
            'visible'           => Yii::t('polls', 'ATTR_VISIBLE'),
            'multiple'          => 'Множественный',
            'code'              => 'Код',
        ];
    }

    /**
     * @param $poll_params_id
     * @return self[]
     */
    public static function findAllItemsByPollParamsId($poll_params_id) {
        return self::find()->where([
            'poll_param_id' => $poll_params_id
        ])->all();
    }
}