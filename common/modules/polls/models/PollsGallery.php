<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 01.06.2017
 * Time: 12:17
 */

namespace common\modules\polls\models;


use common\modules\polls\Module;
use common\modules\polls\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class PollsGallery
 * @package common\modules\polls\models
 *
 * @property integer $id
 * @property integer $poll_id
 * @property string $name
 * @property string $image
 *
 * @property Module module
 * @property Polls poll
 */
class PollsGallery extends ActiveRecord
{

    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polls_gallery}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => $this->module->galleryPath,
                        'tempPath' => $this->module->galleryTempPath,
                        'url' => $this->module->galleryUrl
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
            'name'              => 'Заголовок',
            'image'             => 'Фотография',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'],'safe'],
            // Обязательные поля
            [['image', 'poll_id'], 'required'],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoll() {
        return $this->hasOne(Polls::className(), ['id' => 'poll_id']);
    }
}