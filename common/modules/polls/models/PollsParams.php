<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 17:20
 */

namespace common\modules\polls\models;


use common\modules\polls\models\query\PollsParamsQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Class PollsParams
 * @package common\modules\polls\models
 *
 * @property integer $id
 * @property string $name
 * @property boolean $visible
 * @property integer $created_at
 * @property integer $updated_at
 */
class PollsParams extends ActiveRecord
{
    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polls_params}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PollsParamsQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'id',
            ],'safe'],
            // Обязательные поля
            [['name', 'visible', 'code'], 'required'],
            // Visible
            [
                ['visible'],
                'default',
                'value' => self::STATUS_PUBLISHED
            ],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
            'name'              => Yii::t('polls', 'ATTR_NAME'),
            'visible'           => Yii::t('polls', 'ATTR_VISIBLE'),
            'code'              => 'Код'
        ];
    }

    public static function getParamsArrayByPollId($poll_id) {
        $result = (new Query())
            ->select([
                'params.code AS param_code',
                'item.code AS item_code',
                'val.value'
            ])
            ->from(['val' => PollsParamsItemsValues::tableName()])
            ->innerJoin(['item' => PollsParamsItems::tableName()],
                'item.id = val.poll_param_item_id')
            ->innerJoin(['params' => PollsParams::tableName()],
                'params.id = item.poll_param_id')
            ->where([
                'val.poll_id'   => $poll_id
            ])->all();
        return $result;
    }
}