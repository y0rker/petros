<?php
namespace common\modules\polls\models\query;

use common\modules\polls\models\Polls;
use common\modules\polls\models\PollsParamsItems;
use yii\db\ActiveQuery;

/**
 * Class PollsParamsItemsValuesQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[PollsParams]]
 */
class PollsParamsItemsValuesQuery extends ActiveQuery
{

}