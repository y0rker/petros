<?php
namespace common\modules\polls\models\query;

use common\modules\polls\models\Polls;
use yii\db\ActiveQuery;

/**
 * Class PollsParamsQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[PollsParams]]
 */
class PollsParamsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function published()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [':visible' => Polls::STATUS_PUBLISHED]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function unpublished()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [':visible' => Polls::STATUS_UNPUBLISHED]);
        return $this;
    }

}