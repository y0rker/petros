<?php
namespace common\modules\polls\models\query;

use common\modules\polls\models\Polls;
use common\modules\polls\models\PollsParamsItems;
use yii\db\ActiveQuery;

/**
 * Class PollsParamsItemsQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[PollsParams]]
 */
class PollsParamsItemsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function published()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [':visible' => Polls::STATUS_PUBLISHED]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function unpublished()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [':visible' => Polls::STATUS_UNPUBLISHED]);
        return $this;
    }

    /**
     * Выбираем только множественный.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function multiple()
    {
        $this->andWhere(PollsParamsItems::tableName() . '.multiple = :multiple', [':multiple' => PollsParamsItems::MULTIPLE_TRUE]);
        return $this;
    }

    /**
     * Выбираем только не множественный.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notMultiple()
    {
        $this->andWhere(PollsParamsItems::tableName() . '.multiple = :multiple', [':multiple' => PollsParamsItems::MULTIPLE_FALSE]);
        return $this;
    }

}