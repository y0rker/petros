<?php
namespace common\modules\polls\models\query;

use common\modules\polls\models\Polls;
use Yii;
use yii\db\ActiveQuery;

/**
 * Class PollQuery
 * @package common\modules\polls\models\query
 *  Класс кастомных запросов модели [[Poll]]
 */
class PollsQuery extends ActiveQuery
{
    /**
     * Выбираем только опубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function published()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [':visible' => Polls::STATUS_PUBLISHED]);
        return $this;
    }

    /**
     * Выбираем только не привытные опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function notPrivate()
    {
        if (!Yii::$app->user->can('iogv')) {
            $this->andWhere(Polls::tableName() . '.private != :private', [':private' => Polls::STATUS_PUBLISHED]);
        }
        return $this;
    }

    public function active() {
        $this->andWhere(Polls::tableName().'.complete = :complete', [
            ':complete' => false
        ]);
        return $this;
    }

    /**
     * Выбираем только неопубликованые опросы.
     * @return $this
     * @internal param \yii\db\ActiveQuery $query
     */
    public function unpublished()
    {
        $this->andWhere(Polls::tableName() . '.visible = :visible', [':visible' => Polls::STATUS_UNPUBLISHED]);
        return $this;
    }

}