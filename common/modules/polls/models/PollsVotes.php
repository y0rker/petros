<?php

namespace common\modules\polls\models;


use common\modules\polls\models\vote\Vote;
use common\modules\polls\models\vote\VoteList;
use common\modules\polls\Module;
use common\modules\polls\traits\ModuleTrait;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * Class PollsVotes
 *
 * @package common\modules\polls\models
 *
 * @property integer $id
 * @property string $name
 * @property integer $poll_id
 * @property string $description
 * @property string $photo
 * @property boolean $visible
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Module module
 * @property integer count
 * @property integer accepted
 * @property integer notaccepted
 */
class PollsVotes extends ActiveRecord
{

    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polls_votes}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'photo' => [
                        'path' => $this->module->photoPath,
                        'tempPath' => $this->module->photoTempPath,
                        'url' => $this->module->photoUrl
                    ],
                ]
            ],
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
            'name'              => 'Заголовок',
            'visible'           => Yii::t('polls', 'ATTR_VISIBLE'),
            'subtitle'          => 'Дополнительный заголовок',
            'description'       => 'Описание',
            'first_name'        => 'Имя',
            'last_name'         => 'Фамилия',
            'mid_name'          => 'Отчество',
            'photo'             => 'Фотография',
            'count'             => 'Количество проголовавших'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'description', 'photo'],'safe'],
            // Обязательные поля
            [['name', 'visible', 'poll_id'], 'required'],
            // Visible
            [
                ['visible', 'status'],
                'default',
                'value' => self::STATUS_PUBLISHED
            ],
        ];
    }

    /**
     * @return int
     */
    public function getCount() {
        $query = new Query();
        $query->from(VoteList::tableName().' AS vote');
        $query->where([
            'vote.poll_vote_id'  => $this->id
        ]);
        $query->join('JOIN', Vote::tableName().' AS us', 'us.id = vote.vote_id');

        return $query->count();
    }

    /**
     * @return int
     */
    public function getAccepted() {
        $query = new Query();
        $query->from(VoteList::tableName().' AS vote');
        $query->where([
            'vote.poll_vote_id'   => $this->id,
            'us.accepted'         => true
        ]);
        $query->join('JOIN', Vote::tableName().' AS us', 'us.id = vote.vote_id');

        return $query->count();
    }

    /**
     * @return int
     */
    public function getNotaccepted() {
        $query = new Query();
        $query->from(VoteList::tableName().' AS vote');
        $query->where([
            'vote.poll_vote_id'   => $this->id,
            'us.accepted'         => false
        ]);
        $query->join('JOIN', Vote::tableName().' AS us', 'us.id = vote.vote_id');

        return $query->count();
    }
}