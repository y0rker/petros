<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 17:31
 */

namespace common\modules\polls\models;


use common\modules\polls\models\query\PollsParamsItemsValuesQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class PollsParamsItemsValues
 * @package common\modules\polls\models
 *
 * @property integer $id
 * @property integer $poll_param_item_id
 * @property integer $poll_id
 * @property string $value
 */
class PollsParamsItemsValues extends ActiveRecord
{
    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polls_params_items_values}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new PollsParamsItemsValuesQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Обязательные поля
            [['value', 'poll_id', 'poll_param_item_id'], 'required'],
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
        ];
    }

    /**
     * @param $poll_id
     * @return self[]
     */
    public static function getParamsByPollId($poll_id) {
        return self::find()->where([
            'poll_id'   => $poll_id
        ])->all();
    }
}