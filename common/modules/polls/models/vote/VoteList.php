<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 21.01.2017
 * Time: 15:37
 */

namespace common\modules\polls\models\vote;


use common\modules\polls\models\PollsVotes;
use common\modules\polls\traits\ModuleTrait;
use yii\db\ActiveRecord;

/**
 * Class VoteList
 * @package common\modules\polls\models\vote
 *
 * @property integer $vote_id
 * @property integer $poll_vote_id
 * @property integer $poll_id
 *
 * @property Vote vote
 * @property PollsVotes pollVote
 * @property mixed votesrel
 */
class VoteList extends ActiveRecord
{
    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%votes_vote}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Обязательные поля
            [['vote_id', 'poll_vote_id', 'poll_id'], 'required'],
        ];
    }

    public function getVote() {
        return $this->hasOne(Vote::className(), ['id' => 'vote_id']);
    }

    public function getPollVote() {
        return $this->hasOne(PollsVotes::className(), ['id' => 'poll_vote_id']);
    }
}