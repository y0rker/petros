<?php
namespace common\modules\polls\models\vote;


use common\modules\polls\models\Polls;
use common\modules\polls\models\PollsVotes;
use common\modules\polls\Module;
use common\modules\polls\traits\ModuleTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Vote
 * @package frontend\modules\polls\models\vote
 *
 * @property integer $id
 * @property integer $poll_id
 * @property integer $user_id
 * @property string $comment
 * @property boolean $accepted
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property VoteList[] votesRel
 * @property PollsVotes[] votes
 * @property Polls poll
 * @property Module module
 */
class Vote extends ActiveRecord
{
    use ModuleTrait;

    public $votes_array;
    public $points = 80;


    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%votes}}';
    }

    /**
     * @return array Status array.
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_UNPUBLISHED 	=> 'Не подтвержден',
            self::STATUS_PUBLISHED 		=> 'Подтвержден',
        ];
    }

    /**
     * @return string Readable result status
     */
    public function getVisible()
    {
        $statuses = self::getStatusArray();

        return $statuses[$this->accepted];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['save'] = [
            'poll_id',
            'user_id',
            'comment',
            'created_at',
            'votes_array'
        ];
        $scenarios['update-visible'] = [
            'visible',
            'updated_at'
        ];

        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Обязательные поля
            [['user_id'], 'required'],
            [['votes_array', 'comment'], 'safe'],
            [['comment'], 'default', 'value' => '<p></p>'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' 			    => Yii::t('polls', 'ID'),
            'accepted'          => 'Подтвержден',
            'votes'             => 'Ваш выбор',
            'points'            => 'Баллы',
            'created_at'        => 'Дата голоса',
            'poll_id'           => 'Голосование'
        ];
    }

    /**
     * @return PollsVotes[]
     */
    public function getVotes() {
        return PollsVotes::find()->where([
            'id'    => $this->votes_array
        ])->all();
    }

    /**
     * @return VoteList[]|\yii\db\ActiveQuery
     */
    public function getVotesRel() {
        return $this->hasMany(VoteList::className(), ['vote_id' => 'id']);
    }

    /**
     * @return Polls|\yii\db\ActiveQuery
     */
    public function getPoll() {
        return $this->hasOne(Polls::className(), ['id' => 'poll_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $votes = json_decode($this->votes_array);
        if (is_array($votes)) {
            foreach ($votes AS $vote) {
                if ($vote > 0) {
                    $voteModel = new VoteList();
                    $voteModel->vote_id = $this->id;
                    $voteModel->poll_vote_id = $vote;
                    $voteModel->poll_id = $this->poll_id;
                    $voteModel->save();
                }
            }
        }
    }

    public function sendMail() {
        return $this->module->mail
            ->compose('vote', ['model' => $this])
            ->setTo($this->email)
            ->setSubject('Подтверждение на сайте golos.gov.spb.ru')
            ->send();
    }

    public static function findVotePollByUser($id) {
        if (Yii::$app->user->isGuest) return false;
        return self::find()->where([
            'user_id'   => Yii::$app->user->id,
            'poll_id'   => $id
        ])->count();
    }

    /**
     * @param $user_id
     * @return self[]|ActiveRecord[]
     */
    public static function findByUserId($user_id) {
        return self::find()->where([
            'user_id'   => $user_id
        ])->all();
    }

    public static function countBallsForUser() {
        if (Yii::$app->user->isGuest) return 0;
        $votes = self::findByUserId(Yii::$app->user->id);
        $count = 0;
        foreach ($votes AS $vote) {
            $count += $vote->poll->points;
        }
        return $count;
    }
}