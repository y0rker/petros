<?php

namespace common\modules\gifts\commands;

use Yii;
use yii\console\Controller;

/**
 * Gifts RBAC controller.
 */
class RbacController extends Controller
{
    /**
     * @inheritdoc
     */
    public $defaultAction = 'add';

    /**
     * @var array Main module permission array
     */
    public $mainPermission = [
        'name' => 'administrateGifts',
        'description' => 'Can administrate all "Gifts" module'
    ];

    /**
     * @var array Permission
     */
    public $permissions = [
        'BViewGifts' => [
            'description' => 'Can view backend gifts list'
        ],
        'BCreateGifts' => [
            'description' => 'Can create backend gifts'
        ],
        'BUpdateGifts' => [
            'description' => 'Can update backend gifts'
        ],
        'BDeleteGifts' => [
            'description' => 'Can delete backend gifts'
        ],
        'viewGifts' => [
            'description' => 'Can view gifts list'
        ],
        'createGifts' => [
            'description' => 'Can create gifts'
        ],
        'updateGifts' => [
            'description' => 'Can update gifts'
        ],
        'deleteGifts' => [
            'description' => 'Can delete gifts'
        ],
    ];

    /**
     * Add comments RBAC.
     */
    public function actionAdd()
    {
        $auth = Yii::$app->authManager;
        $superadmin = $auth->getRole('superadmin');
        $mainPermission = $auth->createPermission($this->mainPermission['name']);
        if (isset($this->mainPermission['description'])) {
            $mainPermission->description = $this->mainPermission['description'];
        }
        if (isset($this->mainPermission['rule'])) {
            $mainPermission->ruleName = $this->mainPermission['rule'];
        }
        $auth->add($mainPermission);

        foreach ($this->permissions as $name => $option) {
            $permission = $auth->createPermission($name);
            if (isset($option['description'])) {
                $permission->description = $option['description'];
            }
            if (isset($option['rule'])) {
                $permission->ruleName = $option['rule'];
            }
            $auth->add($permission);
            $auth->addChild($mainPermission, $permission);
        }

        $auth->addChild($superadmin, $mainPermission);

        return static::EXIT_CODE_NORMAL;
    }

    /**
     * Remove comments RBAC.
     */
    public function actionRemove()
    {
        $auth = Yii::$app->authManager;
        $permissions = array_keys($this->permissions);

        foreach ($permissions as $name => $option) {
            $permission = $auth->getPermission($name);
            $auth->remove($permission);
        }

        $mainPermission = $auth->getPermission($this->mainPermission['name']);
        $auth->remove($mainPermission);

        return static::EXIT_CODE_NORMAL;
    }
}
