<?php

namespace common\modules\gifts\traits;

use common\modules\gifts\Module AS Module;
use Yii;

/**
 * Class ModuleTrait
 * @package common\modules\pages\traits
 * Implements `getModule` method, to receive current module instance.
 */
trait ModuleTrait
{
    /**
     * @var \common\modules\gifts\Module|null Module instance
     */
    private $_module;

    /**
     * @return \common\modules\gifts\Module|null Module instance
     */
    public function getModule()
    {
        if ($this->_module === null) {
            $module = Module::getInstance();
            if ($module instanceof Module) {
                $this->_module = $module;
            } else {
                $this->_module = Yii::$app->getModule('gifts');
            }
        }
        return $this->_module;
    }
}