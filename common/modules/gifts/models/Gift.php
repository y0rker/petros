<?php
namespace common\modules\gifts\models;

use common\modules\gifts\models\query\GiftQuery;
use common\modules\gifts\Module;
use common\modules\gifts\traits\ModuleTrait;
use common\modules\users\models\User;
use common\modules\users\models\UserGifts;
use vova07\fileapi\behaviors\UploadBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Gift
 * @package common\modules\gifts\models
 * Модель подарка.
 *
 * @property integer $id ID
 * @property string $name Наименования
 * @property string $photo
 * @property string $points
 * @property integer $created_at
 * @property string $updated_at
 * @property integer $count
 * @property string $points_title
 *
 * @property Module module
 * @property string photoSrc
 *
 */
class Gift extends ActiveRecord
{

    use ModuleTrait;

    /**
     * Статусы публикации записей модели.
     */
    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gifts}}';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new GiftQuery(static::class);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'photo' => [
                        'path' => $this->module->photoPath,
                        'tempPath' => $this->module->photoTempPath,
                        'url' => $this->module->photoUrl
                    ],
                ]
            ],
        ];
    }

    public function canBuy()
    {
        if ($this->count === 0) {
            return false;
        }
        $points = 0;
        if (!Yii::$app->getUser()->getIsGuest()) {
            $user = User::findOne(Yii::$app->getUser()->getId());
            if (null !== $user) {
                $points = $user->points;
            }
        }
        $canBuy = ($this->points > 0 && $points >= $this->points);

        $countOrders = UserGifts::find()->where([
            'gift_id'   => $this->id,
            'status'    => true
        ])->count();

        return ($canBuy && $this->count > $countOrders);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'points', 'count', 'points_title'],'safe'],
            // Обязательные поля
            [['name', 'photo'], 'required'],

            [['points'], 'default', 'value' => 0],
            [['count'], 'default', 'value' => 0]
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name'          => 'Наименование',
            'photo'         => 'Изображение',
            'points'        => 'Баллы',
            'count'         => 'Количество',
            'points_title'  => 'Заголовок баллов'
        ];
    }

    public function getPhotoLink() {
        return $this->urlAttribute('photo');
    }
}