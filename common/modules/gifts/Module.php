<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 18.05.2017
 * Time: 18:19
 */

namespace common\modules\gifts;


class Module extends \yii\base\Module
{
    /**
     * @var integer Кол-во на странице
     */
    public $recordsPerPage = 20;

    /**
     * @var string Image path
     */
    public $photoPath = '@statics/web/gifts/images/';

    /**
     * @var string Images temporary path
     */
    public $photoTempPath = '@statics/temp/gifts/images/';

    /**
     * @var string Image URL
     */
    public $photoUrl = '/statics/gifts/images';
}