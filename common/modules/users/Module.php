<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 17.05.2017
 * Time: 12:49
 */

namespace common\modules\users;


/**
 * Module [[Users]]
 * Yii2 users module.
 */
class Module extends \yii\base\Module
{
    /**
     * @var integer Users per page
     */
    public $recordsPerPage = 10;

    /**
     * @var array User roles that can access backend module.
     */
    public $adminRoles = ['superadmin', 'admin'];

    /**
     * @var boolean Is module used for backend.
     */
    public $isBackend = false;

    /**
     * @var boolean If true after registration user will be required to confirm his e-mail address.
     */
    public $requireEmailConfirmation = false;

}