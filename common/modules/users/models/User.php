<?php

namespace common\modules\users\models;

use common\helpers\Security;
use common\modules\polls\models\vote\Vote;
use common\modules\users\traits\ModuleTrait;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Html;
use yii\web\IdentityInterface;
use Yii;

/**
 * Class User
 * @package common\modules\users\models
 * User model.
 *
 * @property integer $id ID
 * @property string $username Username
 * @property string $email E-mail
 * @property string $password_hash Password hash
 * @property string $auth_key Authentication key
 * @property string $token
 * @property string $role Role
 * @property integer $status Status
 * @property integer $created_at Created time
 * @property integer $updated_at Updated time
 * @property string $sms_code
 * @property string $sms_code_wake_up
 *
 * @property Profile $profile Profile
 * @property string statusName
 * @property string fullName
 * @property integer points
 *
 * @property string $password Password
 * @property string $repassword Repeat password
 */
class User extends ActiveRecord implements IdentityInterface
{
    use ModuleTrait;

    /** Inactive status */
    const STATUS_INACTIVE = 0;
    /** Active status */
    const STATUS_ACTIVE = 1;
    /** Banned status */
    const STATUS_BANNED = 2;
    /** Deleted status */
    const STATUS_DELETED = 3;

    /**
     * Default role
     */
    const ROLE_DEFAULT = 'user';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Find users by IDs.
     *
     * @param $ids self IDs
     * @param null $scope Scope
     *
     * @return self[] Users
     */
    public static function findIdentities($ids, $scope = null)
    {
        $query = static::find()->where(['id' => $ids]);
        if ($scope !== null) {
            if (is_array($scope)) {
                foreach ($scope as $value) {
                    $query->$value();
                }
            } else {
                $query->$scope();
            }
        }
        return $query->all();
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * Find model by username.
     *
     * @param string $username Username
     * @param string $scope Scope
     *
     * @return self User
     */
    public static function findByUsername($username, $scope = null)
    {
        $query = static::find()->where(['username' => $username]);
        if ($scope !== null) {
            if (is_array($scope)) {
                foreach ($scope as $value) {
                    $query->$value();
                }
            } else {
                $query->$scope();
            }
        }
        return $query->one();
    }

    /**
     * Find model by email.
     *
     * @param string $email Email
     * @param string $scope Scope
     *
     * @return array|\yii\db\ActiveRecord[] User
     */
    public static function findByEmail($email, $scope = null)
    {
        $query = static::find()->where(['email' => $email]);
        if ($scope !== null) {
            if (is_array($scope)) {
                foreach ($scope as $value) {
                    $query->$value();
                }
            } else {
                $query->$scope();
            }
        }
        return $query->one();
    }

    /**
     * Find model by token.
     *
     * @param string $token Token
     * @param string $scope Scope
     *
     * @return array|\yii\db\ActiveRecord[] User
     */
    public static function findByToken($token, $scope = null)
    {
        $query = static::find()->where(['token' => $token]);
        if ($scope !== null) {
            if (is_array($scope)) {
                foreach ($scope as $value) {
                    $query->$value();
                }
            } else {
                $query->$scope();
            }
        }
        return $query->one();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::className(),
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Auth Key validation.
     *
     * @param string $authKey
     *
     * @return boolean
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Password validation.
     *
     * @param string $password
     *
     * @return boolean
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * @return string Human readable created date
     */
    public function getCreated()
    {
        return Yii::$app->formatter->asDate($this->created_at);
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username'      => Yii::t('users', 'ATTR_USERNAME'),
            'email'         => Yii::t('users', 'ATTR_EMAIL'),
            'role'          => Yii::t('users', 'ATTR_ROLE'),
            'status'        => Yii::t('users', 'ATTR_STATUS'),
            'created_at'    => Yii::t('users', 'ATTR_CREATED'),
            'updated_at'    => Yii::t('users', 'ATTR_UPDATED'),
            'is_shop'       => 'Пользователь магазина'
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                // Set default status
                if (!$this->status) {
                    $this->status = self::STATUS_ACTIVE;
                }
                // Set default role
                if (!$this->role) {
                    $this->role = self::ROLE_DEFAULT;
                }
                // Generate auth and secure keys
                $this->generateAuthKey();
                $this->generateToken();
            }
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            Yii::$app->authManager->revokeAll($this->id);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return Profile|\yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id'])->inverseOf('user');
    }

    /**
     * Generates "remember me" authentication key.
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates secure key.
     */
    public function generateToken()
    {
        $this->token = Security::generateExpiringRandomString();
    }

    /**
     * Activates user account.
     *
     * @return boolean true if account was successfully activated
     */
    public function activation()
    {
        $this->status = self::STATUS_ACTIVE;
        $this->generateToken();
        return $this->save(false);
    }

    /**
     * Recover password.
     *
     * @param string $password New Password
     *
     * @return boolean true if password was successfully recovered
     */
    public function recovery($password)
    {
        $this->setPassword($password);
        $this->generateToken();
        return $this->save(false);
    }

    /**
     * Generates password hash from password and sets it to the model.
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Change user password.
     *
     * @param string $password Password
     *
     * @return boolean true if password was successfully changed
     */
    public function password($password)
    {
        $this->setPassword($password);
        return $this->save(false);
    }

    /**
     * Имя пользователя
     * @return string
     */
    public function getFullName() {
        $has_name = false;
        $profile = $this->profile;
        // Телефон
        $return = $this->username;
        $title = '';
        if (!empty($profile->last_name)) {
            $has_name = true;
            $title .= $profile->last_name.' ';
        }
        if (!empty($profile->first_name)) {
            $has_name = true;
            $title .= $profile->first_name.' ';
        }
        if (!empty($profile->middle_name)) {
            $has_name = true;
            $title .= $profile->middle_name.' ';
        }

        if ($has_name) {
            $return = $title;
        }
        return Html::encode($return);
    }

    public function getAllName()
    {
        $profile = $this->profile;
        // Телефон
        $return = $this->username;
        $title = '';
        if (!empty($profile->last_name)) {
            $title .= $profile->last_name.' ';
        }
        if (!empty($profile->first_name)) {
            $title .= $profile->first_name.' ';
        }
        if (!empty($profile->middle_name)) {
            $title .= $profile->middle_name.' ';
        }

        $return .= ' ' .  $title;

        return Html::encode($return);
    }

    /**
     * Количество заработанных баллов
     * @return integer
     */
    public function getPoints() {
        $points = 40;
        $profile = $this->profile;
        if (!empty($profile->first_name)) {
            $points += 10;
        }
        if (!empty($profile->last_name)) {
            $points += 10;
        }
        if (!empty($profile->middle_name)) {
            $points += 10;
        }
        if (!empty($this->email)) {
            $points += 30;
        }
        $points += Vote::countBallsForUser();
        $points -= UserGifts::countPointsForUser();

        return $points;
    }
}
