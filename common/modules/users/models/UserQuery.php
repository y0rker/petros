<?php

namespace common\modules\users\models;

use common\modules\users\models\User;
use common\modules\users\Module;
use common\modules\users\traits\ModuleTrait;
use yii\db\ActiveQuery;

/**
 * Class UserQuery
 * @package modules\users\models
 *
 * @property Module module
 *
 */
class UserQuery extends ActiveQuery
{
	use ModuleTrait;

    /**
     * Select active users.
     *
     * @return $this
     * @internal param ActiveQuery $query
     */
	public function active()
	{
		$this->andWhere(['status' => User::STATUS_ACTIVE]);
		return $this;
	}

    /**
     * Select inactive users.
     * @return $this
     * @internal param ActiveQuery $query
     */
	public function inactive()
	{
		$this->andWhere(['status' => User::STATUS_INACTIVE]);
		return $this;
	}

    /**
     * Select banned users.
     *
     * @return $this
     * @internal param ActiveQuery $query
     */
	public function banned()
	{
		$this->andWhere(['status' => User::STATUS_BANNED]);
		return $this;
	}

    /**
     * Select deleted users.
     * @return $this
     * @internal param ActiveQuery $query
     */
	public function deleted()
	{
		$this->andWhere(['status' => User::STATUS_DELETED]);
		return $this;
	}

    /**
     * Select users with role "user".
     *
     * @return $this
     * @internal param ActiveQuery $query
     */
	public function registered()
	{
		$this->andWhere(['role' => User::ROLE_DEFAULT]);
		return $this;
	}

    /**
     * Select users with role "user".
     *
     * @return $this
     * @internal param ActiveQuery $query
     */
	public function admin()
	{
		$this->andWhere(['role' => $this->module->adminRoles]);
		return $this;
	}
}
