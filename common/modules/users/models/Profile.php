<?php

namespace common\modules\users\models;

use common\modules\users\traits\ModuleTrait;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Profile
 * @package common\modules\users\models
 * User profile model.
 *
 * @property integer $user_id User ID
 * @property string $first_name Name
 * @property string $middle_name Mid Name
 * @property string $last_name Last Name
 *
 * @property User $user User
 */
class Profile extends ActiveRecord
{
    use ModuleTrait;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profiles}}';
    }

    /**
     * @inheritdoc
     * @return self
     */
    public static function findByUserId($id)
    {
        return static::findOne(['user_id' => $id]);
    }

    /**
     * @return string User full name
     */
    public function getFullName()
    {
        return $this->last_name . $this->first_name . ' ' . $this->middle_name;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // Safe
            [['first_name', 'middle_name', 'last_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'first_name'    => Yii::t('users', 'ATTR_FIRST_NAME'),
            'last_name'     => Yii::t('users', 'ATTR_LAST_NAME'),
            'middle_name'   => Yii::t('users', 'ATTR_MIDDLE_NAME'),
        ];
    }

    /**
     * @return User|\yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id'])->inverseOf('profile');
    }
}
