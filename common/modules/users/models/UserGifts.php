<?php
/**
 * User: y0rker
 * Date: 12.12.2017
 * Time: 0:49
 */

namespace common\modules\users\models;

use common\modules\gifts\models\Gift;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\BadRequestHttpException;

/**
 * Class UserGifts
 *
 * @package common\modules\users\models
 */
class UserGifts extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_gifts}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'Номер заказа',
            'user_id'            => 'Пользователь',
            'gift_id'          => 'Товар',
            'status'         => 'Опубликовано',
            'points'        => 'Баллы',
            'approve'        => 'Подтверждено',
            'created'       => 'Дата заказа'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'approve'],'safe'],
            [['status', 'approve'], 'boolean'],
            // Обязательные поля
            [['user_id', 'gift_id', 'points'], 'required'],
        ];
    }

    /**
     * @return int|string
     */
    public static function countPointsForUser() {
        if (Yii::$app->user->isGuest) return 0;
        $gifts = self::find()->where([
            'user_id'   => Yii::$app->user->id,
            'status'    => true
        ])->all();
        $points = 0;
        foreach ($gifts as $gift) {
            $points += $gift->points;
        }
        return $points;
    }

    /**
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     * @throws BadRequestHttpException
     */
    public static function addNewGiftForUser($gift_id)
    {
        if (Yii::$app->user->isGuest) return false;

        $gift = Gift::findOne($gift_id);
        if (null === $gift) {
            throw new BadRequestHttpException();
        }
        $userGift = new self();
        $userGift->user_id = Yii::$app->user->id;
        $userGift->gift_id = $gift_id;
        $userGift->points = $gift->points;
        $userGift->created = new Expression('NOW()');
        if (!$userGift->save()) {
            return false;
        }
        return $userGift;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGift() {
        return $this->hasOne(Gift::className(), ['id' => 'gift_id']);
    }
}
