<?php
/**
 * Created by PhpStorm.
 * User: y0rker
 * Date: 08.08.2017
 * Time: 16:56
 */

namespace common\widgets\MetricsWidget;


use yii\base\Widget;

class MetricsWidget extends Widget
{

    public function run()
    {
        return $this->render('index');
    }

}