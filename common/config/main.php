<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language'  => 'ru',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => null,
                    'js' => [
                        '//ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js',
                    ]
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => \yii\web\User::className(),
            'identityClass' => \common\modules\users\models\User::className(),
            'loginUrl' => ['/users/guest/login']
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '/'
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => [
                'user'
            ],
            'itemFile' => '@common/modules/rbac/data/items.php',
            'assignmentFile' => '@common/modules/rbac/data/assignments.php',
            'ruleFile' => '@common/modules/rbac/data/rules.php',
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'timeFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'php:Y-m-d H:i:s',
            'decimalSeparator' => '.',
            'thousandSeparator' => ' ',

        ],
        'i18n' => [
            'translations' => [
                'users' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/modules/users/messages',
                ],
                'news' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/modules/news/messages',
                ],
                'polls' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/modules/polls/messages',
                ],
                'pages' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/modules/pages/messages',
                ],
            ]
        ],
    ],
    'modules'   => [
        'users' => [
            'class' => \common\modules\users\Module::className(),
        ],
    ]
];