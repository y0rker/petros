<?php

use common\helpers\Security;
use yii\db\Migration;

class m170516_111937_profile extends Migration
{
    public function up()
    {
        $this->createTable('profiles', [
            'user_id'       => $this->primaryKey(),
            'first_name'    => $this->string(50)->notNull(),
            'middle_name'   => $this->string(50)->notNull(),
            'last_name'     => $this->string(50)->notNull(),
        ]);
        // Foreign Keys
        $this->addForeignKey('FK_profile_user', 'profiles', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->execute($this->getUserSql());
        $this->execute($this->getProfileSql());
    }

    /**
     * @return string SQL to insert first user
     */
    private function getUserSql()
    {
        $time = time();
        $password_hash = Yii::$app->security->generatePasswordHash('admin12345');
        $auth_key = Yii::$app->security->generateRandomString();
        $token = Security::generateExpiringRandomString();
        return "INSERT INTO {{%user}} (username, email, password_hash, auth_key, token, role, status, created_at, updated_at) VALUES ('admin', 'admin@demo.com', '$password_hash', '$auth_key', '$token', 'superadmin', 1, $time, $time)";
    }

    /**
     * @return string SQL to insert first profile
     */
    private function getProfileSql()
    {
        return "INSERT INTO {{%profiles}} (user_id, first_name, middle_name, last_name) VALUES (1, 'Administration', '', 'Site')";
    }

    public function down()
    {
        $this->dropTable('profiles');
    }
}
