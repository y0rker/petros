<?php

use yii\db\Migration;

/**
 * Class m171211_214507_addUserGiftBuy
 */
class m171211_214507_addUserGiftBuy extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('user_gifts', [
            'id'    => $this->primaryKey(),
            'user_id'    => $this->integer(),
            'gift_id'   => $this->integer(),
            'points'    => $this->integer(),
            'status'    => $this->boolean()->defaultValue(true),
            'created'   => $this->dateTime()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('user_gifts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_214507_addUserGiftBuy cannot be reverted.\n";

        return false;
    }
    */
}
