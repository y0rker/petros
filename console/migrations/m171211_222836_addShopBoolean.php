<?php

use yii\db\Migration;

/**
 * Class m171211_222836_addShopBoolean
 */
class m171211_222836_addShopBoolean extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user', 'is_shop', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user', 'is_shop');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_222836_addShopBoolean cannot be reverted.\n";

        return false;
    }
    */
}
