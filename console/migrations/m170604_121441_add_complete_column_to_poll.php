<?php

use yii\db\Migration;

class m170604_121441_add_complete_column_to_poll extends Migration
{
    public function up()
    {
        $this->addColumn('polls', 'complete', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('polls', 'complete');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
