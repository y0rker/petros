<?php

use yii\db\Migration;

/**
 * Class m171218_193300_addCountGifts
 */
class m171218_193300_addCountGifts extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('gifts', 'count', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('gifts', 'count');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171218_193300_addCountGifts cannot be reverted.\n";

        return false;
    }
    */
}
