<?php

use yii\db\Migration;

class m170517_151352_add_big_text_column_to_params extends Migration
{
    public function up()
    {
        $this->addColumn('polls_params_items', 'big_text', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('polls_params_items', 'big_text');
    }
}
