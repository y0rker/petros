<?php

use yii\db\Migration;

class m170518_151704_gifts_table extends Migration
{
    public function up()
    {
        $this->createTable('gifts', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'photo'         => $this->string()->notNull(),
            'points'        => $this->integer()->notNull(),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('gifts');
    }
}
