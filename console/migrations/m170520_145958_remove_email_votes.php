<?php

use yii\db\Migration;

class m170520_145958_remove_email_votes extends Migration
{
    public function up()
    {
        $this->dropColumn('votes', 'email');
    }

    public function down()
    {
        $this->addColumn('votes', 'email', $this->string()->notNull());
    }
}
