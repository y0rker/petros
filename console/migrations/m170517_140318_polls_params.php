<?php

use yii\db\Migration;

class m170517_140318_polls_params extends Migration
{
    public function up()
    {
        $this->createTable('polls_params', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(50)->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
            'created_at'    => $this->integer(11)->notNull(),
            'updated_at'    => $this->integer(11)->notNull()
        ]);

        $this->createTable('polls_params_items', [
            'id'                => $this->primaryKey(),
            'poll_param_id'     => $this->integer()->notNull(),
            'name'              => $this->string()->notNull(),
            'multiple'          => $this->boolean()->defaultValue(false),
            'code'              => $this->string(50)->notNull(),
            'visible'           => $this->boolean()->defaultValue(true),
            'created_at'        => $this->integer(11)->notNull(),
            'updated_at'        => $this->integer(11)->notNull()
        ]);

        $this->createTable('polls_params_items_values', [
            'id'                    => $this->primaryKey(),
            'poll_param_item_id'    => $this->integer()->notNull(),
            'poll_id'               => $this->integer()->notNull(),
            'value'                 => $this->text()->notNull(),
            'created_at'            => $this->integer(11)->notNull(),
            'updated_at'            => $this->integer(11)->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('polls_params');
        $this->dropTable('polls_params_items');
        $this->dropTable('polls_params_items_values');
    }
}
