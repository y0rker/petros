<?php

use yii\db\Migration;

class m170520_150155_remove_sig_votes extends Migration
{
    public function up()
    {
        $this->dropColumn('votes', 'sig');
    }

    public function down()
    {
        $this->addColumn('votes', 'sig', $this->string()->notNull());
    }
}
