<?php

use yii\db\Migration;

class m170519_092903_sms_code extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'sms_code', $this->string()->null());
        $this->addColumn('user', 'sms_code_wake_up', $this->string()->null());
    }

    public function down()
    {
        $this->dropColumn('user', 'sms_code');
        $this->dropColumn('user', 'sms_code_wake_up');
    }
}
