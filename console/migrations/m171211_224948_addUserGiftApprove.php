<?php

use yii\db\Migration;

/**
 * Class m171211_224948_addUserGiftApprove
 */
class m171211_224948_addUserGiftApprove extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('user_gifts', 'approve', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('user_gifts', 'approve');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171211_224948_addUserGiftApprove cannot be reverted.\n";

        return false;
    }
    */
}
