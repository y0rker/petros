<?php

use yii\db\Migration;

class m170517_132256_polls_table extends Migration
{
    public function up()
    {
        $this->createTable('polls', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
            'alias'         => $this->string(255)->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
            'need_comment'  => $this->boolean()->defaultValue(false),
            'points'        => $this->integer(4)->defaultValue(0),
            'created_at'    => $this->integer(11)->notNull(),
            'updated_at'    => $this->integer(11)->notNull()
        ]);
    }

    public function down()
    {
        $this->dropTable('polls');
    }
}
