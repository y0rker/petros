<?php

use yii\db\Migration;

class m170519_151636_add_code_polls_params_items extends Migration
{
    public function up()
    {
        $this->addColumn('polls_params', 'code', $this->string()->null());
    }

    public function down()
    {
        $this->dropColumn('polls_params', 'code');
    }
}
