<?php

use yii\db\Migration;

class m170518_104851_votes_tables extends Migration
{
    public function up()
    {
        $this->createTable('votes', [
            'id'            => $this->primaryKey(),
            'poll_id'       => $this->integer()->notNull(),
            'user_id'       => $this->integer()->notNull(),
            'email'         => $this->string()->notNull(),
            'comment'       => $this->text()->notNull(),
            'accepted'      => $this->boolean()->defaultValue(false),
            'sig'           => $this->string()->notNull(),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer()
        ]);
        $this->createTable('votes_vote', [
            'id'            => $this->primaryKey(),
            'vote_id'       => $this->integer()->notNull(),
            'poll_vote_id'  => $this->integer()->notNull(),
            'poll_id'       => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('votes');
        $this->dropTable('votes_vote');
    }
}
