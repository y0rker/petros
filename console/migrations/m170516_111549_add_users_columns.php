<?php

use yii\db\Migration;

class m170516_111549_add_users_columns extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role', $this->string(64)->notNull()->defaultValue('user'));
        $this->addColumn('user', 'token', $this->string(54)->notNull());
        $this->createIndex('role', 'user', 'role');
    }

    public function down()
    {
        $this->dropColumn('user', 'role');
        $this->dropColumn('user', 'token');
        $this->dropIndex('role', 'user');
    }
}