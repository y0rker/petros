<?php

use yii\db\Migration;

class m170719_104838_poll_from_to_dates extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'date_from', $this->date());
        $this->addColumn('polls', 'date_to', $this->date());
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'date_to');
        $this->dropColumn('polls', 'date_from');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170719_104838_poll_from_to_dates cannot be reverted.\n";

        return false;
    }
    */
}
