<?php

use yii\db\Migration;

class m170520_152825_email_can_null extends Migration
{
    public function up()
    {
        $this->dropColumn('user', 'email');
        $this->addColumn('user', 'email', $this->string()->null());
    }

    public function down()
    {
        $this->dropColumn('user', 'email');
        $this->addColumn('user', 'email', $this->string()->notNull());
    }
}
