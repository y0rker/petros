<?php

use yii\db\Migration;

class m170518_102813_create_polls_votes extends Migration
{
    public function up()
    {
        $this->createTable('polls_votes', [
            'id'            => $this->primaryKey(),
            'name'          => $this->string(255)->notNull(),
            'poll_id'       => $this->integer()->notNull(),
            'description'   => $this->text()->notNull(),
            'photo'         => $this->string(255)->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
            'created_at'    => $this->integer(),
            'updated_at'    => $this->integer()
        ]);
    }

    public function down()
    {
        $this->dropTable('polls_votes');
    }
}
