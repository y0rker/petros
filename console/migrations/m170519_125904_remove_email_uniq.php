<?php

use yii\db\Migration;

class m170519_125904_remove_email_uniq extends Migration
{
    public function up()
    {
        $this->createIndex('email', 'user', [
            'email'
        ], true);

    }

    public function down()
    {
        $this->dropIndex('email', 'user');
    }
}
