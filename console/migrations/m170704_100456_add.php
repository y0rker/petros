<?php

use yii\db\Migration;

class m170704_100456_add extends Migration
{
    public function safeUp()
    {
        $this->addColumn('blogs', 'news_date', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('blogs', 'news_date');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170704_100456_add cannot be reverted.\n";

        return false;
    }
    */
}
