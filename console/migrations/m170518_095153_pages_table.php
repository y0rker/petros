<?php

use yii\db\Migration;

class m170518_095153_pages_table extends Migration
{
    public function up()
    {
        $this->createTable('pages', [
            'id'            => $this->primaryKey(),
            'code'          => $this->string(255)->notNull(),
            'name'          => $this->string(255)->notNull(),
            'subname'       => $this->string(50)->notNull(),
            'description'   => $this->text()->notNull(),
            'first_name'    => $this->string(255)->notNull(),
            'last_name'     => $this->string(255)->notNull(),
            'mid_name'      => $this->string(255)->notNull(),
            'photo'         => $this->string(255)->notNull(),
            'visible'       => $this->boolean()->defaultValue(true),
        ]);
    }

    public function down()
    {
        $this->dropTable('pages');
    }
}
