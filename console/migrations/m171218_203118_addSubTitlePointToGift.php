<?php

use yii\db\Migration;

/**
 * Class m171218_203118_addSubTitlePointToGift
 */
class m171218_203118_addSubTitlePointToGift extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('gifts', 'points_title', $this->string()->defaultValue(null));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('gifts', 'points_title');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171218_203118_addSubTitlePointToGift cannot be reverted.\n";

        return false;
    }
    */
}
