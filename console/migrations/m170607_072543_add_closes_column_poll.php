<?php

use yii\db\Migration;

class m170607_072543_add_closes_column_poll extends Migration
{
    public function up()
    {
        $this->addColumn('polls', 'private', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('polls', 'private');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
