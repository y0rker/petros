<?php

use yii\db\Migration;

class m170803_151247_polls_subfield extends Migration
{
    public function safeUp()
    {
        $this->addColumn('polls', 'sub_name', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('polls', 'sub_name');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_151247_polls_subfield cannot be reverted.\n";

        return false;
    }
    */
}
