<?php

use yii\db\Migration;

class m170520_152958_profile_info_can_null extends Migration
{
    public function up()
    {
        $this->dropColumn('profiles', 'first_name');
        $this->dropColumn('profiles', 'last_name');
        $this->dropColumn('profiles', 'middle_name');
        $this->addColumn('profiles', 'first_name', $this->string()->null());
        $this->addColumn('profiles', 'last_name', $this->string()->null());
        $this->addColumn('profiles', 'middle_name', $this->string()->null());
    }

    public function down()
    {
        $this->dropColumn('profiles', 'first_name');
        $this->dropColumn('profiles', 'last_name');
        $this->dropColumn('profiles', 'middle_name');
        $this->addColumn('profiles', 'first_name', $this->string()->notNull());
        $this->addColumn('profiles', 'last_name', $this->string()->notNull());
        $this->addColumn('profiles', 'middle_name', $this->string()->notNull());
    }
}
