<?php

use yii\db\Migration;

class m170608_114333_add_private_column_to_news extends Migration
{
    public function up()
    {
        $this->addColumn('blogs', 'private', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        $this->dropColumn('blogs', 'private');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
