<?php

use yii\db\Migration;

class m170601_091552_polls_gallery extends Migration
{
    public function up()
    {
        $this->createTable('polls_gallery', [
            'id'        => $this->primaryKey(),
            'poll_id'   => $this->integer(),
            'name'      => $this->string(),
            'image'     => $this->string()
        ]);

    }

    public function down()
    {
        $this->dropTable('polls_gallery');
    }

}
