<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'console\controllers',
    'controllerMap' => [
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
          ],
    ],
    'modules'   => [
        'rbac' => [
            'class' => \common\modules\rbac\Module::className(),
            'controllerNamespace' => 'common\modules\rbac\commands'
        ],
        'users' => [
            'class' => \common\modules\users\Module::className(),
            'controllerNamespace' => 'common\modules\users\commands'
        ],
        'news' => [
            'class' => \common\modules\news\Module::className(),
            'controllerNamespace' => 'common\modules\news\commands'
        ],
        'polls' => [
            'class' => \common\modules\polls\Module::className(),
            'controllerNamespace' => 'common\modules\polls\commands'
        ],
        'gifts' => [
            'class' => \common\modules\gifts\Module::className(),
            'controllerNamespace' => 'common\modules\gifts\commands'
        ],
        'pages' => [
            'class' => \common\modules\pages\Module::className(),
            'controllerNamespace' => 'common\modules\pages\commands'
        ],
    ],
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params' => $params,
];
